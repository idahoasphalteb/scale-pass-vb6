VERSION 5.00
Begin VB.Form frmCommError 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Scale Indicator Communication Errors"
   ClientHeight    =   10545
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13620
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmCommError.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10545
   ScaleWidth      =   13620
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtWarning 
      Height          =   2895
      Left            =   6960
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   11
      Top             =   6720
      Width           =   6255
   End
   Begin VB.TextBox txtStatus 
      Height          =   2895
      Left            =   6960
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   9
      Top             =   3480
      Width           =   6255
   End
   Begin VB.TextBox txtWeight 
      Height          =   2895
      Left            =   6960
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   7
      Top             =   240
      Width           =   6255
   End
   Begin VB.TextBox txtRaw 
      Height          =   2895
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   5
      Top             =   6720
      Width           =   6255
   End
   Begin VB.TextBox txtClean 
      Height          =   2895
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   2
      Top             =   3480
      Width           =   6255
   End
   Begin VB.Timer tmrScale 
      Enabled         =   0   'False
      Interval        =   10000
      Left            =   2880
      Top             =   1200
   End
   Begin VB.TextBox txtErrMsg 
      Height          =   2895
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   1
      Top             =   240
      Width           =   6255
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Close"
      Default         =   -1  'True
      Height          =   375
      Left            =   6000
      TabIndex        =   0
      Top             =   9960
      Width           =   1575
   End
   Begin VB.Label Label6 
      Caption         =   "Warning"
      Height          =   255
      Left            =   6960
      TabIndex        =   12
      Top             =   6480
      Width           =   3855
   End
   Begin VB.Label Label5 
      Caption         =   "Status Change"
      Height          =   255
      Left            =   6960
      TabIndex        =   10
      Top             =   3240
      Width           =   3855
   End
   Begin VB.Label Label4 
      Caption         =   "Scale Weight"
      Height          =   255
      Left            =   6960
      TabIndex        =   8
      Top             =   0
      Width           =   3855
   End
   Begin VB.Label Label3 
      Caption         =   "Raw Scale Output"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   6480
      Width           =   3855
   End
   Begin VB.Label Label2 
      Caption         =   "Cleaned Scale Output"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   3240
      Width           =   3855
   End
   Begin VB.Label Label1 
      Caption         =   "Errors"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   0
      Width           =   3855
   End
End
Attribute VB_Name = "frmCommError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOk_Click()
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    Unload Me

    Err.Clear
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error Resume Next
    tmrScale.Enabled = False
    
End Sub

Private Sub tmrScale_Timer()
    On Error Resume Next
    
    tmrScale.Enabled = False
    
    Unload Me
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub


Public Sub DisplayError(Msg As String)
    On Error Resume Next
    
    If Len(Msg & vbCrLf & txtErrMsg.Text) > 60000 Then
        txtErrMsg.Text = Msg
    Else
        txtErrMsg.Text = Msg & vbCrLf & txtErrMsg.Text
    End If
        
    Me.Show
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub

Public Sub DisplayCleanedOutput(Msg As Variant)
    On Error Resume Next
    
    If Len(Msg & vbCrLf & txtClean.Text) > 60000 Then
        txtClean.Text = Msg
    Else
        txtClean.Text = Msg & vbCrLf & txtClean.Text
    End If
        
    Me.Show
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub

Public Sub DisplayRawOutput(Msg As Variant)
    On Error Resume Next
    
    If Len(Msg & vbCrLf & txtRaw.Text) > 60000 Then
        txtRaw.Text = Msg
    Else
        txtRaw.Text = Msg & vbCrLf & txtRaw.Text
    End If
        
    Me.Show
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub

Public Sub DisplayWeight(Msg As String)
    On Error Resume Next
    
    If Len(Msg & vbCrLf & txtWeight.Text) > 60000 Then
        txtWeight.Text = Msg
    Else
        txtWeight.Text = Msg & vbCrLf & txtWeight.Text
    End If
    
    Me.Show
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub

Public Sub DisplayStatus(Msg As String)
    On Error Resume Next
    
    If Len(Msg & vbCrLf & txtStatus.Text) > 60000 Then
        txtStatus.Text = Msg
    Else
        txtStatus.Text = Msg & vbCrLf & txtStatus.Text
    End If
    
    If Me.Visible = False Then
        Me.Show
    End If
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub

Public Sub DisplayWarning(Msg As String)
    On Error Resume Next
    
    If Len(Msg & vbCrLf & txtWarning.Text) > 60000 Then
        txtWarning.Text = Msg
    Else
        txtWarning.Text = Msg & vbCrLf & txtWarning.Text
    End If
        
    Me.Show
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub



