VERSION 5.00
Begin VB.Form frmCommTesting 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Scale Indicator Communication Testing"
   ClientHeight    =   10545
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13620
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10545
   ScaleWidth      =   13620
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   6480
      TabIndex        =   22
      Text            =   ":)"
      Top             =   360
      Width           =   495
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear Data"
      Height          =   375
      Left            =   5040
      TabIndex        =   21
      Top             =   9840
      Width           =   1455
   End
   Begin VB.CheckBox chRaw 
      Alignment       =   1  'Right Justify
      Caption         =   "Log Raw Output"
      Height          =   195
      Left            =   4680
      TabIndex        =   20
      Top             =   6480
      Value           =   1  'Checked
      Width           =   1695
   End
   Begin VB.CheckBox chWarning 
      Alignment       =   1  'Right Justify
      Caption         =   "Log Warnings"
      Height          =   195
      Left            =   11760
      TabIndex        =   19
      Top             =   6480
      Value           =   1  'Checked
      Width           =   1455
   End
   Begin VB.CheckBox chStatus 
      Alignment       =   1  'Right Justify
      Caption         =   "Log Status"
      Height          =   195
      Left            =   11760
      TabIndex        =   18
      Top             =   3240
      Value           =   1  'Checked
      Width           =   1455
   End
   Begin VB.CheckBox chCleanOutput 
      Alignment       =   1  'Right Justify
      Caption         =   "Log Clean Output"
      Height          =   195
      Left            =   4560
      TabIndex        =   17
      Top             =   3240
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.CheckBox chErrors 
      Alignment       =   1  'Right Justify
      Caption         =   "Log Errors"
      Height          =   195
      Left            =   4920
      TabIndex        =   16
      Top             =   0
      Value           =   1  'Checked
      Width           =   1455
   End
   Begin VB.CheckBox chWeight 
      Alignment       =   1  'Right Justify
      Caption         =   "Log Weight"
      Height          =   195
      Left            =   11760
      TabIndex        =   15
      Top             =   0
      Value           =   1  'Checked
      Width           =   1455
   End
   Begin VB.CommandButton cmdClosePort 
      Caption         =   "Close Port"
      Height          =   375
      Left            =   2400
      TabIndex        =   14
      Top             =   9840
      Width           =   1575
   End
   Begin VB.CommandButton cmdOpenPort 
      Caption         =   "Open Port"
      Height          =   375
      Left            =   600
      TabIndex        =   13
      Top             =   9840
      Width           =   1575
   End
   Begin VB.TextBox txtWarning 
      Height          =   2895
      Left            =   6960
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   11
      Top             =   6720
      Width           =   6255
   End
   Begin VB.TextBox txtStatus 
      Height          =   2895
      Left            =   6960
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   9
      Top             =   3480
      Width           =   6255
   End
   Begin VB.TextBox txtWeight 
      Height          =   2895
      Left            =   6960
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   7
      Top             =   240
      Width           =   6255
   End
   Begin VB.TextBox txtRaw 
      Height          =   2895
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   5
      Top             =   6720
      Width           =   6255
   End
   Begin VB.TextBox txtClean 
      Height          =   2895
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   2
      Top             =   3480
      Width           =   6255
   End
   Begin VB.TextBox txtErrMsg 
      Height          =   2895
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   1
      Top             =   240
      Width           =   6255
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Close"
      Default         =   -1  'True
      Height          =   375
      Left            =   11640
      TabIndex        =   0
      Top             =   9840
      Width           =   1575
   End
   Begin VB.Label Label6 
      Caption         =   "Warning"
      Height          =   255
      Left            =   6960
      TabIndex        =   12
      Top             =   6480
      Width           =   3855
   End
   Begin VB.Label Label5 
      Caption         =   "Status Change"
      Height          =   255
      Left            =   6960
      TabIndex        =   10
      Top             =   3240
      Width           =   3855
   End
   Begin VB.Label Label4 
      Caption         =   "Scale Weight"
      Height          =   255
      Left            =   6960
      TabIndex        =   8
      Top             =   0
      Width           =   3855
   End
   Begin VB.Label Label3 
      Caption         =   "Raw Scale Output"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   6480
      Width           =   3855
   End
   Begin VB.Label Label2 
      Caption         =   "Cleaned Scale Output"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   3240
      Width           =   3855
   End
   Begin VB.Label Label1 
      Caption         =   "Errors"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   0
      Width           =   3855
   End
End
Attribute VB_Name = "frmCommTesting"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents SGSScale As SGSScaleComm.clsScaleComm
Attribute SGSScale.VB_VarHelpID = -1

Private Sub cmdClear_Click()
    txtErrMsg.Text = Empty
    txtWeight.Text = Empty
    txtClean.Text = Empty
    txtStatus.Text = Empty
    txtRaw.Text = Empty
    txtWarning.Text = Empty
    
End Sub

Private Sub cmdClosePort_Click()
    On Error GoTo Error
    
    cmdOpenPort.Enabled = True
    cmdClosePort.Enabled = False
    
    SGSScale.ClosePort
    
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdClosePort_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
End Sub

Private Sub cmdOk_Click()
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    Unload Me

    Err.Clear
End Sub


Private Sub cmdOpenPort_Click()
    On Error GoTo Error
    
    
    If SGSScale.PortIsOpen = False Then
        cmdOpenPort.Enabled = False
        cmdClosePort.Enabled = True
        SGSScale.OpenPort
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdOpenPort_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
End Sub

Private Sub Form_Load()
    Set SGSScale = CreateObject("SGSScaleComm.clsScaleComm")
    SGSScale.conn = gbScaleConnStr
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error Resume Next
    
End Sub



Public Sub DisplayError(Msg As String)
    On Error Resume Next
    
    If Len(Msg & vbCrLf & txtErrMsg.Text) > 60000 Then
        txtErrMsg.Text = Msg
    Else
        txtErrMsg.Text = Msg & vbCrLf & txtErrMsg.Text
    End If
        
    Me.Show
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub

Public Sub DisplayCleanedOutput(Msg As Variant)
    On Error Resume Next
    
    If Len(Msg & vbCrLf & txtClean.Text) > 60000 Then
        txtClean.Text = Msg
    Else
        txtClean.Text = Msg & vbCrLf & txtClean.Text
    End If
        
    Me.Show
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub

Public Sub DisplayRawOutput(Msg As Variant)
    On Error Resume Next
    
    If Len(Msg & vbCrLf & txtRaw.Text) > 60000 Then
        txtRaw.Text = Msg
    Else
        txtRaw.Text = Msg & vbCrLf & txtRaw.Text
    End If
        
    Me.Show
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub

Public Sub DisplayWeight(Msg As String)
    On Error Resume Next
    
    If Len(Msg & vbCrLf & txtWeight.Text) > 60000 Then
        txtWeight.Text = Msg
    Else
        txtWeight.Text = Msg & vbCrLf & txtWeight.Text
    End If
    
    VerifyRead
    
    Me.Show
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub

Public Sub DisplayStatus(Msg As String)
    On Error Resume Next
    
    If Len(Msg & vbCrLf & txtStatus.Text) > 60000 Then
        txtStatus.Text = Msg
    Else
        txtStatus.Text = Msg & vbCrLf & txtStatus.Text
    End If
    
    If Me.Visible = False Then
        Me.Show
    End If
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub

Public Sub DisplayWarning(Msg As String)
    On Error Resume Next
    
    If Len(Msg & vbCrLf & txtWarning.Text) > 60000 Then
        txtWarning.Text = Msg
    Else
        txtWarning.Text = Msg & vbCrLf & txtWarning.Text
    End If
        
    Me.Show
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub



Private Sub Form_Unload(Cancel As Integer)
    SGSScale.ClosePort
    Set SGSScale = Nothing
End Sub

Private Sub SGSScale_CleanedScaleOutput(Data As Variant)
    On Error GoTo Error
    Dim ErrMsg As String
    
    If chCleanOutput.Value = vbChecked Then
        txtClean.Text = Data & vbCrLf & txtClean.Text
    End If
    
    Exit Sub
Error:
    ErrMsg = Me.Name & ".SGSScale_CleanedScaleOutput()" & " Error: " & Err.Number & " " & Err.Description
    txtErrMsg.Text = txtErrMsg.Text & ErrMsg & vbCrLf
    

End Sub

Private Sub SGSScale_Error(ErrMsg As String)
    On Error GoTo Error
    Dim lcErrMsg As String
    
    If chErrors.Value = vbChecked Then
        txtErrMsg.Text = ErrMsg & vbCrLf & txtErrMsg.Text
    End If
    
    Exit Sub
Error:
    lcErrMsg = Me.Name & ".SGSScale_Error()" & " Error: " & Err.Number & " " & Err.Description
    txtErrMsg.Text = lcErrMsg & vbCrLf & txtErrMsg.Text
    
End Sub

Private Sub SGSScale_RawScaleOutput(Data As Variant)
    On Error GoTo Error
    Dim ErrMsg As String
    
    If chRaw.Value = vbChecked Then
        txtRaw.Text = Data & txtRaw.Text
    End If
    
    Exit Sub
Error:
    ErrMsg = Me.Name & ".SGSScale_RawScaleOutput()" & " Error: " & Err.Number & " " & Err.Description
    txtErrMsg.Text = txtErrMsg.Text & ErrMsg & vbCrLf
    
End Sub

Private Sub SGSScale_ScaleWeight(Weight As Double, Sign As SGSScaleComm.Polarity, UnitOfMeasure As SGSScaleComm.WeightType, GN As SGSScaleComm.GrossOrNet)
    On Error GoTo Error
    Dim ErrMsg As String
    
    If chWeight.Value = vbChecked Then
        txtWeight.Text = "[Weight = " & Weight & "] [Sign = " & Sign & "] [UnitOfMeasure = " & UnitOfMeasure & "] [GN = " & GN & "]" & vbCrLf & txtWeight.Text
    End If
    
    VerifyRead


    Exit Sub
Error:
    ErrMsg = Me.Name & ".SGSScale_ScaleWeight()" & " Error: " & Err.Number & " " & Err.Description
    txtErrMsg.Text = txtErrMsg.Text & ErrMsg & vbCrLf
    
End Sub

Private Sub SGSScale_StatusChange(StatusStr As String)
    On Error GoTo Error
    Dim ErrMsg As String
    
    If chStatus.Value = vbChecked Then
        txtStatus.Text = StatusStr & vbCrLf & txtStatus.Text
    End If
    
    Exit Sub
Error:
    ErrMsg = Me.Name & ".SGSScale_StatusChange()" & " Error: " & Err.Number & " " & Err.Description
    txtErrMsg.Text = txtErrMsg.Text & ErrMsg & vbCrLf

End Sub

Private Sub SGSScale_Warning(WarningMsg As String)
    On Error GoTo Error
    Dim ErrMsg As String
    
    If chWarning.Value = vbChecked Then
        txtWarning.Text = WarningMsg & vbCrLf & txtWarning.Text
    End If
    
    Exit Sub
Error:
    ErrMsg = Me.Name & ".SGSScale_Warning()" & " Error: " & Err.Number & " " & Err.Description
    txtErrMsg.Text = txtErrMsg.Text & ErrMsg & vbCrLf

End Sub


Sub VerifyRead()
    On Error Resume Next
    
    Static TxtColor01 As Variant
    Static BgColor01 As Variant
    Static bIni As Boolean
    
    If bIni = False Then
        bIni = True
        TxtColor01 = &HC0FFFF
        BgColor01 = &HFF&
    End If
    
    'Front
'    If Text1.ForeColor = &HFF& Then
        With Text1
            .ForeColor = TxtColor01
            .BackColor = BgColor01
        End With
'    End If
    
    If TxtColor01 = &HC0FFFF Then
        TxtColor01 = &HFF&
        BgColor01 = &HC0FFFF
    Else
        TxtColor01 = &HC0FFFF
        BgColor01 = &HFF&
    End If
    
    Err.Clear

End Sub
