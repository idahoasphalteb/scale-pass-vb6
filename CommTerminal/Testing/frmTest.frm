VERSION 5.00
Begin VB.Form frmTest 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Parsing Weight Test"
   ClientHeight    =   10215
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9870
   Icon            =   "frmTest.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10215
   ScaleWidth      =   9870
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtClean 
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   2685
      Left            =   2040
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   30
      Top             =   7080
      Width           =   6375
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Create Object"
      Height          =   375
      Left            =   3120
      TabIndex        =   29
      Top             =   240
      Width           =   1695
   End
   Begin VB.CommandButton cmdSettings 
      Caption         =   "Settings"
      Height          =   255
      Left            =   120
      TabIndex        =   28
      Top             =   120
      Width           =   1455
   End
   Begin VB.Frame Frame1 
      Caption         =   "G/N"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   7680
      TabIndex        =   24
      Top             =   2640
      Width           =   1335
      Begin VB.OptionButton OptnGross 
         Caption         =   "Gross"
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   240
         Width           =   735
      End
      Begin VB.OptionButton OptnNet 
         Caption         =   "Net"
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   600
         Width           =   615
      End
      Begin VB.OptionButton OptnGNUnknown 
         Caption         =   "Unknown"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   960
         Width           =   1095
      End
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "&Exit"
      Height          =   255
      Left            =   8400
      TabIndex        =   22
      Top             =   2160
      Width           =   1335
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "&Close Port"
      Height          =   255
      Left            =   8400
      TabIndex        =   21
      Top             =   1680
      Width           =   1335
   End
   Begin VB.CommandButton cmdOpen 
      Caption         =   "&Open Port"
      Height          =   255
      Left            =   8400
      TabIndex        =   20
      Top             =   1320
      Width           =   1335
   End
   Begin VB.Frame frmUOM 
      Caption         =   "Unit Of Measure"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   5040
      TabIndex        =   11
      Top             =   2640
      Width           =   2535
      Begin VB.OptionButton OptnUnknownUOM 
         Caption         =   "Unknown"
         Height          =   255
         Left            =   1320
         TabIndex        =   23
         Top             =   960
         Width           =   1095
      End
      Begin VB.OptionButton OptnOunces 
         Caption         =   "Ounces"
         Height          =   255
         Left            =   1320
         TabIndex        =   19
         Top             =   600
         Width           =   1095
      End
      Begin VB.OptionButton OptnGrams 
         Caption         =   "Grams"
         Height          =   255
         Left            =   1320
         TabIndex        =   18
         Top             =   240
         Width           =   1095
      End
      Begin VB.OptionButton OptnTons 
         Caption         =   "Tons"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   960
         Width           =   1095
      End
      Begin VB.OptionButton OptnKilograms 
         Caption         =   "Kilograms"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   600
         Width           =   1095
      End
      Begin VB.OptionButton OptnPounds 
         Caption         =   "Pounds"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame frmSign 
      Caption         =   "Polarity"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   3600
      TabIndex        =   10
      Top             =   2640
      Width           =   1335
      Begin VB.OptionButton OptnUnknown 
         Caption         =   "Unknown"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   960
         Width           =   1095
      End
      Begin VB.OptionButton OptnNegative 
         Caption         =   "Negative"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   600
         Width           =   1095
      End
      Begin VB.OptionButton OptnPositive 
         Caption         =   "Positive"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.TextBox txtStatus 
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   9
      Top             =   2040
      Width           =   6375
   End
   Begin VB.TextBox txtWt 
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   2760
      Width           =   1815
   End
   Begin VB.TextBox txtRaw 
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   2685
      Left            =   2040
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   4200
      Width           =   6375
   End
   Begin VB.TextBox txtWarning 
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   1680
      Width           =   6375
   End
   Begin VB.TextBox txtError 
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   1320
      Width           =   6375
   End
   Begin VB.Label Label6 
      Caption         =   "Cleaned Scale Output:"
      Height          =   255
      Left            =   120
      TabIndex        =   31
      Top             =   7080
      Width           =   1935
   End
   Begin VB.Label Label5 
      Caption         =   "Status Change:"
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   2040
      Width           =   1455
   End
   Begin VB.Label Label4 
      Caption         =   "Scale Weight:"
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   2760
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "Raw Scale Output:"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   4200
      Width           =   1935
   End
   Begin VB.Label Label2 
      Caption         =   "Warnings:"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   1680
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Errors:"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   1320
      Width           =   1455
   End
End
Attribute VB_Name = "frmTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents SGSComm As clsScaleComm
Attribute SGSComm.VB_VarHelpID = -1

Private Sub cmdClose_Click()
    On Error Resume Next
    
    SGSComm.ClosePort
    
End Sub

Private Sub cmdExit_Click()
    Unload Me
End Sub

Private Sub cmdOpen_Click()
    On Error Resume Next
    
    SGSComm.OpenPort
    
End Sub

Private Sub cmdSettings_Click()
    On Error GoTo Error
    
    SGSComm.Setup Me
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSettings_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
End Sub

Private Sub Command1_Click()
'    Dim obj As SGSScaleComm.clsScaleComm
    Dim obj As Object
    
    Set obj = CreateObject("SGSScaleComm.clsScaleComm")
    
    obj.Setup
    
    Set obj = Nothing
End Sub

Private Sub Form_Load()
    On Error Resume Next
    Set SGSComm = New clsScaleComm
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error Resume Next
    
    SGSComm.ClosePort
    Set SGSComm = Nothing
End Sub


Private Sub SGSComm_CleanedScaleOutput(Data As Variant)
    txtClean.Text = txtClean.Text & Data & vbCrLf
End Sub

Private Sub SGSComm_Error(ErrMsg As String)
    txtError.Text = ErrMsg
End Sub


Private Sub SGSComm_RawScaleOutput(Data As Variant)
    txtRaw.Text = txtRaw.Text & Data & vbCrLf
End Sub


Private Sub SGSComm_ScaleWeight(Weight As Double, Sign As Polarity, UnitOfMeasure As WeightType, GN As GrossOrNet)
    txtWt.Text = Weight
    
    Select Case Sign
        Case Polarity.Negative
            OptnNegative = True
        Case Polarity.Positive
            OptnPositive = True
        Case Polarity.Unknown
            OptnUnknown = True
        Case Else
            OptnUnknown = True
    End Select
    
    Select Case UnitOfMeasure
        Case WeightType.Grams
            OptnGrams = True
        Case WeightType.Kilograms
            OptnKilograms = True
        Case WeightType.Ounces
            OptnOunces = True
        Case WeightType.Pounds
            OptnPounds = True
        Case WeightType.Tons
            OptnTons = True
        Case WeightType.Unknown
            OptnUnknownUOM = True
        Case Else
            OptnUnknownUOM = True
    End Select
    
    Select Case GN
        Case GrossOrNet.Gross
            OptnGross = True
        Case GrossOrNet.Net
            OptnNet = True
        Case GrossOrNet.Unknown
            OptnGNUnknown = True
        Case Else
            OptnGNUnknown = True
    End Select
        
End Sub

Private Sub SGSComm_StatusChange(StatusStr As String)
    txtStatus.Text = StatusStr
End Sub

Private Sub SGSComm_Warning(WarningMsg As String)
    txtWarning.Text = WarningMsg
End Sub
