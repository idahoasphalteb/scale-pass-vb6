VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmSetupParcing 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Setup Input String Parsing"
   ClientHeight    =   8715
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8775
   ControlBox      =   0   'False
   Icon            =   "frmSetupParcing.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8715
   ScaleWidth      =   8775
   StartUpPosition =   1  'CenterOwner
   Begin TabDlg.SSTab SSTab1 
      Height          =   5415
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   8775
      _ExtentX        =   15478
      _ExtentY        =   9551
      _Version        =   393216
      TabOrientation  =   1
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   2
      TabHeight       =   520
      BackColor       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Communication"
      TabPicture(0)   =   "frmSetupParcing.frx":0442
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "chContinuous"
      Tab(0).Control(1)=   "chACK"
      Tab(0).Control(2)=   "txtACK"
      Tab(0).Control(3)=   "cmdLKUACK"
      Tab(0).Control(4)=   "txtENQ"
      Tab(0).Control(5)=   "cmdLKUENQ"
      Tab(0).Control(6)=   "Label31"
      Tab(0).Control(7)=   "Label30"
      Tab(0).ControlCount=   8
      TabCaption(1)   =   "Field Layout"
      TabPicture(1)   =   "frmSetupParcing.frx":045E
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Label20"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Label19"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Label18"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Label17"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "Label16"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "Label15"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "Label14"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "Label13"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "Label12"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).Control(9)=   "Label11"
      Tab(1).Control(9).Enabled=   0   'False
      Tab(1).Control(10)=   "Label10"
      Tab(1).Control(10).Enabled=   0   'False
      Tab(1).Control(11)=   "Label9"
      Tab(1).Control(11).Enabled=   0   'False
      Tab(1).Control(12)=   "Label8"
      Tab(1).Control(12).Enabled=   0   'False
      Tab(1).Control(13)=   "Label7"
      Tab(1).Control(13).Enabled=   0   'False
      Tab(1).Control(14)=   "Label6"
      Tab(1).Control(14).Enabled=   0   'False
      Tab(1).Control(15)=   "Label5"
      Tab(1).Control(15).Enabled=   0   'False
      Tab(1).Control(16)=   "Label4"
      Tab(1).Control(16).Enabled=   0   'False
      Tab(1).Control(17)=   "Label3"
      Tab(1).Control(17).Enabled=   0   'False
      Tab(1).Control(18)=   "Label2"
      Tab(1).Control(18).Enabled=   0   'False
      Tab(1).Control(19)=   "Label1"
      Tab(1).Control(19).Enabled=   0   'False
      Tab(1).Control(20)=   "Label29"
      Tab(1).Control(20).Enabled=   0   'False
      Tab(1).Control(21)=   "Label28"
      Tab(1).Control(21).Enabled=   0   'False
      Tab(1).Control(22)=   "cmdLKUGNNet"
      Tab(1).Control(22).Enabled=   0   'False
      Tab(1).Control(23)=   "cmdLKUGNGross"
      Tab(1).Control(23).Enabled=   0   'False
      Tab(1).Control(24)=   "txtGNNet"
      Tab(1).Control(24).Enabled=   0   'False
      Tab(1).Control(25)=   "txtGNGross"
      Tab(1).Control(25).Enabled=   0   'False
      Tab(1).Control(26)=   "txtGNLen"
      Tab(1).Control(26).Enabled=   0   'False
      Tab(1).Control(27)=   "txtGNStart"
      Tab(1).Control(27).Enabled=   0   'False
      Tab(1).Control(28)=   "cmdLKUUOMOnc"
      Tab(1).Control(28).Enabled=   0   'False
      Tab(1).Control(29)=   "cmdLKUUOMGram"
      Tab(1).Control(29).Enabled=   0   'False
      Tab(1).Control(30)=   "cmdLKUUOMTon"
      Tab(1).Control(30).Enabled=   0   'False
      Tab(1).Control(31)=   "cmdLKUUOMKilo"
      Tab(1).Control(31).Enabled=   0   'False
      Tab(1).Control(32)=   "cmdLKUUOMPnds"
      Tab(1).Control(32).Enabled=   0   'False
      Tab(1).Control(33)=   "txtUOMOnc"
      Tab(1).Control(33).Enabled=   0   'False
      Tab(1).Control(34)=   "txtUOMGram"
      Tab(1).Control(34).Enabled=   0   'False
      Tab(1).Control(35)=   "txtUOMTon"
      Tab(1).Control(35).Enabled=   0   'False
      Tab(1).Control(36)=   "cmdLKUWtDec"
      Tab(1).Control(36).Enabled=   0   'False
      Tab(1).Control(37)=   "cmdLKUWtPad"
      Tab(1).Control(37).Enabled=   0   'False
      Tab(1).Control(38)=   "cmdLKUPolNeg"
      Tab(1).Control(38).Enabled=   0   'False
      Tab(1).Control(39)=   "cmdLKUPolPos"
      Tab(1).Control(39).Enabled=   0   'False
      Tab(1).Control(40)=   "txtUOMKilo"
      Tab(1).Control(40).Enabled=   0   'False
      Tab(1).Control(41)=   "txtUOMPnds"
      Tab(1).Control(41).Enabled=   0   'False
      Tab(1).Control(42)=   "txtUOMLen"
      Tab(1).Control(42).Enabled=   0   'False
      Tab(1).Control(43)=   "txtUOMStart"
      Tab(1).Control(43).Enabled=   0   'False
      Tab(1).Control(44)=   "txtWtDec"
      Tab(1).Control(44).Enabled=   0   'False
      Tab(1).Control(45)=   "txtWtPad"
      Tab(1).Control(45).Enabled=   0   'False
      Tab(1).Control(46)=   "txtWtLen"
      Tab(1).Control(46).Enabled=   0   'False
      Tab(1).Control(47)=   "txtWtStart"
      Tab(1).Control(47).Enabled=   0   'False
      Tab(1).Control(48)=   "txtPolNeg"
      Tab(1).Control(48).Enabled=   0   'False
      Tab(1).Control(49)=   "txtPolPos"
      Tab(1).Control(49).Enabled=   0   'False
      Tab(1).Control(50)=   "txtPolLen"
      Tab(1).Control(50).Enabled=   0   'False
      Tab(1).Control(51)=   "txtPolStart"
      Tab(1).Control(51).Enabled=   0   'False
      Tab(1).Control(52)=   "txtLength"
      Tab(1).Control(52).Enabled=   0   'False
      Tab(1).Control(53)=   "cmdLkuTerm"
      Tab(1).Control(53).Enabled=   0   'False
      Tab(1).Control(54)=   "txtTerm"
      Tab(1).Control(54).Enabled=   0   'False
      Tab(1).Control(55)=   "cmdLkuSTX"
      Tab(1).Control(55).Enabled=   0   'False
      Tab(1).Control(56)=   "txtSTX"
      Tab(1).Control(56).Enabled=   0   'False
      Tab(1).ControlCount=   57
      Begin VB.TextBox txtSTX 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   1680
         TabIndex        =   10
         Top             =   120
         Width           =   975
      End
      Begin VB.CommandButton cmdLkuSTX 
         Caption         =   "Lookup"
         Height          =   255
         Left            =   2760
         TabIndex        =   11
         ToolTipText     =   "Lookup ASCII Code"
         Top             =   120
         Width           =   735
      End
      Begin VB.TextBox txtTerm 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   5040
         TabIndex        =   13
         Top             =   120
         Width           =   975
      End
      Begin VB.CommandButton cmdLkuTerm 
         Caption         =   "Lookup"
         Height          =   255
         Left            =   6120
         TabIndex        =   14
         ToolTipText     =   "Lookup ASCII Code"
         Top             =   120
         Width           =   735
      End
      Begin VB.CheckBox chContinuous 
         Caption         =   "Continuous Feed"
         Height          =   255
         Left            =   -74760
         TabIndex        =   0
         ToolTipText     =   $"frmSetupParcing.frx":047A
         Top             =   240
         Value           =   1  'Checked
         Width           =   1695
      End
      Begin VB.CheckBox chACK 
         Caption         =   "Requires Acknowledgment"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -74520
         TabIndex        =   4
         ToolTipText     =   "Requires an acnowledgement sent after recieving data from the scale."
         Top             =   1080
         Width           =   3255
      End
      Begin VB.TextBox txtACK 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Enabled         =   0   'False
         Height          =   285
         Left            =   -72240
         TabIndex        =   6
         Top             =   1440
         Width           =   975
      End
      Begin VB.CommandButton cmdLKUACK 
         Caption         =   "Lookup"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -71160
         TabIndex        =   7
         ToolTipText     =   "Lookup ASCII Code"
         Top             =   1440
         Width           =   735
      End
      Begin VB.TextBox txtENQ 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Enabled         =   0   'False
         Height          =   285
         Left            =   -72600
         TabIndex        =   2
         Top             =   600
         Width           =   975
      End
      Begin VB.CommandButton cmdLKUENQ 
         Caption         =   "Lookup"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -71520
         TabIndex        =   3
         ToolTipText     =   "Lookup ASCII Code"
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox txtLength 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2400
         TabIndex        =   16
         Text            =   "13"
         Top             =   450
         Width           =   615
      End
      Begin VB.TextBox txtPolStart 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2400
         TabIndex        =   18
         Text            =   "1"
         Top             =   780
         Width           =   615
      End
      Begin VB.TextBox txtPolLen 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2400
         TabIndex        =   20
         Text            =   "1"
         Top             =   1140
         Width           =   615
      End
      Begin VB.TextBox txtPolPos 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   1
         TabIndex        =   22
         Text            =   " "
         Top             =   1500
         Width           =   615
      End
      Begin VB.TextBox txtPolNeg 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   1
         TabIndex        =   25
         Text            =   "-"
         Top             =   1860
         Width           =   615
      End
      Begin VB.TextBox txtWtStart 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   6720
         MaxLength       =   2
         TabIndex        =   28
         Text            =   "2"
         Top             =   780
         Width           =   615
      End
      Begin VB.TextBox txtWtLen 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   6720
         MaxLength       =   7
         TabIndex        =   30
         Text            =   "7"
         Top             =   1140
         Width           =   615
      End
      Begin VB.TextBox txtWtPad 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   6720
         MaxLength       =   1
         TabIndex        =   32
         Text            =   " "
         Top             =   1500
         Width           =   615
      End
      Begin VB.TextBox txtWtDec 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   6720
         MaxLength       =   1
         TabIndex        =   35
         Text            =   "."
         Top             =   1860
         Width           =   615
      End
      Begin VB.TextBox txtUOMStart 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2400
         TabIndex        =   38
         Text            =   "9"
         Top             =   2460
         Width           =   615
      End
      Begin VB.TextBox txtUOMLen 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2400
         TabIndex        =   40
         Text            =   "1"
         Top             =   2820
         Width           =   615
      End
      Begin VB.TextBox txtUOMPnds 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   1
         TabIndex        =   42
         Text            =   "L"
         Top             =   3180
         Width           =   615
      End
      Begin VB.TextBox txtUOMKilo 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   1
         TabIndex        =   45
         Text            =   "K"
         Top             =   3540
         Width           =   615
      End
      Begin VB.CommandButton cmdLKUPolPos 
         Caption         =   "Lookup"
         Height          =   255
         Left            =   3120
         TabIndex        =   23
         ToolTipText     =   "Lookup ASCII Code"
         Top             =   1500
         Width           =   735
      End
      Begin VB.CommandButton cmdLKUPolNeg 
         Caption         =   "Lookup"
         Height          =   255
         Left            =   3120
         TabIndex        =   26
         ToolTipText     =   "Lookup ASCII Code"
         Top             =   1860
         Width           =   735
      End
      Begin VB.CommandButton cmdLKUWtPad 
         Caption         =   "Lookup"
         Height          =   255
         Left            =   7440
         TabIndex        =   33
         ToolTipText     =   "Lookup ASCII Code"
         Top             =   1500
         Width           =   735
      End
      Begin VB.CommandButton cmdLKUWtDec 
         Caption         =   "Lookup"
         Height          =   255
         Left            =   7440
         TabIndex        =   36
         ToolTipText     =   "Lookup ASCII Code"
         Top             =   1860
         Width           =   735
      End
      Begin VB.TextBox txtUOMTon 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   6720
         MaxLength       =   1
         TabIndex        =   48
         Text            =   "T"
         Top             =   2820
         Width           =   615
      End
      Begin VB.TextBox txtUOMGram 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   6720
         MaxLength       =   1
         TabIndex        =   51
         Text            =   "G"
         Top             =   3180
         Width           =   615
      End
      Begin VB.TextBox txtUOMOnc 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   6720
         MaxLength       =   1
         TabIndex        =   54
         Text            =   "O"
         Top             =   3540
         Width           =   615
      End
      Begin VB.CommandButton cmdLKUUOMPnds 
         Caption         =   "Lookup"
         Height          =   255
         Left            =   3120
         TabIndex        =   43
         ToolTipText     =   "Lookup ASCII Code"
         Top             =   3180
         Width           =   735
      End
      Begin VB.CommandButton cmdLKUUOMKilo 
         Caption         =   "Lookup"
         Height          =   255
         Left            =   3120
         TabIndex        =   46
         ToolTipText     =   "Lookup ASCII Code"
         Top             =   3540
         Width           =   735
      End
      Begin VB.CommandButton cmdLKUUOMTon 
         Caption         =   "Lookup"
         Height          =   255
         Left            =   7440
         TabIndex        =   49
         ToolTipText     =   "Lookup ASCII Code"
         Top             =   2820
         Width           =   735
      End
      Begin VB.CommandButton cmdLKUUOMGram 
         Caption         =   "Lookup"
         Height          =   255
         Left            =   7440
         TabIndex        =   52
         ToolTipText     =   "Lookup ASCII Code"
         Top             =   3180
         Width           =   735
      End
      Begin VB.CommandButton cmdLKUUOMOnc 
         Caption         =   "Lookup"
         Height          =   255
         Left            =   7440
         TabIndex        =   55
         ToolTipText     =   "Lookup ASCII Code"
         Top             =   3540
         Width           =   735
      End
      Begin VB.TextBox txtGNStart 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2400
         TabIndex        =   57
         Text            =   "11"
         Top             =   4260
         Width           =   615
      End
      Begin VB.TextBox txtGNLen 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2400
         TabIndex        =   59
         Text            =   "1"
         Top             =   4620
         Width           =   615
      End
      Begin VB.TextBox txtGNGross 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   6720
         TabIndex        =   61
         Text            =   "G"
         Top             =   4260
         Width           =   615
      End
      Begin VB.TextBox txtGNNet 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   6720
         TabIndex        =   64
         Text            =   "N"
         Top             =   4620
         Width           =   615
      End
      Begin VB.CommandButton cmdLKUGNGross 
         Caption         =   "Lookup"
         Height          =   255
         Left            =   7440
         TabIndex        =   62
         ToolTipText     =   "Lookup ASCII Code"
         Top             =   4260
         Width           =   735
      End
      Begin VB.CommandButton cmdLKUGNNet 
         Caption         =   "Lookup"
         Height          =   255
         Left            =   7440
         TabIndex        =   65
         ToolTipText     =   "Lookup ASCII Code"
         Top             =   4620
         Width           =   735
      End
      Begin VB.Label Label28 
         Caption         =   "Start of Text (STX):"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   120
         Width           =   1455
      End
      Begin VB.Label Label29 
         Caption         =   "Terminator (Term)"
         Height          =   255
         Left            =   3720
         TabIndex        =   12
         Top             =   120
         Width           =   1455
      End
      Begin VB.Label Label31 
         Caption         =   "Acknowledgement (ACK)"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -74160
         TabIndex        =   5
         Top             =   1440
         Width           =   1935
      End
      Begin VB.Label Label30 
         Caption         =   "Enquiry / Request (ENQ)"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -74520
         TabIndex        =   1
         Top             =   600
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "Length Of Input:"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   450
         Width           =   2175
      End
      Begin VB.Label Label2 
         Caption         =   "Polarity Starting Position:"
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   780
         Width           =   2175
      End
      Begin VB.Label Label3 
         Caption         =   "Polarity Length:"
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   1140
         Width           =   2175
      End
      Begin VB.Label Label4 
         Caption         =   "Polarity Positive Character(s):"
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   1500
         Width           =   2175
      End
      Begin VB.Label Label5 
         Caption         =   "Polarity Negative Character(s):"
         Height          =   255
         Left            =   240
         TabIndex        =   24
         Top             =   1860
         Width           =   2295
      End
      Begin VB.Label Label6 
         Caption         =   "Weight Starting Position:"
         Height          =   255
         Left            =   4560
         TabIndex        =   27
         Top             =   780
         Width           =   2175
      End
      Begin VB.Label Label7 
         Caption         =   "Weight Length:"
         Height          =   255
         Left            =   4560
         TabIndex        =   29
         Top             =   1140
         Width           =   2175
      End
      Begin VB.Label Label8 
         Caption         =   "Weight Pading Character:"
         Height          =   255
         Left            =   4560
         TabIndex        =   31
         Top             =   1500
         Width           =   2175
      End
      Begin VB.Label Label9 
         Caption         =   "Weight Decimal Character:"
         Height          =   255
         Left            =   4560
         TabIndex        =   34
         Top             =   1860
         Width           =   2175
      End
      Begin VB.Label Label10 
         Caption         =   "UOM Starting Position:"
         Height          =   255
         Left            =   240
         TabIndex        =   37
         Top             =   2460
         Width           =   2175
      End
      Begin VB.Label Label11 
         Caption         =   "UOM Length:"
         Height          =   255
         Left            =   240
         TabIndex        =   39
         Top             =   2820
         Width           =   2175
      End
      Begin VB.Label Label12 
         Caption         =   "UOM Pounds Character(s):"
         Height          =   255
         Left            =   240
         TabIndex        =   41
         Top             =   3180
         Width           =   2175
      End
      Begin VB.Label Label13 
         Caption         =   "UOM Kilograms Character(s):"
         Height          =   255
         Left            =   240
         TabIndex        =   44
         Top             =   3540
         Width           =   2175
      End
      Begin VB.Label Label14 
         Caption         =   "UOM Tons Character(s):"
         Height          =   255
         Left            =   4560
         TabIndex        =   47
         Top             =   2820
         Width           =   2175
      End
      Begin VB.Label Label15 
         Caption         =   "UOM Grams Character(s):"
         Height          =   255
         Left            =   4560
         TabIndex        =   50
         Top             =   3180
         Width           =   2175
      End
      Begin VB.Label Label16 
         Caption         =   "UOM Ounces Character(s):"
         Height          =   255
         Left            =   4560
         TabIndex        =   53
         Top             =   3540
         Width           =   2175
      End
      Begin VB.Label Label17 
         Caption         =   "Gross / Net Starting Position:"
         Height          =   255
         Left            =   240
         TabIndex        =   56
         Top             =   4260
         Width           =   2175
      End
      Begin VB.Label Label18 
         Caption         =   "Gross / Net Length:"
         Height          =   255
         Left            =   240
         TabIndex        =   58
         Top             =   4620
         Width           =   2175
      End
      Begin VB.Label Label19 
         Caption         =   "Gross Character(s):"
         Height          =   255
         Left            =   4560
         TabIndex        =   60
         Top             =   4260
         Width           =   2175
      End
      Begin VB.Label Label20 
         Caption         =   "Net Character(s):"
         Height          =   255
         Left            =   4560
         TabIndex        =   63
         Top             =   4620
         Width           =   2175
      End
   End
   Begin VB.TextBox txtSamplePos 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000011&
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   1
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   3000
      TabIndex        =   98
      Text            =   "0"
      Top             =   6240
      Width           =   615
   End
   Begin VB.TextBox txtSampleLen 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000011&
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   1
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   720
      TabIndex        =   97
      Text            =   "0"
      Top             =   6240
      Width           =   615
   End
   Begin VB.Frame Frame1 
      Caption         =   "G/N"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   5640
      TabIndex        =   92
      Top             =   7320
      Width           =   1335
      Begin VB.OptionButton OptnGNUnknown 
         Caption         =   "Unknown"
         Height          =   255
         Left            =   120
         TabIndex        =   95
         Top             =   960
         Width           =   1095
      End
      Begin VB.OptionButton OptnNet 
         Caption         =   "Net"
         Height          =   255
         Left            =   120
         TabIndex        =   94
         Top             =   600
         Width           =   615
      End
      Begin VB.OptionButton OptnGross 
         Caption         =   "Gross"
         Height          =   255
         Left            =   120
         TabIndex        =   93
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame frmUOM 
      Caption         =   "Unit Of Measure"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   3000
      TabIndex        =   85
      Top             =   7320
      Width           =   2535
      Begin VB.OptionButton OptnPounds 
         Caption         =   "Pounds"
         Height          =   255
         Left            =   120
         TabIndex        =   91
         Top             =   240
         Width           =   1095
      End
      Begin VB.OptionButton OptnKilograms 
         Caption         =   "Kilograms"
         Height          =   255
         Left            =   120
         TabIndex        =   90
         Top             =   600
         Width           =   1095
      End
      Begin VB.OptionButton OptnTons 
         Caption         =   "Tons"
         Height          =   255
         Left            =   120
         TabIndex        =   89
         Top             =   960
         Width           =   1095
      End
      Begin VB.OptionButton OptnGrams 
         Caption         =   "Grams"
         Height          =   255
         Left            =   1320
         TabIndex        =   88
         Top             =   240
         Width           =   1095
      End
      Begin VB.OptionButton OptnOunces 
         Caption         =   "Ounces"
         Height          =   255
         Left            =   1320
         TabIndex        =   87
         Top             =   600
         Width           =   1095
      End
      Begin VB.OptionButton OptnUnknownUOM 
         Caption         =   "Unknown"
         Height          =   255
         Left            =   1320
         TabIndex        =   86
         Top             =   960
         Width           =   1095
      End
   End
   Begin VB.Frame frmSign 
      Caption         =   "Polarity"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   120
      TabIndex        =   81
      Top             =   7320
      Width           =   1335
      Begin VB.OptionButton OptnPositive 
         Caption         =   "Positive"
         Height          =   255
         Left            =   120
         TabIndex        =   84
         Top             =   240
         Width           =   1095
      End
      Begin VB.OptionButton OptnNegative 
         Caption         =   "Negative"
         Height          =   255
         Left            =   120
         TabIndex        =   83
         Top             =   600
         Width           =   1095
      End
      Begin VB.OptionButton OptnUnknown 
         Caption         =   "Unknown"
         Height          =   255
         Left            =   120
         TabIndex        =   82
         Top             =   960
         Width           =   1095
      End
   End
   Begin VB.TextBox txtSampleGN 
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   5640
      Locked          =   -1  'True
      TabIndex        =   80
      TabStop         =   0   'False
      Top             =   6960
      Width           =   1335
   End
   Begin VB.TextBox txtSampleUOM 
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   3000
      Locked          =   -1  'True
      TabIndex        =   78
      TabStop         =   0   'False
      Top             =   6960
      Width           =   2535
   End
   Begin VB.TextBox txtSampleWt 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   76
      TabStop         =   0   'False
      Text            =   "0"
      Top             =   6960
      Width           =   1215
   End
   Begin VB.TextBox txtSamplePol 
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   74
      TabStop         =   0   'False
      Top             =   6960
      Width           =   1455
   End
   Begin VB.CommandButton cmdParse 
      Caption         =   "Parse"
      Height          =   255
      Left            =   6000
      TabIndex        =   69
      Top             =   5880
      Width           =   1575
   End
   Begin VB.CommandButton cmdLKUSample 
      Caption         =   "Lookup"
      Height          =   255
      Left            =   5160
      TabIndex        =   68
      ToolTipText     =   "Lookup ASCII Code"
      Top             =   5880
      Width           =   735
   End
   Begin VB.TextBox txtSample 
      Height          =   285
      Left            =   120
      MaxLength       =   13
      TabIndex        =   67
      Top             =   5880
      Width           =   4935
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   255
      Left            =   7200
      TabIndex        =   72
      Top             =   7920
      Width           =   1455
   End
   Begin VB.CommandButton cmdSaveExit 
      Caption         =   "Save and &Exit"
      Height          =   255
      Left            =   7200
      TabIndex        =   71
      Top             =   7560
      Width           =   1455
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Height          =   255
      Left            =   7200
      TabIndex        =   70
      Top             =   7200
      Width           =   1455
   End
   Begin VB.Line Line1 
      BorderWidth     =   3
      X1              =   0
      X2              =   8760
      Y1              =   5520
      Y2              =   5520
   End
   Begin VB.Label Label27 
      Caption         =   "Current Position:"
      Height          =   255
      Left            =   1680
      TabIndex        =   99
      Top             =   6240
      Width           =   1335
   End
   Begin VB.Label Label26 
      Caption         =   "Length:"
      Height          =   255
      Left            =   120
      TabIndex        =   96
      Top             =   6240
      Width           =   615
   End
   Begin VB.Label Label25 
      Alignment       =   2  'Center
      Caption         =   "Gross / Net"
      Height          =   255
      Left            =   5640
      TabIndex        =   79
      Top             =   6720
      Width           =   1335
   End
   Begin VB.Label Label24 
      Alignment       =   2  'Center
      Caption         =   "Unit Of Measure"
      Height          =   255
      Left            =   3000
      TabIndex        =   77
      Top             =   6720
      Width           =   2535
   End
   Begin VB.Label Label23 
      Alignment       =   2  'Center
      Caption         =   "Weight"
      Height          =   255
      Left            =   1680
      TabIndex        =   75
      Top             =   6720
      Width           =   1215
   End
   Begin VB.Label Label22 
      Alignment       =   2  'Center
      Caption         =   "Polarity"
      Height          =   255
      Left            =   120
      TabIndex        =   73
      Top             =   6720
      Width           =   1455
   End
   Begin VB.Label Label21 
      Caption         =   "Sample Scale Data:"
      Height          =   255
      Left            =   120
      TabIndex        =   66
      Top             =   5640
      Width           =   1695
   End
End
Attribute VB_Name = "frmSetupParcing"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents pSGSComm As clsScaleComm
Attribute pSGSComm.VB_VarHelpID = -1


Sub GetSampleStats()
    On Error Resume Next
    txtSampleLen.Text = Len(txtSample.Text)
    txtSamplePos.Text = txtSample.SelStart
End Sub

Public Property Set SGSComm(ByRef obj As clsScaleComm)
    On Error GoTo Error
    
    Set pSGSComm = obj

    Exit Property
Error:
    MsgBox Me.Name & ".SGSComm(ByRef obj As clsScaleComm)" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
End Property

Function UpdateSettings() As Boolean
    On Error GoTo Error
    
    If ValidateSettings = False Then Exit Function
        
    gbContinious = CLng(chContinuous.Value)
    gbENQ = txtENQ.Text
    gbRequireACK = CLng(chACK.Value)
    gbACK = txtACK.Text
        
    gbLenOfInput = CLng(txtLength.Text)
        
    gbSTX = txtSTX.Text
    gbTerm = txtTerm.Text
        
    'Polarity Settings  *******************************************************************************************
    gbPolStartPos = CLng(txtPolStart.Text)
    gbPolLen = CLng(txtPolLen.Text)
    gbPolPosChars = txtPolPos.Text
    gbPolNegChars = txtPolNeg.Text
    
    'Weight Settings  *******************************************************************************************
    gbWtStartPos = CLng(txtWtStart.Text)
    gbWtLen = CLng(txtWtLen.Text)
    gbWtPadChar = txtWtPad.Text
    gbWtDecChar = txtWtDec.Text
    
    'Unit of Measure Settings  *******************************************************************************************
    gbUOMStartPos = CLng(txtUOMStart.Text)
    gbUOMLen = CLng(txtUOMLen.Text)
    gbUOMLbsChars = txtUOMPnds.Text
    gbUOMKGChars = txtUOMKilo.Text
    gbUOMTonChars = txtUOMTon.Text
    gbUOMGrmChars = txtUOMGram.Text
    gbUOMOncChars = txtUOMOnc.Text
        
    'Gross / Net Settings  *******************************************************************************************
    gbGNStartPos = CLng(txtGNStart.Text)
    gbGNLen = CLng(txtGNLen.Text)
    gbGNGrossChars = txtGNGross.Text
    gbGNNetChars = txtGNNet.Text
    
    UpdateSettings = True
    
    Exit Function
Error:
    MsgBox Me.Name & ".UpdateSettings()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
End Function


Function UpdateFrmSettings() As Boolean
    On Error GoTo Error
    Dim Cancel As Boolean
    
'    If ValidateSettings = False Then Exit Function
    
    chContinuous.Value = gbContinious
    txtENQ.Text = gbENQ
    chACK.Value = gbRequireACK
    txtACK.Text = gbACK
    
    txtLength.Text = gbLenOfInput
    
    txtSTX.Text = gbSTX
    txtTerm.Text = gbTerm
    
    'Polarity Settings  *******************************************************************************************
    txtPolStart.Text = gbPolStartPos
    txtPolLen.Text = gbPolLen
    Call txtPolLen_Validate(Cancel)
    txtPolPos.Text = gbPolPosChars
    txtPolNeg.Text = gbPolNegChars
    
    'Weight Settings  *******************************************************************************************
    txtWtStart.Text = gbWtStartPos
    txtWtLen.Text = gbWtLen
    txtWtPad.Text = gbWtPadChar
    txtWtDec.Text = gbWtDecChar
    
    'Unit of Measure Settings  *******************************************************************************************
    txtUOMStart.Text = gbUOMStartPos
    txtUOMLen.Text = gbUOMLen
    Call txtUOMLen_Validate(Cancel)
    txtUOMPnds.Text = gbUOMLbsChars
    txtUOMKilo.Text = gbUOMKGChars
    txtUOMTon.Text = gbUOMTonChars
    txtUOMGram.Text = gbUOMGrmChars
    txtUOMOnc.Text = gbUOMOncChars
        
    'Gross / Net Settings  *******************************************************************************************
    txtGNStart.Text = gbGNStartPos
    txtGNLen.Text = gbGNLen
    Call txtGNLen_Validate(Cancel)
    txtGNGross.Text = gbGNGrossChars
    txtGNNet.Text = gbGNNetChars
    
    UpdateFrmSettings = True
    
    Exit Function
Error:
    MsgBox Me.Name & ".UpdateFrmSettings()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
End Function



Public Sub SaveSettings()
    On Error Resume Next
    
    'Write to the ini file
    '**************************************************************************************************
    '**************************************************************************************************
    '       Continuous Feed / Enquiry
    '**************************************************************************************************
    '**************************************************************************************************
    SaveSetting AppTitle, "ScaleSettings", "Continious", CStr(gbContinious)
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Enquiry ENQ String
    '**************************************************************************************************
    '**************************************************************************************************
    SaveSetting AppTitle, "ScaleSettings", "ENQ", CStr(gbENQ)
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Require ACK
    '**************************************************************************************************
    '**************************************************************************************************
    SaveSetting AppTitle, "ScaleSettings", "RequireACK", CStr(gbRequireACK)
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Acknowledgement ACK string
    '**************************************************************************************************
    '**************************************************************************************************
    SaveSetting AppTitle, "ScaleSettings", "ACK", CStr(gbACK)
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Input Length
    '**************************************************************************************************
    '**************************************************************************************************
    SaveSetting AppTitle, "ScaleSettings", "LenOfInput", CStr(gbLenOfInput)

    '**************************************************************************************************
    '**************************************************************************************************
    '       Start of Text (STX)
    '**************************************************************************************************
    '**************************************************************************************************
    SaveSetting AppTitle, "ScaleSettings", "STX", CStr(gbSTX)
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Terminator (TERM)
    '**************************************************************************************************
    '**************************************************************************************************
    SaveSetting AppTitle, "ScaleSettings", "TERM", CStr(gbTerm)
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Polarity Information
    '**************************************************************************************************
    '**************************************************************************************************
    SaveSetting AppTitle, "ScaleSettings", "PolStartPos", CStr(gbPolStartPos)
    SaveSetting AppTitle, "ScaleSettings", "PolLen", CStr(gbPolLen)
    SaveSetting AppTitle, "ScaleSettings", "PolPosChars", gbPolPosChars
    SaveSetting AppTitle, "ScaleSettings", "PolNegChars", gbPolNegChars

    '**************************************************************************************************
    '**************************************************************************************************
    '       Weight Information
    '**************************************************************************************************
    '**************************************************************************************************
    SaveSetting AppTitle, "ScaleSettings", "WtStartPos", CStr(gbWtStartPos)
    SaveSetting AppTitle, "ScaleSettings", "WtLen", CStr(gbWtLen)
    SaveSetting AppTitle, "ScaleSettings", "WtPadChar", gbWtPadChar
    SaveSetting AppTitle, "ScaleSettings", "WtDecChar", gbWtDecChar

    '**************************************************************************************************
    '**************************************************************************************************
    '       Unit Of Measure Information
    '**************************************************************************************************
    '**************************************************************************************************
    SaveSetting AppTitle, "ScaleSettings", "UOMStartPos", CStr(gbUOMStartPos)
    SaveSetting AppTitle, "ScaleSettings", "UOMLen", CStr(gbUOMLen)
    SaveSetting AppTitle, "ScaleSettings", "UOMLbsChars", gbUOMLbsChars
    SaveSetting AppTitle, "ScaleSettings", "UOMKGChars", gbUOMKGChars
    SaveSetting AppTitle, "ScaleSettings", "UOMTonChars", gbUOMTonChars
    SaveSetting AppTitle, "ScaleSettings", "UOMGrmChars", gbUOMGrmChars
    SaveSetting AppTitle, "ScaleSettings", "UOMOncChars", gbUOMOncChars

    '**************************************************************************************************
    '**************************************************************************************************
    '       Gross / Net Information
    '**************************************************************************************************
    '**************************************************************************************************
    SaveSetting AppTitle, "ScaleSettings", "GNStartPos", CStr(gbGNStartPos)
    SaveSetting AppTitle, "ScaleSettings", "GNLen", CStr(gbGNLen)
    SaveSetting AppTitle, "ScaleSettings", "GNGrossChars", gbGNGrossChars
    SaveSetting AppTitle, "ScaleSettings", "GNNetChars", gbGNNetChars
    
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       RKL DEJ - save to database
    '**************************************************************************************************
    '**************************************************************************************************
    modSaveSettingDB.SaveParseSettingsDB
    
    Err.Clear

End Sub



Function ValidateSettings() As Boolean
    On Error GoTo Error
    
    If chContinuous.Value = vbUnchecked Then
        If txtENQ.Text = Empty Then
            MsgBox "The ENQ or enquire data is required.", vbInformation, "Setting Not Correct"
            txtENQ.SetFocus
            Exit Function
        End If
    End If
    
    If chACK.Value = vbChecked And chContinuous.Value = vbUnchecked Then
        If txtACK.Text = Empty Then
            MsgBox "The ACK or acknowledgement data is required.", vbInformation, "Setting Not Correct"
            txtENQ.SetFocus
            Exit Function
        End If
    End If
    
    If IsNumeric(txtLength.Text) = False Then
        MsgBox "The Length of Input is required.", vbInformation, "Setting Not Correct"
        txtLength.SetFocus
        Exit Function
    End If
    
    'Polarity Settings  *******************************************************************************************
    If IsNumeric(txtPolStart.Text) = False Then
        MsgBox "The Polarity Starting Position is required.", vbInformation, "Setting Not Correct"
        txtPolStart.SetFocus
        Exit Function
    End If
    
    If IsNumeric(txtPolLen.Text) = False Then
        MsgBox "The Polarity Length is required.", vbInformation, "Setting Not Correct"
        txtPolLen.SetFocus
        Exit Function
    End If
    
    If Len(txtPolPos.Text) <> CLng(txtPolLen.Text) Then
        MsgBox "The Polarity Positive Character(s) is not the correct length.", vbInformation, "Setting Not Correct"
        txtPolPos.SetFocus
        Exit Function
    End If
    
    If Len(txtPolNeg.Text) <> CLng(txtPolLen.Text) Then
        MsgBox "The Polarity Nagative Character(s) is not the correct length.", vbInformation, "Setting Not Correct"
        txtPolNeg.SetFocus
        Exit Function
    End If
    
    'Weight Settings  *******************************************************************************************
    If IsNumeric(txtWtStart.Text) = False Then
        MsgBox "The Weight Starting Position is required.", vbInformation, "Setting Not Correct"
        txtWtStart.SetFocus
        Exit Function
    End If
    
    If IsNumeric(txtWtLen.Text) = False Then
        MsgBox "The Weight Length is required.", vbInformation, "Setting Not Correct"
        txtWtLen.SetFocus
        Exit Function
    End If
    
    If Len(txtWtPad.Text) <= 0 Then
        MsgBox "The Weight Pading Character is required", vbInformation, "Setting Not Correct"
        txtWtPad.SetFocus
        Exit Function
    End If
    
    If Len(txtWtDec.Text) <= 0 Then
        MsgBox "The Weight Decimal Character is required", vbInformation, "Setting Not Correct"
        txtWtDec.SetFocus
        Exit Function
    End If
    
    'Unit of Measure Settings  *******************************************************************************************
    If IsNumeric(txtUOMStart.Text) = False Then
        MsgBox "The UOM Starting Position is required.", vbInformation, "Setting Not Correct"
        txtUOMStart.SetFocus
        Exit Function
    End If
    
    If IsNumeric(txtUOMLen.Text) = False Then
        MsgBox "The UOM Length is required.", vbInformation, "Setting Not Correct"
        txtUOMLen.SetFocus
        Exit Function
    End If
    
    If Len(txtUOMPnds.Text) <> CLng(txtUOMLen.Text) Then
        MsgBox "The UOM Pounds Character(s) is not the correct length.", vbInformation, "Setting Not Correct"
        txtUOMPnds.SetFocus
        Exit Function
    End If
    
    If Len(txtUOMKilo.Text) <> CLng(txtUOMLen.Text) Then
        MsgBox "The UOM Kilograms Character(s) is not the correct length.", vbInformation, "Setting Not Correct"
        txtUOMKilo.SetFocus
        Exit Function
    End If
    
    If Len(txtUOMTon.Text) <> CLng(txtUOMLen.Text) Then
        MsgBox "The UOM Tons Character(s) is not the correct length.", vbInformation, "Setting Not Correct"
        txtUOMTon.SetFocus
        Exit Function
    End If
    
    If Len(txtUOMGram.Text) <> CLng(txtUOMLen.Text) Then
        MsgBox "The UOM Grams Character(s) is not the correct length.", vbInformation, "Setting Not Correct"
        txtUOMGram.SetFocus
        Exit Function
    End If
    
    If Len(txtUOMOnc.Text) <> CLng(txtUOMLen.Text) Then
        MsgBox "The UOM Ounces Character(s) is not the correct length.", vbInformation, "Setting Not Correct"
        txtUOMOnc.SetFocus
        Exit Function
    End If
    
    'Gross / Net Settings  *******************************************************************************************
    If IsNumeric(txtGNStart.Text) = False Then
        MsgBox "The Gross / Net Starting Position is required.", vbInformation, "Setting Not Correct"
        txtGNStart.SetFocus
        Exit Function
    End If
    
    If IsNumeric(txtGNLen.Text) = False Then
        MsgBox "The Gross / Net Length is required.", vbInformation, "Setting Not Correct"
        txtGNLen.SetFocus
        Exit Function
    End If
    
    If Len(txtGNGross.Text) <> CLng(txtGNLen.Text) Then
        MsgBox "The Gross Character(s) is not the correct length.", vbInformation, "Setting Not Correct"
        txtGNGross.SetFocus
        Exit Function
    End If
    
    If Len(txtGNNet.Text) <> CLng(txtGNLen.Text) Then
        MsgBox "The Net Character(s) is not the correct length.", vbInformation, "Setting Not Correct"
        txtGNNet.SetFocus
        Exit Function
    End If
    
    ValidateSettings = True
    
    Exit Function
Error:
    MsgBox Me.Name & ".ValidateSettings()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
End Function


Private Sub chACK_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    If chACK.Value = vbChecked Then
        Label31.Enabled = True
        txtACK.Enabled = True
        cmdLKUACK.Enabled = True
    Else
        Label31.Enabled = False
        txtACK.Enabled = False
        cmdLKUACK.Enabled = False
    End If

    Exit Sub
Error:
    ErrMSG = Me.Name & ".chACK_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"
    
End Sub

Private Sub chContinuous_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    If chContinuous.Value = vbChecked Then
        Label30.Enabled = False
        txtENQ.Enabled = False
        cmdLKUENQ.Enabled = False
        
        chACK.Enabled = False
        
        Label31.Enabled = False
        txtACK.Enabled = False
        cmdLKUACK.Enabled = False
    Else
        Label30.Enabled = True
        txtENQ.Enabled = True
        cmdLKUENQ.Enabled = True
        
        chACK.Enabled = True
        
        If chACK.Value = vbChecked Then
            Label31.Enabled = True
            txtACK.Enabled = True
            cmdLKUACK.Enabled = True
        End If
    End If
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".chContinuous_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"
    
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub


Private Sub cmdLKUACK_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtACK)
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLKUACK_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"

End Sub

Private Sub cmdLKUENQ_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtENQ)
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLKUENQ_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"

End Sub

Private Sub cmdLKUGNGross_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtGNGross)
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLKUGNGross_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"
End Sub

Private Sub cmdLKUGNNet_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtGNNet)
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLKUGNNet_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"
End Sub


Private Sub cmdLKUPolNeg_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtPolNeg)
        
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLKUPolNeg_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"
End Sub

Private Sub cmdLKUPolPos_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtPolPos)
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLKUPolPos_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"
End Sub

Private Sub cmdLKUSample_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtSample)
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLKUSample_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"
End Sub

Private Sub cmdLkuSTX_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtSTX)
            
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLkuSTX_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"
End Sub

Private Sub cmdLkuTerm_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtTerm)
            
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLkuTerm_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"
End Sub


Private Sub cmdLKUUOMGram_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtUOMGram)
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLKUUOMGram_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"

End Sub

Private Sub cmdLKUUOMKilo_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtUOMKilo)
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLKUUOMKilo_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"

End Sub

Private Sub cmdLKUUOMOnc_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtUOMOnc)
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLKUUOMOnc_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"

End Sub

Private Sub UpdateASCIICode(ByRef txtBox As TextBox)
    On Error GoTo Error
    
    Dim ASCIICode As Integer
    Dim ErrMSG As String
    
    ASCIICode = -1
    
    If txtBox.MaxLength = 1 Then
        If Len(txtBox.Text) = 1 Then
            ASCIICode = Asc(txtBox.Text)
        End If
        
        ASCIICode = pSGSComm.LookupASCII(ASCIICode)
        
        If ASCIICode = -1 Then
            Exit Sub
        Else
            txtBox.Text = Chr(ASCIICode)
        End If
    
    ElseIf Len(txtBox.Text) = txtBox.MaxLength = 1 Then
        ASCIICode = Asc(Right(txtBox.Text, 1))
        
        ASCIICode = pSGSComm.LookupASCII(ASCIICode)
        
        If ASCIICode = -1 Then
            Exit Sub
        Else
            txtBox.Text = Mid(txtBox.Text, 1, txtBox.MaxLength - 1) & Chr(ASCIICode)
        End If
    
    Else
        ASCIICode = pSGSComm.LookupASCII(ASCIICode)
        
        If ASCIICode = -1 Then
            Exit Sub
        Else
            txtBox.Text = txtBox.Text + Chr(ASCIICode)
        End If
    
    End If
    
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".UpdateASCIICode()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"

End Sub


Private Sub cmdLKUUOMPnds_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtUOMPnds)
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLKUUOMPnds_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"

End Sub

Private Sub cmdLKUUOMTon_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtUOMTon)
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLKUUOMTon_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"

End Sub

Private Sub cmdLKUWtDec_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtWtDec)
            
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLKUWtDec_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"

End Sub

Private Sub cmdLKUWtPad_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtWtPad)
            
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLKUWtPad_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"

End Sub

Private Sub cmdParse_Click()
    On Error GoTo Error
    Dim Start As Long
    Dim Length As Long
    Dim i As Long
    
    If CLng(Len(txtSample.Text)) <> CLng(txtLength.Text) Then
        MsgBox "The sample data is not the same length as what is expected.", vbInformation + vbExclamation, "Warnning Wrong Length"
'        MsgBox "The sample data is not the same length as what is expected.", vbInformation, "Wrong Length"
'        Exit Sub
    End If
    
    '*********************************************************************************************************
    '*********************************************************************************************************
    '       Get Polarity
    '*********************************************************************************************************
    '*********************************************************************************************************
    If Not IsNumeric(txtPolStart) Or Not IsNumeric(txtPolLen) Then
        MsgBox "The Polarity start position and length are must be valid numbers.", vbInformation, "Not a Valid Number"
        Exit Sub
    Else
        Start = CLng(txtPolStart)
        Length = CLng(txtPolLen)
    End If
    txtSamplePol.Text = Mid(txtSample.Text, Start, Length)
    Select Case txtSamplePol.Text
        Case txtPolPos.Text
            OptnPositive = True
        Case txtPolNeg.Text
            OptnNegative = True
        Case Else
            OptnUnknown = True
    End Select
    
    '*********************************************************************************************************
    '*********************************************************************************************************
    '       Get Weight
    '*********************************************************************************************************
    '*********************************************************************************************************
    If Not IsNumeric(txtWtStart) Or Not IsNumeric(txtWtLen) Then
        MsgBox "The Weight start position and length are must be valid numbers.", vbInformation, "Not a Valid Number"
        Exit Sub
    Else
        Start = CLng(txtWtStart)
        Length = CLng(txtWtLen)
    End If
    txtSampleWt.Text = Mid(txtSample.Text, Start, Length)
    txtSampleWt.Text = Replace(txtSampleWt.Text, txtWtDec.Text, ".")
    txtSampleWt.Text = Replace(txtSampleWt.Text, txtWtPad.Text, "0")    '*** This may not be correct... !!!!
    
    '*********************************************************************************************************
    '*********************************************************************************************************
    '       Get Unit Of Measure
    '*********************************************************************************************************
    '*********************************************************************************************************
    If Not IsNumeric(txtUOMStart) Or Not IsNumeric(txtUOMLen) Then
        MsgBox "The Unit Of Measure start position and length are must be valid numbers.", vbInformation, "Not a Valid Number"
        Exit Sub
    Else
        Start = CLng(txtUOMStart)
        Length = CLng(txtUOMLen)
    End If
    txtSampleUOM.Text = Mid(txtSample.Text, Start, Length)
    Select Case txtSampleUOM.Text
        Case txtUOMPnds.Text
            Me.OptnPounds = True
        Case txtUOMKilo.Text
            Me.OptnKilograms = True
        Case txtUOMTon.Text
            Me.OptnTons = True
        Case txtUOMGram.Text
            Me.OptnGrams = True
        Case txtUOMOnc.Text
            Me.OptnOunces = True
        Case Else
            OptnUnknownUOM = True
    End Select
    
    '*********************************************************************************************************
    '*********************************************************************************************************
    '       Get Gross / Net
    '*********************************************************************************************************
    '*********************************************************************************************************
    If Not IsNumeric(txtGNStart) Or Not IsNumeric(txtGNLen) Then
        MsgBox "The Gross / Net start position and length are must be valid numbers.", vbInformation, "Not a Valid Number"
        Exit Sub
    Else
        Start = CLng(txtGNStart)
        Length = CLng(txtGNLen)
    End If
    txtSampleGN.Text = Mid(txtSample.Text, Start, Length)
    Select Case txtSampleGN.Text
        Case txtGNGross.Text
'            GrossOrNet.Gross
            OptnGross.Value = True
        Case txtGNNet.Text
'            GrossOrNet.Net
            OptnNet.Value = True
        Case Else
'            GrossOrNet.Unknown
            OptnGNUnknown.Value = True
    End Select
    
'    txtSampleGN.Text = replace(txtSampleGN.Text, txtWtDec.Text, ".")
'    txtSampleGN.Text = replace(txtSampleGN.Text, txtWtPad.Text, "0")    '*** This may not be correct... !!!!
    
    Exit Sub
Error:
    MsgBox "The following error occurred while parsing the data:" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
End Sub


Private Sub cmdSave_Click()
    If UpdateSettings() = False Then Exit Sub
    Call Me.SaveSettings
End Sub


Private Sub cmdSaveExit_Click()
    If UpdateSettings() = False Then Exit Sub
    Call Me.SaveSettings
    Unload Me
End Sub

Private Sub Form_Load()
    Call UpdateFrmSettings
End Sub

Private Sub txtGNLen_Validate(Cancel As Boolean)
    On Error GoTo Error
    Dim ErrMSG As String
    
    If IsNumeric(txtGNLen.Text) = False Then Exit Sub
    
    txtGNGross.MaxLength = CInt(txtGNLen.Text)
    txtGNNet.MaxLength = CInt(txtGNLen.Text)
    
    txtGNGross.Text = Mid(txtGNGross.Text, 1, CInt(txtGNLen.Text))
    txtGNNet.Text = Mid(txtGNNet.Text, 1, CInt(txtGNLen.Text))
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".txtGNLen_Validate(Cancel As Boolean)" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"

End Sub


Private Sub txtLength_Change()
    On Error GoTo Error
    Dim ErrMSG As String
    
    If IsNumeric(txtLength.Text) = False Then Exit Sub
    
    txtSample.MaxLength = CInt(txtLength.Text)
    
    txtSample.Text = Mid(txtSample.Text, 1, CInt(txtLength.Text))
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".txtLength_Validate(Cancel As Boolean)" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"
End Sub

Private Sub txtPolLen_Validate(Cancel As Boolean)
    On Error GoTo Error
    Dim ErrMSG As String
    
    If IsNumeric(txtPolLen.Text) = False Then Exit Sub
    
    txtPolPos.MaxLength = CInt(txtPolLen.Text)
    txtPolNeg.MaxLength = CInt(txtPolLen.Text)
    
    txtPolPos.Text = Mid(txtPolPos.Text, 1, CInt(txtPolLen.Text))
    txtPolNeg.Text = Mid(txtPolNeg.Text, 1, CInt(txtPolLen.Text))
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".txtPolLen_Validate(Cancel As Boolean)" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"
End Sub


Private Sub txtSample_Change()
    Call GetSampleStats
End Sub

Private Sub txtSample_Click()
    Call GetSampleStats
End Sub


Private Sub txtSample_GotFocus()
    Call GetSampleStats
End Sub


Private Sub txtSample_KeyDown(KeyCode As Integer, Shift As Integer)
    Call GetSampleStats
End Sub


Private Sub txtSample_KeyPress(KeyAscii As Integer)
    Call GetSampleStats
End Sub


Private Sub txtSample_KeyUp(KeyCode As Integer, Shift As Integer)
    Call GetSampleStats
End Sub


Private Sub txtUOMLen_Validate(Cancel As Boolean)
    On Error GoTo Error
    Dim ErrMSG As String
    
    If IsNumeric(txtUOMLen.Text) = False Then Exit Sub
    
    txtUOMPnds.MaxLength = CInt(txtUOMLen.Text)
    txtUOMKilo.MaxLength = CInt(txtUOMLen.Text)
    txtUOMTon.MaxLength = CInt(txtUOMLen.Text)
    txtUOMGram.MaxLength = CInt(txtUOMLen.Text)
    txtUOMOnc.MaxLength = CInt(txtUOMLen.Text)
    
    txtUOMPnds.Text = Mid(txtUOMPnds.Text, 1, CInt(txtUOMLen.Text))
    txtUOMKilo.Text = Mid(txtUOMKilo.Text, 1, CInt(txtUOMLen.Text))
    txtUOMTon.Text = Mid(txtUOMTon.Text, 1, CInt(txtUOMLen.Text))
    txtUOMGram.Text = Mid(txtUOMGram.Text, 1, CInt(txtUOMLen.Text))
    txtUOMOnc.Text = Mid(txtUOMOnc.Text, 1, CInt(txtUOMLen.Text))
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".txtUOMLen_Validate(Cancel As Boolean)" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    pSGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"

End Sub


