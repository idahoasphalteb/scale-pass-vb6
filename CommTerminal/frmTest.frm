VERSION 5.00
Begin VB.Form frmTest 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Testing"
   ClientHeight    =   4140
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9870
   Icon            =   "frmTest.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4140
   ScaleWidth      =   9870
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chLog 
      Caption         =   "Log Raw Scale Output"
      Height          =   255
      Left            =   7440
      TabIndex        =   35
      Top             =   120
      Width           =   2295
   End
   Begin VB.CommandButton cmdSend 
      Caption         =   "Send Data"
      Height          =   285
      Left            =   1440
      TabIndex        =   34
      ToolTipText     =   "Send the data in the text box to the right to the Comport."
      Top             =   120
      Width           =   1095
   End
   Begin VB.CommandButton cmdLKUManual 
      Caption         =   "Lookup"
      Height          =   255
      Left            =   6360
      TabIndex        =   2
      ToolTipText     =   "Lookup ASCII Code"
      Top             =   120
      Width           =   735
   End
   Begin VB.TextBox txtManual 
      Height          =   285
      Left            =   3840
      TabIndex        =   1
      Top             =   120
      Width           =   2415
   End
   Begin VB.CommandButton cmdManual 
      Caption         =   "Manual Test"
      Height          =   285
      Left            =   2640
      TabIndex        =   0
      ToolTipText     =   "Enter the raw scale input value and click on this button to mimic the scale interface."
      Top             =   120
      Width           =   1095
   End
   Begin VB.TextBox txtCleaned 
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   12
      Top             =   2040
      Width           =   6375
   End
   Begin VB.CommandButton cmdProperties 
      Caption         =   "Properties"
      Height          =   375
      Left            =   8400
      TabIndex        =   33
      Top             =   1920
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "G/N"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   7680
      TabIndex        =   26
      Top             =   2640
      Width           =   1335
      Begin VB.OptionButton OptnGross 
         Caption         =   "Gross"
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   240
         Width           =   735
      End
      Begin VB.OptionButton OptnNet 
         Caption         =   "Net"
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   600
         Width           =   615
      End
      Begin VB.OptionButton OptnGNUnknown 
         Caption         =   "Unknown"
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   960
         Width           =   1095
      End
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "&Exit"
      Height          =   255
      Left            =   8400
      TabIndex        =   32
      Top             =   1320
      Width           =   1335
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "&Close Port"
      Height          =   255
      Left            =   8400
      TabIndex        =   31
      Top             =   960
      Width           =   1335
   End
   Begin VB.CommandButton cmdOpen 
      Caption         =   "&Open Port"
      Height          =   255
      Left            =   8400
      TabIndex        =   30
      Top             =   600
      Width           =   1335
   End
   Begin VB.Frame frmUOM 
      Caption         =   "Unit Of Measure"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   5040
      TabIndex        =   19
      Top             =   2640
      Width           =   2535
      Begin VB.OptionButton OptnUnknownUOM 
         Caption         =   "Unknown"
         Height          =   255
         Left            =   1320
         TabIndex        =   25
         Top             =   960
         Width           =   1095
      End
      Begin VB.OptionButton OptnOunces 
         Caption         =   "Ounces"
         Height          =   255
         Left            =   1320
         TabIndex        =   24
         Top             =   600
         Width           =   1095
      End
      Begin VB.OptionButton OptnGrams 
         Caption         =   "Grams"
         Height          =   255
         Left            =   1320
         TabIndex        =   23
         Top             =   240
         Width           =   1095
      End
      Begin VB.OptionButton OptnTons 
         Caption         =   "Tons"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   960
         Width           =   1095
      End
      Begin VB.OptionButton OptnKilograms 
         Caption         =   "Kilograms"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   600
         Width           =   1095
      End
      Begin VB.OptionButton OptnPounds 
         Caption         =   "Pounds"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame frmSign 
      Caption         =   "Polarity"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   3600
      TabIndex        =   15
      Top             =   2640
      Width           =   1335
      Begin VB.OptionButton OptnUnknown 
         Caption         =   "Unknown"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   960
         Width           =   1095
      End
      Begin VB.OptionButton OptnNegative 
         Caption         =   "Negative"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   600
         Width           =   1095
      End
      Begin VB.OptionButton OptnPositive 
         Caption         =   "Positive"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.TextBox txtStatus 
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   8
      Top             =   1320
      Width           =   6375
   End
   Begin VB.TextBox txtWt 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   14
      Top             =   2760
      Width           =   1815
   End
   Begin VB.TextBox txtRaw 
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   10
      Top             =   1680
      Width           =   6375
   End
   Begin VB.TextBox txtWarning 
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   960
      Width           =   6375
   End
   Begin VB.TextBox txtError 
      BackColor       =   &H80000011&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   4
      Top             =   600
      Width           =   6375
   End
   Begin VB.Label Label6 
      Caption         =   "Cleaned Output:"
      Height          =   255
      Left            =   240
      TabIndex        =   11
      Top             =   2040
      Width           =   1455
   End
   Begin VB.Label Label5 
      Caption         =   "Status Change:"
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   1320
      Width           =   1455
   End
   Begin VB.Label Label4 
      Caption         =   "Scale Weight:"
      Height          =   255
      Left            =   240
      TabIndex        =   13
      Top             =   2760
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "Raw Scale Output:"
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   1680
      Width           =   1455
   End
   Begin VB.Label Label2 
      Caption         =   "Warnings:"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   960
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Errors:"
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   600
      Width           =   1455
   End
End
Attribute VB_Name = "frmTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents SGSComm As clsScaleComm
Attribute SGSComm.VB_VarHelpID = -1

Sub ClearOutput()
    On Error Resume Next
    
    txtError.Text = Empty
    txtWarning.Text = Empty
    txtStatus.Text = Empty
    txtRaw.Text = Empty
    txtCleaned.Text = Empty
    txtWt.Text = Empty
    
    OptnPositive.Value = False
    OptnNegative.Value = False
    OptnUnknown.Value = False
    
    OptnPounds.Value = False
    OptnKilograms.Value = False
    OptnTons.Value = False
    OptnGrams.Value = False
    OptnOunces.Value = False
    OptnUnknownUOM.Value = False
    
    OptnGross.Value = False
    OptnNet.Value = False
    OptnGNUnknown.Value = False
    
    Err.Clear
End Sub

Private Sub chLog_Click()
    If chLog.Value = vbChecked Then
        SGSComm.LogOutput = True
    Else
        SGSComm.LogOutput = False
    End If
End Sub

Private Sub cmdClose_Click()
    On Error Resume Next
    
    SGSComm.ClosePort
    
End Sub

Private Sub cmdExit_Click()
    Unload Me
End Sub

Private Sub cmdLKUManual_Click()
    On Error GoTo Error
    
    Dim ErrMSG As String
    
    Call UpdateASCIICode(txtManual)
            
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdLKUManual_Click()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    SGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"


End Sub

Private Sub UpdateASCIICode(ByRef txtBox As TextBox)
    On Error GoTo Error
    
    Dim ASCIICode As Integer
    Dim ErrMSG As String
    
    ASCIICode = -1
    
    If txtBox.MaxLength = 1 Then
        If Len(txtBox.Text) = 1 Then
            ASCIICode = Asc(txtBox.Text)
        End If
        
        ASCIICode = SGSComm.LookupASCII(ASCIICode)
        
        If ASCIICode = -1 Then
            Exit Sub
        Else
            txtBox.Text = Chr(ASCIICode)
        End If
    
    ElseIf Len(txtBox.Text) = txtBox.MaxLength = 1 Then
        ASCIICode = Asc(Right(txtBox.Text, 1))
        
        ASCIICode = SGSComm.LookupASCII(ASCIICode)
        
        If ASCIICode = -1 Then
            Exit Sub
        Else
            txtBox.Text = Mid(txtBox.Text, 1, txtBox.MaxLength - 1) & Chr(ASCIICode)
        End If
    
    Else
        ASCIICode = SGSComm.LookupASCII(ASCIICode)
        
        If ASCIICode = -1 Then
            Exit Sub
        Else
            txtBox.Text = txtBox.Text + Chr(ASCIICode)
        End If
    
    End If
    
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".UpdateASCIICode()" & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    SGSComm.ErrorMessage (ErrMSG)
    
    MsgBox ErrMSG, vbCritical, "Error"

End Sub


Private Sub cmdManual_Click()
    On Error GoTo Error
    
    ClearOutput
    
    Call SGSComm.Test_OnComm(txtManual.Text, comEvReceive)
    
'    If gbContinious = 0 Then
'        Call SGSComm.Process_ReqstOnComm(txtManual.Text, comEvReceive)
'    Else
'        Call SGSComm.Process_AutoOnComm(txtManual.Text, comEvReceive)
'    End If
    
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdManual_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
End Sub


Private Sub cmdOpen_Click()
    On Error Resume Next
    
    ClearOutput
    
    SGSComm.OpenPort
    
End Sub

Private Sub cmdProperties_Click()
    Dim str As String
    
    str = str & "InBufferSize = " & SGSComm.MSComm.InBufferSize
    str = str & "RThreshold = " & SGSComm.MSComm.RThreshold
    str = str & "InputLen = " & SGSComm.MSComm.InputLen
    str = str & "OutBufferSize = " & SGSComm.MSComm.OutBufferSize
    str = str & "SThreshold = " & SGSComm.MSComm.SThreshold
    str = str & "EOFEnable = " & SGSComm.MSComm.EOFEnable
    str = str & "ParityReplace = " & SGSComm.MSComm.ParityReplace
    str = str & "NullDiscard = " & SGSComm.MSComm.NullDiscard
    str = str & "RTSEnable = " & SGSComm.MSComm.RTSEnable
    str = str & "DTREnable = " & SGSComm.MSComm.DTREnable
    
    MsgBox str, vbInformation, "Some Properties"
End Sub

Private Sub cmdSend_Click()
    On Error GoTo Error
    
    If SGSComm.SendData(txtManual.Text) = True Then
'        MsgBox "The data was sent successfully.", vbOKOnly, "Success"
    Else
        MsgBox "The data was NOT sent successfully.", vbOKOnly, "Failed"
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSend_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
End Sub

Private Sub Form_Load()
    On Error Resume Next
    Set SGSComm = New clsScaleComm
    SGSComm.ParentAppTitle = AppTitle
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error Resume Next
    
    SGSComm.ClosePort
    Set SGSComm = Nothing
End Sub


Private Sub SGSComm_CleanedScaleOutput(Data As Variant)
    txtCleaned.Text = Data
End Sub

Private Sub SGSComm_Error(ErrMSG As String)
    txtError.Text = ErrMSG
End Sub


Private Sub SGSComm_RawScaleOutput(Data As Variant)
    txtRaw.Text = Data
End Sub


Private Sub SGSComm_ScaleWeight(Weight As Double, Sign As Polarity, UnitOfMeasure As WeightType, GN As GrossOrNet)
    On Error GoTo Error
    
    txtWt.Text = Weight
    
    Select Case Sign
        Case Polarity.Negative
            OptnNegative = True
        Case Polarity.Positive
            OptnPositive = True
        Case Polarity.Unknown
            OptnUnknown = True
        Case Else
            OptnUnknown = True
    End Select
    
    Select Case UnitOfMeasure
        Case WeightType.Grams
            OptnGrams = True
        Case WeightType.Kilograms
            OptnKilograms = True
        Case WeightType.Ounces
            OptnOunces = True
        Case WeightType.Pounds
            OptnPounds = True
        Case WeightType.Tons
            OptnTons = True
        Case WeightType.Unknown
            OptnUnknownUOM = True
        Case Else
            OptnUnknownUOM = True
    End Select
    
    Select Case GN
        Case GrossOrNet.Gross
            OptnGross = True
        Case GrossOrNet.Net
            OptnNet = True
        Case GrossOrNet.Unknown
            OptnGNUnknown = True
        Case Else
            OptnGNUnknown = True
    End Select
    
    Exit Sub
Error:
    MsgBox Me.Name & ".SGSComm_ScaleWeight(Weight As Double, Sign As Polarity, UnitOfMeasure As WeightType, GN As GrossOrNet)" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
End Sub

Private Sub SGSComm_StatusChange(StatusStr As String)
    txtStatus.Text = StatusStr
End Sub

Private Sub SGSComm_Warning(WarningMsg As String)
    txtWarning.Text = WarningMsg
End Sub
