VERSION 5.00
Begin VB.Form frmGetASCII 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ASCII Codes"
   ClientHeight    =   1350
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4905
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1350
   ScaleWidth      =   4905
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   2640
      TabIndex        =   3
      Top             =   840
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   960
      TabIndex        =   2
      Top             =   840
      Width           =   1215
   End
   Begin VB.ComboBox cmboASCII 
      Height          =   315
      Left            =   240
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   360
      Width           =   4455
   End
   Begin VB.Label Label3 
      Caption         =   "Dec   -   Hex   -   Oct   -   Char"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   120
      Width           =   4335
   End
End
Attribute VB_Name = "frmGetASCII"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private pASCII As Integer

Public Property Get ASCII() As Integer
    ASCII = pASCII
End Property


Public Property Let ASCII(ASCIICode As Integer)
    On Error Resume Next
    
    pASCII = ASCIICode
    
    cmboASCII.ListIndex = ASCIICode
    
    Err.Clear
    
End Property



Private Sub cmdCancel_Click()
    Unload Me
End Sub


Private Sub cmdOK_Click()
    pASCII = cmboASCII.ListIndex
    Unload Me
End Sub

Private Sub Form_Load()
    pASCII = -1
End Sub
