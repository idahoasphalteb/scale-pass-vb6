Attribute VB_Name = "modSaveSettingDB"
Option Explicit

Global gbConn As New ADODB.Connection
'Global pMSComm As MSCommLib.MSComm
Private pSGSCom As clsScaleComm

Public Property Set SGSCom(ByRef obj As clsScaleComm)
    On Error GoTo Error
    Dim ErrMSG
    
    Set pSGSCom = obj
    
    Exit Property
Error:
    ErrMSG = ErrMSG & "modSaveSettingDB.SGSCom(ByRef obj As clsScaleComm)" & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    MsgBox ErrMSG, vbCritical, "Error"
End Property


Public Function ConvertToString(Val As Variant, DefaultVal As String) As String
    On Error GoTo Error
    
    If IsNull(Val) = True Then
        ConvertToString = DefaultVal
        Exit Function
    End If
    
'    Debug.Print TypeName(Val)
    
    Select Case VarType(Val)
        Case VbVarType.vbArray
            ConvertToString = ConvertToString(Val(0), Empty)
        
        Case VbVarType.vbBoolean
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbByte
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbCurrency
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbDataObject
            ConvertToString = Empty
        
        Case VbVarType.vbDate
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbDecimal
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbDouble
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbEmpty
            ConvertToString = Empty
        
        Case VbVarType.vbError
            ConvertToString = Val.Number & " " & Val.Description
        
        Case VbVarType.vbInteger
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbLong
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbNull
            ConvertToString = Empty
        
        Case VbVarType.vbObject
            ConvertToString = Empty
            
        Case VbVarType.vbSingle
            ConvertToString = Empty
        
        Case VbVarType.vbString
            ConvertToString = Val
        
        Case VbVarType.vbUserDefinedType
            ConvertToString = Empty
        
        Case VbVarType.vbVariant
            ConvertToString = CStr(Val)
        
        Case Else
            If IsArray(Val) = True Then
                ConvertToString = ConvertToString(Val(0), Empty)
            Else
                ConvertToString = Empty
            End If
    
    End Select
        
    
    Exit Function
Error:
    MsgBox "ConvertToString(Val As Variant) As String" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    Err.Clear
    
End Function

Private Function CreateScaleSettingsTbl() As Boolean
    On Error GoTo Error
    
    Dim sSQL As String
    
    If IsConnOpen() = False Then
        'Not a valid connection
        Exit Function
    End If
    
    'Table Name: ScaleSettings
    sSQL = "Create Table ScaleSettings ( "
    
    sSQL = sSQL & "[Continious] Text(100) Null, "
    sSQL = sSQL & "[ENQ] Text(100) Null, "
    sSQL = sSQL & "[RequireACK] Text(100) Null, "
    sSQL = sSQL & "[ACK] Text(100) Null, "
    sSQL = sSQL & "[LenOfInput] Text(100) Null, "
    sSQL = sSQL & "[STX] Text(100) Null, "
    sSQL = sSQL & "[TERM] Text(100) Null, "
    sSQL = sSQL & "[PolStartPos] Text(100) Null, "
    sSQL = sSQL & "[PolLen] Text(100) Null, "
    sSQL = sSQL & "[PolPosChars] Text(100) Null, "
    sSQL = sSQL & "[PolNegChars] Text(100) Null, "
    sSQL = sSQL & "[WtStartPos] Text(100) Null, "
    sSQL = sSQL & "[WtLen] Text(100) Null, "
    sSQL = sSQL & "[WtPadChar] Text(100) Null, "
    sSQL = sSQL & "[WtDecChar] Text(100) Null, "
    sSQL = sSQL & "[UOMStartPos] Text(100) Null, "
    sSQL = sSQL & "[UOMLen] Text(100) Null, "
    sSQL = sSQL & "[UOMLbsChars] Text(100) Null, "
    sSQL = sSQL & "[UOMKGChars] Text(100) Null, "
    sSQL = sSQL & "[UOMTonChars] Text(100) Null, "
    sSQL = sSQL & "[UOMGrmChars] Text(100) Null, "
    sSQL = sSQL & "[UOMOncChars] Text(100) Null, "
    sSQL = sSQL & "[GNStartPos] Text(100) Null, "
    sSQL = sSQL & "[GNLen] Text(100) Null, "
    sSQL = sSQL & "[GNGrossChars] Text(100) Null, "
    sSQL = sSQL & "[GNNetChars] Text(100) Null "
    sSQL = sSQL & ") "

    gbConn.Execute sSQL
    
    CreateScaleSettingsTbl = True
    
    Exit Function
Error:
    Err.Clear
    CreateScaleSettingsTbl = False
End Function




Private Function CreateCommSettingsTbl() As Boolean
    On Error GoTo Error
    
    Dim sSQL As String
    
    If IsConnOpen() = False Then
        'Not a valid connection
        Exit Function
    End If
    
    'Table Name: CommSettings
    sSQL = "Create Table CommSettings ( "
    
    sSQL = sSQL & "[Settings] Text(100) Null, "
    sSQL = sSQL & "[CommPort] Text(100) Null, "
    sSQL = sSQL & "[Handshaking] Text(100) Null, "
    sSQL = sSQL & "[Echo] Text(100) Null, "
    sSQL = sSQL & "[InputBufferSize] Text(100) Null, "
    sSQL = sSQL & "[RThreshold] Text(100) Null, "
    sSQL = sSQL & "[InputLen] Text(100) Null, "
    sSQL = sSQL & "[OutBufferSize] Text(100) Null, "
    sSQL = sSQL & "[SThreshold] Text(100) Null, "
    sSQL = sSQL & "[EOFEnable] Text(100) Null, "
    sSQL = sSQL & "[ParityReplace] Text(100) Null, "
    sSQL = sSQL & "[NullDiscard] Text(100) Null, "
    sSQL = sSQL & "[RTSEnable] Text(100) Null, "
    sSQL = sSQL & "[DTREnable] Text(100) Null "

    sSQL = sSQL & ") "

    gbConn.Execute sSQL
    
    CreateCommSettingsTbl = True
    
    Exit Function
Error:
    Err.Clear
    CreateCommSettingsTbl = False
End Function


Function DoesTableExist(TableName As String) As Boolean
    On Error GoTo Error
    
    Dim sSQL As String
    Dim RS As ADODB.Recordset
    
    If IsConnOpen() = False Then
        'Not a valid connection
        Exit Function
    End If
    
    sSQL = "Select * From [" & TableName & "] "
    
    Set RS = gbConn.Execute(sSQL)
    
    'If we make it to hear then the table exists
    DoesTableExist = True

CleanUp:
    If RS.State = 1 Then
        RS.Close
    End If
    
    Set RS = Nothing
    
    Exit Function
Error:
    Err.Clear
    DoesTableExist = False
End Function


Public Sub GetCommSettingsDB()
    On Error Resume Next
    
    Dim CommPort As String, Handshaking As String, Settings As String
    Dim RtnVal As String
    
    Dim RS As ADODB.Recordset
    Dim sSQL As String
    
    If IsConnOpen() = False Then
        'Not a valid connection
        Exit Sub
    End If
    
    If DoesTableExist("CommSettings") = False Then
        'Need to create the table
        If CreateCommSettingsTbl() = False Then
            'The table was not created
            Exit Sub
        End If
    End If
    
    sSQL = "Select * From CommSettings "
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenDynamic
        
    Set RS = gbConn.Execute(sSQL)
        
    
    If RS.RecordCount <= 0 Then
        'No Records so create the record by doing a save
        
        'This means the data has not ever been set to set defaults (pull from Registry)
        'We pull from the registry first so the settings have already ben gotten
        'We need to create and save the record. then exit
        
        Call SaveCommSettingsDB
        Exit Sub
    End If
    
    RS.movefirst
    
    ' Load Database Settings
    
    Settings = ConvertToString(RS("Settings").Value, "")
    If Settings <> "" Then
        pSGSCom.MSComm.Settings = Settings
        If Err Then
            MsgBox Error$, 48
            Exit Sub
        End If
    End If
    
    CommPort = ConvertToString(RS("CommPort").Value, "")
    If CommPort <> "" Then pSGSCom.MSComm.CommPort = CommPort
    
    Handshaking = ConvertToString(RS("Handshaking").Value, "")
    If Handshaking <> "" Then
        pSGSCom.MSComm.Handshaking = Handshaking
        If Err Then
            MsgBox Error$, 48
            Exit Sub
        End If
    End If
    
    Echo = ConvertToString(RS("Echo").Value, "")
    
    
    '========================================================================================================
    '========================================================================================================
    '           Input Buffer Size
    '========================================================================================================
    '========================================================================================================
    RtnVal = ConvertToString(RS("InputBufferSize").Value, gcInputBufferSize)
'    gbInputBufferSize = RtnVal
    pSGSCom.MSComm.InBufferSize = CLng(RtnVal)
    
    '========================================================================================================
    '========================================================================================================
    '           R Threshold
    '========================================================================================================
    '========================================================================================================
    RtnVal = ConvertToString(RS("RThreshold").Value, gcRThreshold)
'    gbRThreshold = RtnVal
    pSGSCom.MSComm.RThreshold = CLng(RtnVal)
    
    '========================================================================================================
    '========================================================================================================
    '           Input Len
    '========================================================================================================
    '========================================================================================================
    RtnVal = ConvertToString(RS("InputLen").Value, gcInputLen)
'    gbInputLen = RtnVal
    pSGSCom.MSComm.InputLen = CLng(RtnVal)
    
    '========================================================================================================
    '========================================================================================================
    '           Out Buffer Size
    '========================================================================================================
    '========================================================================================================
    RtnVal = ConvertToString(RS("OutBufferSize").Value, gcOutBufferSize)
'    gbOutBufferSize = RtnVal
    pSGSCom.MSComm.OutBufferSize = CLng(RtnVal)
    
    '========================================================================================================
    '========================================================================================================
    '           S Threshold
    '========================================================================================================
    '========================================================================================================
    RtnVal = ConvertToString(RS("SThreshold").Value, gcSThreshold)
'    gbSThreshold = RtnVal
    pSGSCom.MSComm.SThreshold = CLng(RtnVal)
    
    '========================================================================================================
    '========================================================================================================
    '           EOF Enable
    '========================================================================================================
    '========================================================================================================
    RtnVal = ConvertToString(RS("EOFEnable").Value, gcEOFEnable)
'    gbEOFEnable = RtnVal
    pSGSCom.MSComm.EOFEnable = CBool(RtnVal)
    
    '========================================================================================================
    '========================================================================================================
    '           Parity Replace
    '========================================================================================================
    '========================================================================================================
    RtnVal = ConvertToString(RS("ParityReplace").Value, gcParityReplace)
'    gbParityReplace = RtnVal
    pSGSCom.MSComm.ParityReplace = RtnVal
    
    '========================================================================================================
    '========================================================================================================
    '           Null Discard
    '========================================================================================================
    '========================================================================================================
    RtnVal = ConvertToString(RS("NullDiscard").Value, gcNullDiscard)
'    gbNullDiscard = RtnVal
    pSGSCom.MSComm.NullDiscard = CBool(RtnVal)
    
    '========================================================================================================
    '========================================================================================================
    '           RTS Enable
    '========================================================================================================
    '========================================================================================================
    RtnVal = ConvertToString(RS("RTSEnable").Value, gcRTSEnable)
'    gbRTSEnable = RtnVal
    pSGSCom.MSComm.RTSEnable = CBool(RtnVal)
    
    '========================================================================================================
    '========================================================================================================
    '           DTR Enable
    '========================================================================================================
    '========================================================================================================
    RtnVal = ConvertToString(RS("DTREnable").Value, gcDTREnable)
'    gbDTREnable = RtnVal
    pSGSCom.MSComm.DTREnable = CBool(RtnVal)
        
    On Error GoTo 0
    
CleanUp:
    If RS.State = 1 Then
        RS.Close
    End If
    
    Set RS = Nothing
    
    Err.Clear

End Sub


Public Sub GetParseSettingsDB()
    On Error Resume Next
    
    ' Load Registry Settings
    
    Dim RtnVal As String
    Dim Val As String
    
    Dim RS As ADODB.Recordset
    Dim sSQL As String
    
    If IsConnOpen() = False Then
        'Not a valid connection
        Exit Sub
    End If
    
    If DoesTableExist("ScaleSettings") = False Then
        'Need to create the table
        If CreateScaleSettingsTbl() = False Then
            'The table was not created
            Exit Sub
        End If
    End If
    
    sSQL = "Select * From ScaleSettings "
    
    Set RS = gbConn.Execute(sSQL)
    
    If RS.RecordCount <= 0 Then
        'No Records so create the record by doing a save
        
        'This means the data has not ever been set to set defaults (pull from Registry)
        'We pull from the registry first so the settings have already ben gotten
        'We need to create and save the record. then exit
        
        Call SaveParseSettingsDB
        Exit Sub
    End If
    
    RS.movefirst
    
    'ScaleSettings
    '**************************************************************************************************
    '**************************************************************************************************
    '       Continuous Feed / Enquiry
    '**************************************************************************************************
    '**************************************************************************************************
    RtnVal = ConvertToString(RS("Continious").Value, "1")
    If IsNumeric(RtnVal) Then
        gbContinious = CLng(RtnVal)
    Else
        gbContinious = 1
    End If
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Enquiry ENQ String
    '**************************************************************************************************
    '**************************************************************************************************
    RtnVal = ConvertToString(RS("ENQ").Value, "5")
    gbENQ = RtnVal
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Require ACK
    '**************************************************************************************************
    '**************************************************************************************************
    RtnVal = ConvertToString(RS("RequireACK").Value, "0")
    If IsNumeric(RtnVal) Then
        gbRequireACK = CLng(RtnVal)
    Else
        gbRequireACK = 0
    End If
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Acknowledgement ACK string
    '**************************************************************************************************
    '**************************************************************************************************
    RtnVal = ConvertToString(RS("ACK").Value, "6")
    gbACK = RtnVal
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Input Length
    '**************************************************************************************************
    '**************************************************************************************************
    RtnVal = ConvertToString(RS("LenOfInput").Value, "13")
    If IsNumeric(RtnVal) Then
        gbLenOfInput = CLng(RtnVal)
    Else
        gbLenOfInput = 13
    End If
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Start of Text (STX)
    '**************************************************************************************************
    '**************************************************************************************************
    Val = Chr(2)
    
    RtnVal = ConvertToString(RS("STX").Value, Val)
    gbSTX = RtnVal
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Terminator (TERM)
    '**************************************************************************************************
    '**************************************************************************************************
    Val = vbCrLf
    
    RtnVal = ConvertToString(RS("TERM").Value, Val)
    gbTerm = RtnVal
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Polarity Information
    '**************************************************************************************************
    '**************************************************************************************************
    RtnVal = ConvertToString(RS("PolStartPos").Value, "1")
    If IsNumeric(RtnVal) Then
        gbPolStartPos = CLng(RtnVal)
    Else
        gbPolStartPos = 1
    End If
    
    RtnVal = ConvertToString(RS("PolLen").Value, "1")
    If IsNumeric(RtnVal) Then
        gbPolLen = CLng(RtnVal)
    Else
        gbPolLen = 1
    End If
    
    RtnVal = ConvertToString(RS("PolPosChars").Value, " ")
    gbPolPosChars = RtnVal
    
    RtnVal = ConvertToString(RS("PolNegChars").Value, "-")
    gbPolNegChars = RtnVal
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Weight Information
    '**************************************************************************************************
    '**************************************************************************************************
    RtnVal = ConvertToString(RS("WtStartPos").Value, "2")
    If IsNumeric(RtnVal) Then
        gbWtStartPos = CLng(RtnVal)
    Else
        gbWtStartPos = 2
    End If
    
    RtnVal = ConvertToString(RS("WtLen").Value, "7")
    If IsNumeric(RtnVal) Then
        gbWtLen = CLng(RtnVal)
    Else
        gbWtLen = 7
    End If
    
    RtnVal = ConvertToString(RS("WtPadChar").Value, " ")
    gbWtPadChar = RtnVal
    
    RtnVal = ConvertToString(RS("WtDecChar").Value, ".")
    gbWtDecChar = RtnVal
        
    '**************************************************************************************************
    '**************************************************************************************************
    '       Unit Of Measure Information
    '**************************************************************************************************
    '**************************************************************************************************
    RtnVal = ConvertToString(RS("UOMStartPos").Value, "10")
    If IsNumeric(RtnVal) Then
        gbUOMStartPos = CLng(RtnVal)
    Else
        gbUOMStartPos = 9
    End If
    
    RtnVal = ConvertToString(RS("UOMLen").Value, "1")
    If IsNumeric(RtnVal) Then
        gbUOMLen = CLng(RtnVal)
    Else
        gbUOMLen = 1
    End If
    
    RtnVal = ConvertToString(RS("UOMLbsChars").Value, "L")
    gbUOMLbsChars = RtnVal
        
    RtnVal = ConvertToString(RS("UOMKGChars").Value, "K")
    gbUOMKGChars = RtnVal
        
    RtnVal = ConvertToString(RS("UOMTonChars").Value, "T")
    gbUOMTonChars = RtnVal
        
    RtnVal = ConvertToString(RS("UOMGrmChars").Value, "G")
    gbUOMGrmChars = RtnVal
        
    RtnVal = ConvertToString(RS("UOMOncChars").Value, "O")
    gbUOMOncChars = RtnVal
            
    '**************************************************************************************************
    '**************************************************************************************************
    '       Gross / Net Information
    '**************************************************************************************************
    '**************************************************************************************************
    RtnVal = ConvertToString(RS("GNStartPos").Value, "11")
    If IsNumeric(RtnVal) Then
        gbGNStartPos = CLng(RtnVal)
    Else
        gbGNStartPos = 11
    End If
    
    RtnVal = ConvertToString(RS("GNLen").Value, "1")
    If IsNumeric(RtnVal) Then
        gbGNLen = CLng(RtnVal)
    Else
        gbGNLen = 1
    End If
    
    RtnVal = ConvertToString(RS("GNGrossChars").Value, "G")
    gbGNGrossChars = RtnVal
            
    RtnVal = ConvertToString(RS("GNNetChars").Value, "N")
    gbGNNetChars = RtnVal
    
    
    On Error GoTo 0
CleanUp:
    If RS.State = 1 Then
        RS.Close
    End If
    
    Set RS = Nothing
    
    Err.Clear

End Sub


Public Sub SaveParseSettingsDB()
'Public Sub SaveCommSettingsDB()
    On Error Resume Next
    
    Dim RS As New ADODB.Recordset
    Dim sSQL As String
    
    If IsConnOpen() = False Then
        'Not a valid connection
        Exit Sub
    End If
    
    If DoesTableExist("ScaleSettings") = False Then
        'Need to create the table
        If CreateScaleSettingsTbl() = False Then
            'The table was not created
            Exit Sub
        End If
    End If
    
    sSQL = "Select * From ScaleSettings "
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenDynamic
    RS.LockType = adLockBatchOptimistic
   
    RS.Open sSQL, gbConn, adOpenDynamic, adLockBatchOptimistic
    
    If RS.RecordCount <= 0 Then
        RS.AddNew
    Else
        RS.movefirst
    End If
    
    
    'Write to the ini file
    '**************************************************************************************************
    '**************************************************************************************************
    '       Continuous Feed / Enquiry
    '**************************************************************************************************
    '**************************************************************************************************
    RS("Continious").Value = CStr(gbContinious)
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Enquiry ENQ String
    '**************************************************************************************************
    '**************************************************************************************************
    RS("ENQ").Value = CStr(gbENQ)
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Require ACK
    '**************************************************************************************************
    '**************************************************************************************************
    RS("RequireACK").Value = CStr(gbRequireACK)
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Acknowledgement ACK string
    '**************************************************************************************************
    '**************************************************************************************************
    RS("ACK").Value = CStr(gbACK)
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Input Length
    '**************************************************************************************************
    '**************************************************************************************************
    RS("LenOfInput").Value = CStr(gbLenOfInput)

    '**************************************************************************************************
    '**************************************************************************************************
    '       Start of Text (STX)
    '**************************************************************************************************
    '**************************************************************************************************
    RS("STX").Value = CStr(gbSTX)
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Terminator (TERM)
    '**************************************************************************************************
    '**************************************************************************************************
    RS("TERM").Value = CStr(gbTerm)
    
    '**************************************************************************************************
    '**************************************************************************************************
    '       Polarity Information
    '**************************************************************************************************
    '**************************************************************************************************
    RS("PolStartPos").Value = CStr(gbPolStartPos)
    RS("PolLen").Value = CStr(gbPolLen)
    RS("PolPosChars").Value = gbPolPosChars
    RS("PolNegChars").Value = gbPolNegChars

    '**************************************************************************************************
    '**************************************************************************************************
    '       Weight Information
    '**************************************************************************************************
    '**************************************************************************************************
    RS("WtStartPos").Value = CStr(gbWtStartPos)
    RS("WtLen").Value = CStr(gbWtLen)
    RS("WtPadChar").Value = gbWtPadChar
    RS("WtDecChar").Value = gbWtDecChar

    '**************************************************************************************************
    '**************************************************************************************************
    '       Unit Of Measure Information
    '**************************************************************************************************
    '**************************************************************************************************
    RS("UOMStartPos").Value = CStr(gbUOMStartPos)
    RS("UOMLen").Value = CStr(gbUOMLen)
    RS("UOMLbsChars").Value = gbUOMLbsChars
    RS("UOMKGChars").Value = gbUOMKGChars
    RS("UOMTonChars").Value = gbUOMTonChars
    RS("UOMGrmChars").Value = gbUOMGrmChars
    RS("UOMOncChars").Value = gbUOMOncChars

    '**************************************************************************************************
    '**************************************************************************************************
    '       Gross / Net Information
    '**************************************************************************************************
    '**************************************************************************************************
    RS("GNStartPos").Value = CStr(gbGNStartPos)
    RS("GNLen").Value = CStr(gbGNLen)
    RS("GNGrossChars").Value = gbGNGrossChars
    RS("GNNetChars").Value = gbGNNetChars

    RS.Update
    RS.UpdateBatch
    RS.Close
    Set RS = Nothing
    
    Err.Clear
    
End Sub


Public Sub SaveCommSettingsDB()
'Public Sub SaveParseSettingsDB()
    On Error Resume Next
    
'    Dim OldPort As Integer
'    Dim ReOpen As Boolean
'    Dim NewPort As Variant
    
    Dim RS As New ADODB.Recordset
    Dim sSQL As String
    
    If IsConnOpen() = False Then
        'Not a valid connection
        Exit Sub
    End If
    
    If DoesTableExist("CommSettings") = False Then
        'Need to create the table
        If CreateCommSettingsTbl() = False Then
            'The table was not created
            Exit Sub
        End If
    End If
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenDynamic
    RS.LockType = adLockBatchOptimistic
    
    
    sSQL = "Select * From CommSettings "
    
    RS.Open sSQL, gbConn, adOpenDynamic, adLockBatchOptimistic
            
    If RS.RecordCount <= 0 Then
        RS.AddNew
    Else
        RS.movefirst
    End If
        
    RS("Settings").Value = pSGSCom.MSComm.Settings
    RS("CommPort").Value = pSGSCom.MSComm.CommPort
    RS("Handshaking").Value = pSGSCom.MSComm.Handshaking
    RS("Echo").Value = Echo
'    RS("Echo").Value = pSGSCom.MSComm.Echo
       
    RS("InputBufferSize").Value = pSGSCom.MSComm.InBufferSize
    RS("RThreshold").Value = pSGSCom.MSComm.RThreshold
    RS("InputLen").Value = pSGSCom.MSComm.InputLen
    RS("OutBufferSize").Value = pSGSCom.MSComm.OutBufferSize
    RS("SThreshold").Value = pSGSCom.MSComm.SThreshold
    RS("EOFEnable").Value = pSGSCom.MSComm.EOFEnable
    RS("ParityReplace").Value = pSGSCom.MSComm.ParityReplace
    RS("NullDiscard").Value = pSGSCom.MSComm.NullDiscard
    RS("RTSEnable").Value = pSGSCom.MSComm.RTSEnable
    RS("DTREnable").Value = pSGSCom.MSComm.DTREnable
    
    RS.Update
    RS.UpdateBatch
    RS.Close
    Set RS = Nothing

    Err.Clear
    
End Sub

Public Function IsConnOpen() As Boolean
    On Error GoTo Error
    
    If gbConn.State <> 1 Then
        IsConnOpen = False
    Else
        IsConnOpen = True
    End If
    
    Exit Function
Error:
    Err.Clear
    
    IsConnOpen = False
End Function


