VERSION 5.00
Begin VB.Form frmSetup 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Setup Scale Interface"
   ClientHeight    =   2310
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   2100
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2310
   ScaleWidth      =   2100
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdExit 
      Caption         =   "&Exit"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   1800
      Width           =   1815
   End
   Begin VB.CommandButton cmdTest 
      Caption         =   "Test"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   1080
      Width           =   1815
   End
   Begin VB.CommandButton cmdParse 
      Caption         =   "Setup Data Parsing"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   1815
   End
   Begin VB.CommandButton cmdCom 
      Caption         =   "Setup Com/Terminal"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1815
   End
End
Attribute VB_Name = "frmSetup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents pSGSCom As clsScaleComm
Attribute pSGSCom.VB_VarHelpID = -1

Private Sub cmdCom_Click()
    On Error GoTo Error
    
    Set frmProperties.SGSCom = pSGSCom
    
    frmProperties.Show vbModal, Me
'    frmProperties.Show vbModeless, Me
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdCom_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
End Sub

Private Sub cmdExit_Click()
    Unload Me
End Sub

Private Sub cmdParse_Click()
    On Error GoTo Error
    
    Set frmSetupParcing.SGSComm = pSGSCom
    
    frmSetupParcing.Show vbModal, Me
'    frmSetupParcing.Show vbModeless, Me
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdParse_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
End Sub

Private Sub cmdTest_Click()
    On Error GoTo Error
    
    frmTest.Show vbModal, Me
'    frmTest.Show vbModeless, Me
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdTest_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
End Sub


Friend Property Set SGSCom(obj As clsScaleComm)
    On Error GoTo Error
    
    Set pSGSCom = obj
    
    Exit Property
Error:
    MsgBox Me.Name & ".SGSCom(obj As clsScaleComm)" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
End Property

