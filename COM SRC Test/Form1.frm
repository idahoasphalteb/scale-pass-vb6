VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Simulate Sending data from COMX to COMY"
   ClientHeight    =   9645
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11055
   LinkTopic       =   "Form1"
   ScaleHeight     =   9645
   ScaleWidth      =   11055
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chContinious 
      Caption         =   "Continuious Log ... keep data"
      Height          =   255
      Left            =   6360
      TabIndex        =   25
      ToolTipText     =   "When checked add current data to the existing data in the textbox... else only display current data"
      Top             =   2280
      Width           =   3135
   End
   Begin VB.CommandButton CmdUpd 
      Caption         =   "Update Conn"
      Height          =   255
      Left            =   3360
      TabIndex        =   24
      Top             =   3360
      Width           =   2055
   End
   Begin VB.TextBox txtConn 
      Height          =   375
      Left            =   240
      TabIndex        =   23
      Text            =   $"Form1.frx":0000
      Top             =   3600
      Width           =   10695
   End
   Begin VB.Frame frmLogOptions 
      Caption         =   "Dispaly Events Data"
      Height          =   1095
      Left            =   240
      TabIndex        =   15
      Top             =   2040
      Width           =   6015
      Begin VB.OptionButton OptnEvnt 
         Caption         =   "Warnings"
         Height          =   255
         Index           =   5
         Left            =   4320
         TabIndex        =   21
         Top             =   600
         Width           =   1575
      End
      Begin VB.OptionButton OptnEvnt 
         Caption         =   "Status Change"
         Height          =   255
         Index           =   4
         Left            =   4320
         TabIndex        =   20
         Top             =   240
         Width           =   1575
      End
      Begin VB.OptionButton OptnEvnt 
         Caption         =   "Parced Scale Weight"
         Height          =   255
         Index           =   3
         Left            =   2160
         TabIndex        =   19
         Top             =   600
         Width           =   1935
      End
      Begin VB.OptionButton OptnEvnt 
         Caption         =   "Raw COM Data"
         Height          =   255
         Index           =   2
         Left            =   2160
         TabIndex        =   18
         Top             =   240
         Value           =   -1  'True
         Width           =   1935
      End
      Begin VB.OptionButton OptnEvnt 
         Caption         =   "Errors"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   17
         Top             =   600
         Width           =   1815
      End
      Begin VB.OptionButton OptnEvnt 
         Caption         =   "Cleaned COM Data"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   16
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "DoEvents Interval"
      Height          =   1095
      Left            =   240
      TabIndex        =   8
      Top             =   840
      Width           =   2415
      Begin VB.OptionButton Optn10000 
         Caption         =   "10000"
         Height          =   255
         Left            =   1200
         TabIndex        =   14
         Top             =   720
         Width           =   855
      End
      Begin VB.OptionButton optn700 
         Caption         =   "700"
         Height          =   255
         Left            =   1200
         TabIndex        =   13
         Top             =   480
         Width           =   855
      End
      Begin VB.OptionButton optn200 
         Caption         =   "200"
         Height          =   255
         Left            =   1200
         TabIndex        =   12
         Top             =   240
         Width           =   855
      End
      Begin VB.OptionButton optn1000 
         Caption         =   "1000"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   720
         Width           =   855
      End
      Begin VB.OptionButton optn500 
         Caption         =   "500"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   480
         Width           =   855
      End
      Begin VB.OptionButton optn100 
         Caption         =   "100"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Value           =   -1  'True
         Width           =   855
      End
   End
   Begin VB.CommandButton cmdStopSend 
      Caption         =   "Stop Send"
      Height          =   255
      Left            =   6720
      TabIndex        =   7
      Top             =   360
      Width           =   1215
   End
   Begin VB.Timer Timer1 
      Interval        =   500
      Left            =   4680
      Top             =   1800
   End
   Begin VB.TextBox txtDataToSend 
      Height          =   855
      Left            =   4080
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   6
      Text            =   "Form1.frx":00C8
      Top             =   720
      Width           =   3735
   End
   Begin VB.CommandButton cmdSendData 
      Caption         =   "Send Data"
      Height          =   255
      Left            =   5400
      TabIndex        =   4
      Top             =   360
      Width           =   1215
   End
   Begin VB.CommandButton cmdClosePort 
      Caption         =   "Close Port"
      Height          =   255
      Left            =   3720
      TabIndex        =   3
      Top             =   360
      Width           =   1455
   End
   Begin VB.CommandButton cmdOpenPort 
      Caption         =   "Open Port"
      Height          =   255
      Left            =   2160
      TabIndex        =   2
      Top             =   360
      Width           =   1455
   End
   Begin VB.TextBox txtMsgs 
      Height          =   5175
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   1
      Top             =   4200
      Width           =   10815
   End
   Begin VB.CommandButton cmdSettings 
      Caption         =   "COM Settings"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   "Scale Pass Access Database/Connection"
      Height          =   255
      Left            =   240
      TabIndex        =   22
      Top             =   3360
      Width           =   3255
   End
   Begin VB.Label LblDataToSend 
      Caption         =   "Data to Send"
      Height          =   255
      Left            =   2880
      TabIndex        =   5
      Top             =   720
      Width           =   1095
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Dim DBConnStr As String ' = "PROVIDER=MSDASQL;Driver=Microsoft Access Driver (*.mdb);Server=;UID=;PWD=;User Id=;Jet OLEDB:Database Password=;Dbq=C:\Scale Pass 2009\ScalePass2009-COMTEsterSrc.mdb;Persist Security Info=False;"

Private WithEvents SGSCOM As SGSScaleComm.clsScaleComm
Attribute SGSCOM.VB_VarHelpID = -1

Private bSendData As Boolean


Private Sub cmdClosePort_Click()
    SGSCOM.ClosePort
End Sub

Private Sub cmdOpenPort_Click()
    SGSCOM.OpenPort
End Sub

Private Sub cmdSendData_Click()
    bSendData = True
    Timer1.Enabled = True
End Sub

Private Sub cmdSettings_Click()
    SGSCOM.Setup (Me)
    
End Sub

Private Sub cmdStopSend_Click()
    bSendData = False
    Timer1.Enabled = False

End Sub

Private Sub CmdUpd_Click()
On Error GoTo Error
'    Set SGSCOM = CreateObject("SGSScaleComm.clsScaleComm")
    DBConnStr = txtConn.Text
    
    SGSCOM.Conn = DBConnStr
    
    bSendData = False
    Timer1.Enabled = False
    Exit Sub
Error:
    MsgBox Me.Name & ".Form_Load()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation
    
    Err.Clear
End Sub

Private Sub Form_Load()
    On Error GoTo Error
    
    Set SGSCOM = CreateObject("SGSScaleComm.clsScaleComm")
    
'    SGSCOM.SettingsAppTitle = App.Title
    
    txtConn.Text = "PROVIDER=MSDASQL;Driver=Microsoft Access Driver (*.mdb);Server=;UID=;PWD=;User Id=;Jet OLEDB:Database Password=;Dbq=C:\Scale Pass 2009\ScalePass2009.mdb;Persist Security Info=False;"
    
    DBConnStr = txtConn.Text
    
    SGSCOM.Conn = DBConnStr
    
    bSendData = False
    Timer1.Enabled = False
    
    Exit Sub
Error:
    MsgBox Me.Name & ".Form_Load()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation
    
    Err.Clear
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error Resume Next
    SGSCOM.ClosePort
    Set SGSCOM = Nothing

End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    SGSCOM.ClosePort
    Set SGSCOM = Nothing
End Sub

Private Sub SGSCOM_CleanedScaleOutput(Data As Variant)
    Static cntr As Long
    
    If cntr > 60000 Then
        cntr = 0
    End If
    
    cntr = cntr + 1
    
    
    If OptnEvnt(0).Value = True Then
        If Len(txtMsgs.Text) > 69000 Then
            txtMsgs.Text = Empty
            DoEvents
        End If
        
        
        If chContinious.Value = vbChecked Then
            txtMsgs.Text = Data & vbCrLf & txtMsgs.Text
        Else
            txtMsgs.Text = "(" & cntr & ") " & Data
        End If
    End If

End Sub

Private Sub SGSCOM_Error(ErrMSG As String)
    Static cntr As Long
    
    If cntr > 60000 Then
        cntr = 0
    End If
    
    cntr = cntr + 1
    
    If OptnEvnt(1).Value = True Then
        If Len(txtMsgs.Text) > 69000 Then
            txtMsgs.Text = Empty
            DoEvents
        End If
        
        If chContinious.Value = vbChecked Then
            txtMsgs.Text = ErrMSG & vbCrLf & txtMsgs.Text
        Else
            txtMsgs.Text = "(" & cntr & ") " & ErrMSG
        End If
    End If
End Sub

Private Sub SGSCOM_RawScaleOutput(Data As Variant)
    Static cntr As Long
    
    If cntr > 60000 Then
        cntr = 0
    End If
    
    cntr = cntr + 1
    
    If OptnEvnt(2).Value = True Then
        If Len(txtMsgs.Text) > 69000 Then
            txtMsgs.Text = Empty
            DoEvents
        End If
        
        If chContinious.Value = vbChecked Then
            txtMsgs.Text = txtMsgs.Text & Data
        Else
            txtMsgs.Text = "(" & cntr & ") " & Data
        End If
    End If
End Sub

Private Sub SGSCOM_ScaleWeight(Weight As Double, Sign As SGSScaleComm.Polarity, UnitOfMeasure As SGSScaleComm.WeightType, GN As SGSScaleComm.GrossOrNet)
    Static cntr As Long
    
    If cntr > 60000 Then
        cntr = 0
    End If
    
    cntr = cntr + 1
    
    
    If OptnEvnt(3).Value = True Then
        If Len(txtMsgs.Text) > 69000 Then
            txtMsgs.Text = Empty
            DoEvents
        End If
        
        If chContinious.Value = vbChecked Then
            txtMsgs.Text = "Weight = " & Weight & ": SGSScaleComm.Polarity = " & Sign & ": SGSScaleComm.WeightType = " & UnitOfMeasure & ": SGSScaleComm.GrossOrNet = " & GN & vbCrLf & txtMsgs.Text
        Else
            txtMsgs.Text = "(" & cntr & ") " & "Weight = " & Weight & ": SGSScaleComm.Polarity = " & Sign & ": SGSScaleComm.WeightType = " & UnitOfMeasure & ": SGSScaleComm.GrossOrNet = " & GN
        End If
    End If
End Sub

Private Sub SGSCOM_StatusChange(StatusStr As String)
    Static cntr As Long
    
    If cntr > 60000 Then
        cntr = 0
    End If
    
    cntr = cntr + 1
    
    If OptnEvnt(4).Value = True Then
        If Len(txtMsgs.Text) > 69000 Then
            txtMsgs.Text = Empty
            DoEvents
        End If
        
        If chContinious.Value = vbChecked Then
            txtMsgs.Text = "COM Port Status = " & StatusStr & vbCrLf & txtMsgs.Text
        Else
            txtMsgs.Text = "(" & cntr & ") " & "COM Port Status = " & StatusStr
        End If
    End If
End Sub

Private Sub SGSCOM_Warning(WarningMsg As String)
    Static cntr As Long
    
    If cntr > 60000 Then
        cntr = 0
    End If
    
    cntr = cntr + 1
    
    If OptnEvnt(5).Value = True Then
        If Len(txtMsgs.Text) > 69000 Then
            txtMsgs.Text = Empty
            DoEvents
        End If
        
        If chContinious.Value = vbChecked Then
            txtMsgs.Text = "WarningMsg" & WarningMsg & vbCrLf & txtMsgs.Text
        Else
            txtMsgs.Text = "(" & cntr & ") " & "WarningMsg" & WarningMsg
        End If
    End If
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    SendData
End Sub


Sub SendData()
    On Error GoTo Error
    
    Dim i As Integer
    Dim DeEventCntr As Integer
    
    Select Case True
        Case optn100.Value
            DeEventCntr = 100
        Case optn200.Value
            DeEventCntr = 200
        Case optn500.Value
            DeEventCntr = 500
        Case optn700.Value
            DeEventCntr = 700
        Case optn1000.Value
            DeEventCntr = 1000
        Case Optn10000.Value
            DeEventCntr = 10000
        Case Else
            DeEventCntr = 10
    End Select
    
    While bSendData = True
        SGSCOM.SendData txtDataToSend.Text
        
        i = i + 1
        If i >= DeEventCntr Then
            i = 0
            txtMsgs.Text = Empty
            If DeEventCntr >= 2000 Then
                bSendData = False
            End If
            DoEvents
        End If
        
        If UCase(txtDataToSend.Text) Like "*5420LG*" Then
            txtDataToSend.Text = Replace(txtDataToSend.Text, "5420", "5425")
        ElseIf UCase(txtDataToSend.Text) Like "*5425LG*" Then
            txtDataToSend.Text = Replace(txtDataToSend.Text, "5425", "5415")
        ElseIf UCase(txtDataToSend.Text) Like "*5415LG*" Then
            txtDataToSend.Text = Replace(txtDataToSend.Text, "5415", "5419")
        ElseIf UCase(txtDataToSend.Text) Like "*5419LG*" Then
            txtDataToSend.Text = Replace(txtDataToSend.Text, "5419", "5422")
        ElseIf UCase(txtDataToSend.Text) Like "*5422LG*" Then
            txtDataToSend.Text = Replace(txtDataToSend.Text, "5422", "5421")
        ElseIf UCase(txtDataToSend.Text) Like "*5421LG*" Then
            txtDataToSend.Text = Replace(txtDataToSend.Text, "5421", "5420")
        End If
    Wend
    
    Exit Sub
Error:
    txtMsgs.Text = "SendData() Error: " & Err.Number & " " & Err.Description & vbCrLf & txtMsgs.Text

End Sub
