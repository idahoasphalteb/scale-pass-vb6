VERSION 5.00
Begin VB.Form frmUpdate 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Update Scale Pass Database"
   ClientHeight    =   7050
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8070
   Icon            =   "frmUpdate.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7050
   ScaleWidth      =   8070
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdStop 
      Caption         =   "Stop"
      Height          =   375
      Left            =   1440
      TabIndex        =   6
      ToolTipText     =   "Stop running the SQL scripts."
      Top             =   3000
      Width           =   1215
   End
   Begin VB.TextBox txtLog 
      BackColor       =   &H80000000&
      Height          =   2535
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   4
      Top             =   3840
      Width           =   7815
   End
   Begin VB.CommandButton cmdRunScripts 
      Caption         =   "Run Scripts"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   3000
      Width           =   1215
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "Exit"
      Height          =   375
      Left            =   6720
      TabIndex        =   2
      Top             =   6480
      Width           =   1215
   End
   Begin VB.TextBox txtSQLScripts 
      BackColor       =   &H80000000&
      Height          =   2535
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   1
      Top             =   360
      Width           =   7815
   End
   Begin VB.Label Label2 
      Caption         =   "Log"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   3600
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Scripts to be Ran"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1575
   End
End
Attribute VB_Name = "frmUpdate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim bStopProcess As Boolean
Dim bInprocess As Boolean

Private Sub cmdExit_Click()
    Unload Me
End Sub

Private Sub cmdRunScripts_Click()
    On Error GoTo Error
    
    Static bRunning As Boolean
    
    If bRunning = True Then Exit Sub
    
    If OpenScaleConn() = False Then
        bRunning = False
        MsgBox "You need to setup the Scale Pass Connection before you can use this application.  It is assuming that this application is in the same folder as the ScalePass2009.exe and ScalePass2009.ini files.", vbInformation, "Setup Required"
        cmdRunScripts.Enabled = False
        Exit Sub
    End If
    
    bRunning = True
    bStopProcess = False
    bInprocess = True
    
    'Call code to loop through and execute the sql statements
    Call RunThroughSQL
    
    bRunning = False
    bInprocess = False
    MsgBox "Finished.  Looked through the log for results.", vbInformation, "Run Scripts Finised"
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdRunScripts_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    bRunning = False
    bInprocess = False
End Sub

Private Sub cmdStop_Click()
    bStopProcess = True
End Sub

Private Sub Form_Load()
    On Error GoTo Error
    
    sSQLFilePath = App.Path & "\" & App.EXEName & ".sql"
    
    'Get the Database to connect to
    Call WriteToLog("Reading the Settings...")
    INIFilePath = App.Path & "\ScalePass2009.ini"
    
    'Read the input file
    Call GetSettings
    
    txtSQLScripts.Text = "Scale Pass Database: " & gbScaleDB & vbCrLf & Replace(Space(100), " ", "*") & vbCrLf
    
    txtSQLScripts.Text = txtSQLScripts.Text & "This utility assumes that each line a a separate SQL statement to be executed." & vbCrLf & Replace(Space(100), " ", "*") & vbCrLf
    
    Set gbScaleConn = New ADODB.Connection
    
    'Read the SQL Script file.
    Call WriteToLog("Reading the SQL Script file...")
    ReadBulkSQL
    
    Exit Sub
Error:
    MsgBox Me.Name & ".Form_Load.()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
End Sub

Sub ReadBulkSQL()
    On Error GoTo Error
    Dim FSO As New Scripting.FileSystemObject
    Dim TS As Scripting.TextStream
    
    If FSO.FileExists(sSQLFilePath) = False Then
        cmdRunScripts.Enabled = False
        txtSQLScripts.Text = txtSQLScripts.Text & "The SQL script file is missing: " & vbCrLf & sSQLFilePath & vbCrLf
        
        Call WriteToLog("The SQL script file is missing: " & vbCrLf & sSQLFilePath & vbCrLf)
        
        Exit Sub
    End If
    
    Set TS = FSO.OpenTextFile(sSQLFilePath, ForReading)
    
    If TS.AtEndOfStream = True Then
        cmdRunScripts.Enabled = False
        txtSQLScripts.Text = txtSQLScripts.Text & "There are not scripts to be ran...."
    Else
        txtSQLScripts.Text = txtSQLScripts.Text & TS.ReadAll
    End If
    
    TS.Close
    
    Set TS = Nothing
    Set FSO = Nothing
    
    Exit Sub
Error:
    MsgBox Me.Name & ".ReadBulkSQL()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
End Sub

Sub RunThroughSQL()
    On Error GoTo Error
    Dim FSO As New Scripting.FileSystemObject
    Dim TS As Scripting.TextStream
    Dim sSQL As String
    
    If FSO.FileExists(sSQLFilePath) = False Then
        cmdRunScripts.Enabled = False
        txtSQLScripts.Text = txtSQLScripts.Text & "The SQL script file is missing: " & vbCrLf & sSQLFilePath & vbCrLf
        
        Call WriteToLog("The SQL script file is missing: " & vbCrLf & sSQLFilePath & vbCrLf)
        
        Exit Sub
    End If
    
    Set TS = FSO.OpenTextFile(sSQLFilePath, ForReading)
    
    While Not TS.AtEndOfStream = True
        If bStopProcess = True Then
            Call WriteToLog("****************SQL Script Run has been Stopped****************")
            GoTo CleanUP
        End If
        
        sSQL = TS.ReadLine
        If Trim(sSQL) <> Empty Then
            DoEvents
            Call WriteToLog("Get Next Line to Execute: " & sSQL)
            'Execute SQL
            ExecuteSQL sSQL
            DoEvents
        End If
    Wend
    
CleanUP:
    TS.Close
    
    Set TS = Nothing
    Set FSO = Nothing
    
    Exit Sub
Error:
    MsgBox Me.Name & ".RunThroughSQL()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    GoTo CleanUP
End Sub

Sub ExecuteSQL(sSQL As String)
    On Error GoTo Error
    Dim ErrMsg As String
    
    If gbScaleConn.State <> 1 Then
        ErrMsg = "Connection to the database has been lost." & vbCrLf
        Call WriteToLog(ErrMsg & vbCrLf)
        bStopProcess = True
        
        Exit Sub
    End If
    
    gbScaleConn.Execute sSQL
    
    ErrMsg = "Execute Success" & vbCrLf
    Call WriteToLog(ErrMsg & vbCrLf)
    
    Exit Sub
Error:
    ErrMsg = "Execute Failed.  Error: " & Err.Number & " " & Err.Description & vbCrLf
    Call WriteToLog(ErrMsg)
    
    Err.Clear
End Sub

Sub WriteToLog(strMsg As String)
    On Error Resume Next
    
    txtLog.Text = txtLog.Text & strMsg & vbCrLf
    DoEvents
    
    Err.Clear
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If bInprocess = True Then
        MsgBox "Running through scripts.  Please wait.", vbInformation, "Can't Exit Right Now"
        Cancel = True
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    
    If gbScaleConn.State = 1 Then gbScaleConn.Close
    Set gbScaleConn = Nothing

End Sub

