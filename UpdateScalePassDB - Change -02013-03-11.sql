ALTER TABLE [Loads]  add [FrontPrintCOA] Number Null
ALTER TABLE [Loads]  add [FrontCOARequired] Number Null
ALTER TABLE [Loads]  add [RearPrintCOA] Number Null
ALTER TABLE [Loads]  add [RearCOARequired] Number Null
ALTER TABLE [BOLPrinting]  add [PrintCOA] Number Null
ALTER TABLE [BOLPrinting]  add [COARequired] Number Null
