VERSION 5.00
Begin VB.Form frmCustomLable 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Print Custom Label"
   ClientHeight    =   7830
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6675
   Icon            =   "frmCustomLable.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7830
   ScaleWidth      =   6675
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   5040
      TabIndex        =   14
      Top             =   7200
      Width           =   1335
   End
   Begin VB.CommandButton cmdPrintPreview 
      Caption         =   "Print Preview"
      Height          =   375
      Left            =   3600
      TabIndex        =   12
      Top             =   6720
      Width           =   1335
   End
   Begin VB.TextBox txtCopyCount 
      Alignment       =   2  'Center
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   1
      EndProperty
      Height          =   285
      Left            =   1080
      TabIndex        =   9
      Text            =   "1"
      Top             =   6840
      Width           =   1335
   End
   Begin VB.ComboBox cmboPrinter 
      Height          =   315
      Left            =   1080
      TabIndex        =   11
      Text            =   "Combo1"
      Top             =   7200
      Width           =   2295
   End
   Begin VB.TextBox txtNotes 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3000
      Left            =   1560
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   7
      Text            =   "frmCustomLable.frx":0442
      Top             =   3600
      Width           =   4815
   End
   Begin VB.TextBox txtShipToAddr 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1340
      Left            =   2880
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   5
      Text            =   "frmCustomLable.frx":04DB
      Top             =   2160
      Width           =   3495
   End
   Begin VB.TextBox txtPlantAddr 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1340
      Left            =   1560
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   3
      Text            =   "frmCustomLable.frx":0574
      Top             =   720
      Width           =   4815
   End
   Begin VB.TextBox txtCompanyName 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   1560
      TabIndex        =   1
      Text            =   "Western Emulsions Coolidge"
      Top             =   120
      Width           =   4815
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print"
      Height          =   375
      Left            =   3600
      TabIndex        =   13
      Top             =   7200
      Width           =   1335
   End
   Begin VB.Label lblCopyCount 
      Caption         =   "# of Copies"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   6840
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Printer:"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   7200
      Width           =   975
   End
   Begin VB.Label lblNotes 
      Caption         =   "Notes:"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   3600
      Width           =   1335
   End
   Begin VB.Label lblShipToAddr 
      Caption         =   "Ship To:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1560
      TabIndex        =   4
      Top             =   2160
      Width           =   1335
   End
   Begin VB.Label lblPlantAddr 
      Caption         =   "Plant Address:"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   720
      Width           =   1335
   End
   Begin VB.Label lblCompanyName 
      Caption         =   "Company Name:"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1335
   End
End
Attribute VB_Name = "frmCustomLable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdPrint_Click()
    On Error GoTo Error
    Dim ErrMsg As String
    Dim lblData As CstmLabelData
    Dim lcNbrOfCopies As Integer
    
    gbLastActivityTime = Date + Time
    
    Call PrintLabel(False)
        
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    ErrMsg = Me.Name & ".cmdPrint_Click()" & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox ErrMsg, vbExclamation, "Scale Pass Print Custom Label"
    
    Err.Clear
    gbLastActivityTime = Date + Time
    
End Sub

Private Sub PrintLabel(Optional Preview As Boolean = False)
    On Error GoTo Error
    Dim ErrMsg As String
    Dim lblData As CstmLabelData
    Dim lcNbrOfCopies As Integer
    
    gbLastActivityTime = Date + Time
    
    lblData.CompanyName = txtCompanyName.Text
    lblData.PlantAddress = txtPlantAddr.Text
    lblData.ShipToAddress = txtShipToAddr.Text
    lblData.Notes = txtNotes.Text
    
    If IsNumeric(txtCopyCount.Text) = True Then
        lcNbrOfCopies = CInt(txtCopyCount.Text)
    Else
        lcNbrOfCopies = 1
    End If
    
    If lcNbrOfCopies < 1 Then
        MsgBox "The number of copies must be greater than 0.", vbInformation, "Wrong Number of Copies"
        Exit Sub
    End If
    
    If basMethods.PrintCustomCstmlblData(lblData, cmboPrinter.Text, lcNbrOfCopies, Preview) = False Then
        MsgBox "There was an error printing the Custom Label.", vbInformation, "Label Did Not Print"
        Exit Sub
    End If
        
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    ErrMsg = Me.Name & ".PrintLabel()" & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox ErrMsg, vbExclamation, "Scale Pass Print Custom Label"
    
    Err.Clear
    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdPrintPreview_Click()
    On Error GoTo Error
    Dim ErrMsg As String
    Dim lblData As CstmLabelData
    Dim lcNbrOfCopies As Integer
    
    gbLastActivityTime = Date + Time
    
    Call PrintLabel(True)
        
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    ErrMsg = Me.Name & ".cmdPrintPreview_Click()" & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox ErrMsg, vbExclamation, "Scale Pass Print Custom Label"
    
    Err.Clear
    gbLastActivityTime = Date + Time

End Sub

Private Sub Form_Load()
    On Error GoTo Error
    Dim ErrMsg As String
    
    gbLastActivityTime = Date + Time
    
    txtCompanyName.Text = Empty
    txtPlantAddr.Text = Empty
    txtShipToAddr.Text = Empty
    txtNotes.Text = Empty
    
    PopulateCombos
    
    txtCompanyName.Text = GetMASCompanyName(gbMASCompanyID)
    txtPlantAddr.Text = gbBOLAddrPhone
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    ErrMsg = Me.Name & ".Form_Load()" & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox ErrMsg, vbExclamation, "Scale Pass Print Custom Label"
    
    Err.Clear
    gbLastActivityTime = Date + Time
    
End Sub

Private Sub txtCopyCount_Validate(Cancel As Boolean)
    If IsNumeric(txtCopyCount.Text) = True Then
        txtCopyCount.Text = CInt(txtCopyCount.Text)
    Else
        txtCopyCount.Text = 1
    End If
    
    gbLastActivityTime = Date + Time
End Sub

Sub PopulateCombos()
    On Error GoTo Error
    
    Dim Prntr As Printer
    
    gbLastActivityTime = Date + Time
        
    cmboPrinter.Clear
    
    For Each Prntr In Printers
        cmboPrinter.AddItem Prntr.DeviceName
    Next
    
    cmboPrinter.Text = gbLablePrinter
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".PopulateCombos()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub


