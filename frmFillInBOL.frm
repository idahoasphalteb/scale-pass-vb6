VERSION 5.00
Begin VB.Form frmFillInBOL 
   Caption         =   "Fill In BOL Data"
   ClientHeight    =   10410
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12270
   Icon            =   "frmFillInBOL.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   10410
   ScaleWidth      =   12270
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdOk 
      Caption         =   "Print"
      Height          =   375
      Left            =   3840
      TabIndex        =   55
      Top             =   120
      Width           =   975
   End
   Begin VB.PictureBox picContainer 
      Height          =   9615
      Left            =   120
      ScaleHeight     =   9555
      ScaleWidth      =   11955
      TabIndex        =   53
      TabStop         =   0   'False
      Top             =   600
      Width           =   12015
      Begin VB.PictureBox picChild 
         Height          =   9615
         Left            =   0
         Picture         =   "frmFillInBOL.frx":0442
         ScaleHeight     =   9555
         ScaleWidth      =   12075
         TabIndex        =   54
         TabStop         =   0   'False
         Top             =   0
         Width           =   12135
         Begin VB.TextBox txtAdditiveDesc 
            Height          =   300
            Left            =   200
            MaxLength       =   50
            TabIndex        =   35
            Top             =   7600
            Width           =   3865
         End
         Begin VB.CommandButton cmdSrchAdd 
            Caption         =   "..."
            Height          =   255
            Left            =   4080
            TabIndex        =   34
            ToolTipText     =   "Lookup Product"
            Top             =   7600
            Width           =   435
         End
         Begin VB.CommandButton cmdSrchPrdct 
            Caption         =   "..."
            Height          =   255
            Left            =   10370
            TabIndex        =   48
            ToolTipText     =   "Lookup Product"
            Top             =   8960
            Width           =   375
         End
         Begin VB.TextBox txtLoadTemp 
            Height          =   300
            Left            =   4515
            MaxLength       =   50
            TabIndex        =   36
            Top             =   7600
            Width           =   715
         End
         Begin VB.TextBox txtProdDesc 
            Height          =   300
            Left            =   8880
            MaxLength       =   50
            TabIndex        =   49
            Top             =   8960
            Width           =   1465
         End
         Begin VB.TextBox txtNetTons 
            Height          =   300
            Left            =   10760
            Locked          =   -1  'True
            TabIndex        =   50
            Top             =   8960
            Width           =   1110
         End
         Begin VB.TextBox txtTotalGrossWt 
            Height          =   300
            Left            =   8085
            Locked          =   -1  'True
            TabIndex        =   45
            Top             =   8265
            Width           =   1335
         End
         Begin VB.TextBox txtTotalTareWt 
            Height          =   300
            Left            =   9405
            Locked          =   -1  'True
            TabIndex        =   46
            Top             =   8265
            Width           =   1335
         End
         Begin VB.TextBox txtTotalNetWt 
            Height          =   300
            Left            =   10740
            Locked          =   -1  'True
            TabIndex        =   47
            Top             =   8265
            Width           =   1150
         End
         Begin VB.TextBox txtBOL 
            Height          =   375
            Left            =   10320
            MaxLength       =   50
            TabIndex        =   0
            Top             =   1245
            Width           =   1485
         End
         Begin VB.TextBox txtMASCustomer 
            Height          =   300
            Left            =   1080
            MaxLength       =   500
            TabIndex        =   1
            Top             =   2505
            Width           =   3135
         End
         Begin VB.TextBox txtMASCustomerName 
            Height          =   300
            Left            =   4560
            MaxLength       =   500
            TabIndex        =   2
            Top             =   2505
            Width           =   4335
         End
         Begin VB.TextBox txtContractNumber 
            Height          =   300
            Left            =   1750
            MaxLength       =   50
            TabIndex        =   4
            Top             =   2800
            Width           =   1335
         End
         Begin VB.TextBox txtPurchas_Order 
            Height          =   300
            Left            =   3840
            MaxLength       =   255
            TabIndex        =   5
            Top             =   2800
            Width           =   1935
         End
         Begin VB.TextBox txtBOLPrintDate 
            Height          =   300
            Left            =   10800
            TabIndex        =   3
            Top             =   2550
            Width           =   1075
         End
         Begin VB.TextBox txtMASkDeliveryLoc 
            Height          =   300
            Left            =   1800
            MaxLength       =   50
            TabIndex        =   6
            Top             =   3110
            Width           =   10100
         End
         Begin VB.TextBox txtDestDirections 
            Height          =   300
            Left            =   1245
            MaxLength       =   250
            TabIndex        =   7
            Top             =   3410
            Width           =   10650
         End
         Begin VB.TextBox txtMASProjNumber 
            Height          =   300
            Left            =   1250
            MaxLength       =   60
            TabIndex        =   8
            Top             =   3700
            Width           =   3375
         End
         Begin VB.TextBox txtMASProjDesc 
            Height          =   300
            Left            =   5210
            MaxLength       =   255
            TabIndex        =   9
            Top             =   3700
            Width           =   6695
         End
         Begin VB.TextBox txtCarrierID 
            Height          =   300
            Left            =   840
            MaxLength       =   10
            TabIndex        =   10
            Top             =   4005
            Width           =   4095
         End
         Begin VB.TextBox txtWeightTktNbr 
            Height          =   300
            Left            =   10200
            MaxLength       =   30
            TabIndex        =   11
            Top             =   4005
            Width           =   1700
         End
         Begin VB.TextBox txtTruckNo 
            Height          =   300
            Left            =   1095
            MaxLength       =   50
            TabIndex        =   12
            Top             =   4320
            Width           =   1455
         End
         Begin VB.TextBox txtTrailer01 
            Height          =   300
            Left            =   3600
            MaxLength       =   255
            TabIndex        =   13
            Top             =   4320
            Width           =   1335
         End
         Begin VB.TextBox txtTrailer02 
            Height          =   300
            Left            =   6000
            MaxLength       =   255
            TabIndex        =   14
            Top             =   4320
            Width           =   1215
         End
         Begin VB.TextBox txtLoadTime 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "h:nn AM/PM"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   4
            EndProperty
            Height          =   300
            Left            =   8330
            TabIndex        =   15
            Top             =   4320
            Width           =   1095
         End
         Begin VB.TextBox txtDeliveryTime 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "h:nn AM/PM"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   4
            EndProperty
            Height          =   300
            Left            =   10750
            TabIndex        =   16
            Top             =   4320
            Width           =   1120
         End
         Begin VB.TextBox txtScaleInTime 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "h:nn AM/PM"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   4
            EndProperty
            Height          =   300
            Left            =   1410
            TabIndex        =   17
            Top             =   4630
            Width           =   1095
         End
         Begin VB.TextBox txtScaleOutTime 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "h:nn AM/PM"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   4
            EndProperty
            Height          =   300
            Left            =   3960
            TabIndex        =   18
            Top             =   4630
            Width           =   975
         End
         Begin VB.TextBox txtRemarks 
            Height          =   300
            Left            =   1080
            MaxLength       =   74
            TabIndex        =   19
            ToolTipText     =   "Remarks"
            Top             =   4920
            Width           =   5295
         End
         Begin VB.TextBox txtMASPenatration 
            Height          =   300
            Left            =   3960
            TabIndex        =   21
            ToolTipText     =   "Penetration"
            Top             =   5350
            Width           =   1455
         End
         Begin VB.TextBox txtMASpenetrationtemp 
            Height          =   300
            Left            =   5760
            TabIndex        =   22
            ToolTipText     =   "Penetration Temp"
            Top             =   5350
            Width           =   1455
         End
         Begin VB.TextBox txtFashPoint 
            Height          =   300
            Left            =   8400
            TabIndex        =   23
            Top             =   5350
            Width           =   1455
         End
         Begin VB.TextBox txtLbsPerGal 
            Height          =   300
            Left            =   10680
            TabIndex        =   24
            Top             =   5350
            Width           =   1200
         End
         Begin VB.TextBox txtSpecificGravity 
            Height          =   300
            Left            =   1440
            MaxLength       =   50
            TabIndex        =   26
            Top             =   5640
            Width           =   1455
         End
         Begin VB.TextBox txtViscosity 
            Height          =   300
            Left            =   4680
            MaxLength       =   50
            TabIndex        =   27
            ToolTipText     =   "Viscosity"
            Top             =   5640
            Width           =   1455
         End
         Begin VB.TextBox txtResidue 
            Height          =   300
            Left            =   8640
            MaxLength       =   50
            TabIndex        =   29
            Top             =   5640
            Width           =   1455
         End
         Begin VB.TextBox txtLotNbr 
            Height          =   300
            Left            =   11040
            MaxLength       =   50
            TabIndex        =   30
            Top             =   5640
            Width           =   840
         End
         Begin VB.TextBox txtTankNumber 
            Height          =   300
            Left            =   1080
            MaxLength       =   50
            TabIndex        =   31
            Top             =   5940
            Width           =   1455
         End
         Begin VB.TextBox txtBatchNumber 
            Height          =   300
            Left            =   4680
            MaxLength       =   100
            TabIndex        =   32
            Top             =   5940
            Width           =   1455
         End
         Begin VB.TextBox txtGrossGallons 
            Height          =   300
            Left            =   5280
            TabIndex        =   37
            Top             =   7600
            Width           =   1070
         End
         Begin VB.TextBox txtNetGallons 
            Height          =   300
            Left            =   6360
            TabIndex        =   38
            Top             =   7600
            Width           =   1070
         End
         Begin VB.TextBox txtFrontGrossWt 
            Height          =   300
            Left            =   8085
            TabIndex        =   39
            Top             =   7635
            Width           =   1335
         End
         Begin VB.TextBox txtRearGrossWt 
            Height          =   300
            Left            =   8085
            TabIndex        =   42
            Top             =   7950
            Width           =   1335
         End
         Begin VB.TextBox txtFrontTareWt 
            Height          =   300
            Left            =   9405
            TabIndex        =   40
            Top             =   7635
            Width           =   1335
         End
         Begin VB.TextBox txtFrontNetWt 
            Height          =   300
            Left            =   10740
            Locked          =   -1  'True
            TabIndex        =   41
            Top             =   7635
            Width           =   1150
         End
         Begin VB.TextBox txtRearTareWt 
            Height          =   300
            Left            =   9405
            TabIndex        =   43
            Top             =   7950
            Width           =   1335
         End
         Begin VB.TextBox txtRearNetWt 
            Height          =   300
            Left            =   10740
            Locked          =   -1  'True
            TabIndex        =   44
            Top             =   7950
            Width           =   1150
         End
         Begin VB.TextBox txtMASCompliance 
            Height          =   300
            Left            =   1440
            MaxLength       =   50
            TabIndex        =   33
            Top             =   6915
            Visible         =   0   'False
            Width           =   3855
         End
         Begin VB.TextBox txtSecurityTapeNbr 
            Height          =   300
            Left            =   7920
            MaxLength       =   100
            TabIndex        =   20
            ToolTipText     =   "Security Tape Number"
            Top             =   4920
            Width           =   3495
         End
         Begin VB.TextBox txtViscosityTemp 
            Height          =   300
            Left            =   6240
            MaxLength       =   50
            TabIndex        =   28
            ToolTipText     =   "Viscosity Temp"
            Top             =   5640
            Width           =   1455
         End
         Begin VB.Line Line1 
            X1              =   4320
            X2              =   4440
            Y1              =   2640
            Y2              =   2640
         End
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   2640
      TabIndex        =   52
      Top             =   120
      Width           =   975
   End
   Begin VB.VScrollBar VScrollPic 
      Height          =   1575
      Left            =   12000
      Max             =   10
      TabIndex        =   51
      TabStop         =   0   'False
      Top             =   240
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.HScrollBar HScrollPic 
      Height          =   255
      Left            =   -240
      Max             =   10
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   10080
      Visible         =   0   'False
      Width           =   3375
   End
End
Attribute VB_Name = "frmFillInBOL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const picHeightDiff = 1605
Const picWidthDiff = 735
Public bCancel As Boolean
Public RS As ADODB.Recordset
Public RSLoads As ADODB.Recordset

Private Sub cmdCancel_Click()
    bCancel = True
    Unload Me

End Sub

Private Sub cmdSrchPrdct_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Load frmProductSearch
    
    frmProductSearch.ItemID = ConvertToString(frmPrintBlankBOL.RS("ProductID").Value)
    frmProductSearch.ItemDesc = ConvertToString(frmPrintBlankBOL.RS("ProductDesc").Value)
    frmProductSearch.ItemClassID = ConvertToString(frmPrintBlankBOL.RS("ProductClass").Value)
    
    frmProductSearch.Show vbModal, Me
    
    frmPrintBlankBOL.RS("ProductID").Value = ConvertToString(frmProductSearch.ItemID)
    frmPrintBlankBOL.RS("ProductDesc").Value = ConvertToString(frmProductSearch.ItemDesc)
    frmPrintBlankBOL.RS("ProductClass").Value = ConvertToString(frmProductSearch.ItemClassID)
    
    txtProdDesc.Text = ConvertToString(frmProductSearch.ItemDesc)
        
    Unload frmProductSearch
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSrchPrdct_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdOK_Click()
    bCancel = False
    
    If SaveUserInput() = False Then
        MsgBox "Failed to save data.", vbExclamation + vbCritical, "Scale Pass"
        Exit Sub
    End If

    frmPrintBlankBOL.PrintReport
    
End Sub

Function SaveUserInput() As Boolean
    On Error GoTo Error
    Dim ErrMsg As String
    
    SaveUserInput = False   'Default to failure
    
    If RSLoads Is Nothing Then
        'Don't save
        Exit Function
    End If
    
    If RSLoads.State <> 1 Then
        'Don't save
        Exit Function
    End If

    If RSLoads.BOF Or RSLoads.EOF Then
        'Don't save
        Exit Function
    End If
    
    If RS Is Nothing Then
            'Don't save
        Exit Function
    End If

    If RS.State <> 1 Then
            'Don't save
        Exit Function
    End If

    If RS.EOF Or RS.BOF Then
            'Don't save
        Exit Function
    End If

    RS.Resync
    
    RS("BOLNbr").Value = txtBOL.Text
    RS("MASCustomer").Value = txtMASCustomer.Text
    RS("MASCustomerName").Value = txtMASCustomerName.Text
    RS("PrintDate").Value = ConvertToDateOrNull(txtBOLPrintDate.Text)
    RS("ContractNbr").Value = txtContractNumber.Text
    RS("Purchase_Order").Value = txtPurchas_Order.Text
'    RS("MASFacilityName").Value = txtMASFacilityName.Text
    RS("MASDeliveryLoc").Value = txtMASkDeliveryLoc.Text
    RS("DestDirections").Value = txtDestDirections.Text
    RS("MASProjectNumber").Value = txtMASProjNumber.Text
    RS("MASProjectDescription").Value = txtMASProjDesc.Text
    RS("Carrier_ID").Value = txtCarrierID.Text
    RS("WeightTktNo").Value = txtWeightTktNbr.Text
    RS("TruckNo").Value = txtTruckNo.Text
    RS("Trailer1_ID").Value = txtTrailer01.Text
    RS("Trailer2_ID").Value = txtTrailer02.Text
    RS("LoadTime").Value = ConvertToDateOrNull(txtLoadTime.Text)
    RS("DeliverTime").Value = ConvertToDateOrNull(txtDeliveryTime.Text)
    RS("ScaleInTime").Value = ConvertToDateOrNull(txtScaleInTime.Text)
    RS("ScaleOutTime").Value = ConvertToDateOrNull(txtScaleOutTime.Text)
    RS("Remarks").Value = txtRemarks.Text
    RS("SecurityTapNumber").Value = txtSecurityTapeNbr.Text
    RS("MASpenetration").Value = ConvertToDbleOrNull(txtMASPenatration.Text)
    RS("MASpenetrationtemperature").Value = ConvertToDbleOrNull(txtMASpenetrationtemp.Text)
    RS("Flashpoint").Value = ConvertToDbleOrNull(txtFashPoint.Text)
    
    RS("LbsPerGallon2").Value = ConvertToDbleOrNull(txtLbsPerGal.Text)
    If Not IsNull(RS("LbsPerGallon2").Value) Then
        'Make sure there is a decimal place
        If InStr(1, RS("LbsPerGallon2").Value, ".") <= 0 Then
            'Add decimal
            RS("LbsPerGallon2").Value = Trim(RS("LbsPerGallon2").Value) + "."
        End If
    End If
    
    RS("SpecificGravity").Value = ConvertToDbleOrNull(txtSpecificGravity.Text)
    RS("Viscosity").Value = ConvertToDbleOrNull(txtViscosity.Text)
    RS("ViscosityTemp").Value = txtViscosityTemp.Text
    RS("Residue").Value = txtResidue.Text
    RS("LotNbr").Value = txtLotNbr.Text
    RS("TankNumber").Value = txtTankNumber.Text
    RS("BatchNumber").Value = txtBatchNumber.Text

    RS("MASCompliance").Value = txtMASCompliance.Text
    RS("AdditiveDesc").Value = txtAdditiveDesc.Text
    RS("LoadTemp").Value = txtLoadTemp.Text
    RS("GrossGallons").Value = ConvertToDbleOrNull(txtGrossGallons.Text)
    RS("NetGallons").Value = ConvertToDbleOrNull(txtNetGallons.Text)
    RS("FrontGrossWt").Value = ConvertToDbleOrNull(txtFrontGrossWt.Text)
    RS("FrontTareWt").Value = ConvertToDbleOrNull(txtFrontTareWt.Text)
    RS("FrontNetWt").Value = ConvertToDbleOrNull(txtFrontNetWt.Text)
    RS("RearGrossWt").Value = ConvertToDbleOrNull(txtRearGrossWt.Text)
    RS("RearTareWt").Value = ConvertToDbleOrNull(txtRearTareWt.Text)
    RS("RearNetWt").Value = ConvertToDbleOrNull(txtRearNetWt.Text)
    RS("TotalGrossWt").Value = ConvertToDbleOrNull(txtTotalGrossWt.Text)
    RS("TotalTareWt").Value = ConvertToDbleOrNull(txtTotalTareWt.Text)
    RS("TotalNetWt").Value = ConvertToDbleOrNull(txtTotalNetWt.Text)
    RS("ProductDesc").Value = txtProdDesc.Text
    RS("NetTons").Value = ConvertToDbleOrNull(txtNetTons.Text)
    
    'validate weights for report to print correctly
    If IsNull(RS("FrontGrossWt").Value) = False Then
        If IsNull(RS("RearGrossWt").Value) = True Then
            RS("RearGrossWt").Value = 0
        End If
    End If
    
    If IsNull(RS("RearGrossWt").Value) = False Then
        If IsNull(RS("FrontGrossWt").Value) = True Then
            RS("FrontGrossWt").Value = 0
        End If
    End If
    
    If IsNull(RS("FrontTareWt").Value) = False Then
        If IsNull(RS("RearTareWt").Value) = True Then
            RS("RearTareWt").Value = 0
        End If
    End If
    
    If IsNull(RS("RearTareWt").Value) = False Then
        If IsNull(RS("FrontTareWt").Value) = True Then
            RS("FrontTareWt").Value = 0
        End If
    End If
    
    If IsNull(RS("FrontNetWt").Value) = False Then
        If IsNull(RS("RearNetWt").Value) = True Then
            RS("RearNetWt").Value = 0
        End If
    End If
    
    If IsNull(RS("RearNetWt").Value) = False Then
        If IsNull(RS("FrontNetWt").Value) = True Then
            RS("FrontNetWt").Value = 0
        End If
    End If
    
    RS.UpdateBatch
    
    SaveUserInput = True
    
    Exit Function
Error:
    ErrMsg = Me.Name & ".SaveUserInput()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox ErrMsg, vbInformation, "Scale Pass"
    
    Err.Clear
End Function

Function SaveUserInputOrig() As Boolean
    On Error GoTo Error
    Dim ErrMsg As String
    
    SaveUserInputOrig = False   'Default to failure
    
    If frmPrintBlankBOL.RSLoads Is Nothing Then
        'Don't save
        Exit Function
    End If
    
    If frmPrintBlankBOL.RSLoads.State <> 1 Then
        'Don't save
        Exit Function
    End If

    If frmPrintBlankBOL.RSLoads.BOF Or frmPrintBlankBOL.RSLoads.EOF Then
        'Don't save
        Exit Function
    End If
    
    If frmPrintBlankBOL.RS Is Nothing Then
            'Don't save
        Exit Function
    End If

    If frmPrintBlankBOL.RS.State <> 1 Then
            'Don't save
        Exit Function
    End If

    If frmPrintBlankBOL.RS.EOF Or frmPrintBlankBOL.RS.BOF Then
            'Don't save
        Exit Function
    End If

    
    frmPrintBlankBOL.RS("BOLNbr").Value = txtBOL.Text
    frmPrintBlankBOL.RS("MASCustomer").Value = txtMASCustomer.Text
    frmPrintBlankBOL.RS("MASCustomerName").Value = txtMASCustomerName.Text
    frmPrintBlankBOL.RS("PrintDate").Value = ConvertToDateOrNull(txtBOLPrintDate.Text)
    frmPrintBlankBOL.RS("ContractNbr").Value = txtContractNumber.Text
    frmPrintBlankBOL.RS("Purchase_Order").Value = txtPurchas_Order.Text
'    frmPrintBlankBOL.RS("MASFacilityName").Value = txtMASFacilityName.Text
    frmPrintBlankBOL.RS("MASDeliveryLoc").Value = txtMASkDeliveryLoc.Text
    frmPrintBlankBOL.RS("DestDirections").Value = txtDestDirections.Text
    frmPrintBlankBOL.RS("MASProjectNumber").Value = txtMASProjNumber.Text
    frmPrintBlankBOL.RS("MASProjectDescription").Value = txtMASProjDesc.Text
    frmPrintBlankBOL.RS("Carrier_ID").Value = txtCarrierID.Text
    frmPrintBlankBOL.RS("WeightTktNo").Value = txtWeightTktNbr.Text
    frmPrintBlankBOL.RS("TruckNo").Value = txtTruckNo.Text
    frmPrintBlankBOL.RS("Trailer1_ID").Value = txtTrailer01.Text
    frmPrintBlankBOL.RS("Trailer2_ID").Value = txtTrailer02.Text
    frmPrintBlankBOL.RS("LoadTime").Value = ConvertToDateOrNull(txtLoadTime.Text)
    frmPrintBlankBOL.RS("DeliverTime").Value = ConvertToDateOrNull(txtDeliveryTime.Text)
    frmPrintBlankBOL.RS("ScaleInTime").Value = ConvertToDateOrNull(txtScaleInTime.Text)
    frmPrintBlankBOL.RS("ScaleOutTime").Value = ConvertToDateOrNull(txtScaleOutTime.Text)
    frmPrintBlankBOL.RS("Remarks").Value = txtRemarks.Text
    frmPrintBlankBOL.RS("SecurityTapNumber").Value = txtSecurityTapeNbr.Text
    frmPrintBlankBOL.RS("MASpenetration").Value = ConvertToDbleOrNull(txtMASPenatration.Text)
    frmPrintBlankBOL.RS("MASpenetrationtemperature").Value = ConvertToDbleOrNull(txtMASpenetrationtemp.Text)
    frmPrintBlankBOL.RS("Flashpoint").Value = ConvertToDbleOrNull(txtFashPoint.Text)
    
    frmPrintBlankBOL.RS("LbsPerGallon2").Value = ConvertToDbleOrNull(txtLbsPerGal.Text)
    If Not IsNull(frmPrintBlankBOL.RS("LbsPerGallon2").Value) Then
        'Make sure there is a decimal place
        If InStr(1, frmPrintBlankBOL.RS("LbsPerGallon2").Value, ".") <= 0 Then
            'Add decimal
            frmPrintBlankBOL.RS("LbsPerGallon2").Value = Trim(frmPrintBlankBOL.RS("LbsPerGallon2").Value) + "."
        End If
    End If
    
    frmPrintBlankBOL.RS("SpecificGravity").Value = ConvertToDbleOrNull(txtSpecificGravity.Text)
    frmPrintBlankBOL.RS("Viscosity").Value = ConvertToDbleOrNull(txtViscosity.Text)
    frmPrintBlankBOL.RS("ViscosityTemp").Value = txtViscosityTemp.Text
    frmPrintBlankBOL.RS("Residue").Value = txtResidue.Text
    frmPrintBlankBOL.RS("LotNbr").Value = txtLotNbr.Text
    frmPrintBlankBOL.RS("TankNumber").Value = txtTankNumber.Text
    frmPrintBlankBOL.RS("BatchNumber").Value = txtBatchNumber.Text

    frmPrintBlankBOL.RS("MASCompliance").Value = txtMASCompliance.Text
    frmPrintBlankBOL.RS("AdditiveDesc").Value = txtAdditiveDesc.Text
    frmPrintBlankBOL.RS("LoadTemp").Value = txtLoadTemp.Text
    frmPrintBlankBOL.RS("GrossGallons").Value = ConvertToDbleOrNull(txtGrossGallons.Text)
    frmPrintBlankBOL.RS("NetGallons").Value = ConvertToDbleOrNull(txtNetGallons.Text)
    frmPrintBlankBOL.RS("FrontGrossWt").Value = ConvertToDbleOrNull(txtFrontGrossWt.Text)
    frmPrintBlankBOL.RS("FrontTareWt").Value = ConvertToDbleOrNull(txtFrontTareWt.Text)
    frmPrintBlankBOL.RS("FrontNetWt").Value = ConvertToDbleOrNull(txtFrontNetWt.Text)
    frmPrintBlankBOL.RS("RearGrossWt").Value = ConvertToDbleOrNull(txtRearGrossWt.Text)
    frmPrintBlankBOL.RS("RearTareWt").Value = ConvertToDbleOrNull(txtRearTareWt.Text)
    frmPrintBlankBOL.RS("RearNetWt").Value = ConvertToDbleOrNull(txtRearNetWt.Text)
    frmPrintBlankBOL.RS("TotalGrossWt").Value = ConvertToDbleOrNull(txtTotalGrossWt.Text)
    frmPrintBlankBOL.RS("TotalTareWt").Value = ConvertToDbleOrNull(txtTotalTareWt.Text)
    frmPrintBlankBOL.RS("TotalNetWt").Value = ConvertToDbleOrNull(txtTotalNetWt.Text)
    frmPrintBlankBOL.RS("ProductDesc").Value = txtProdDesc.Text
    frmPrintBlankBOL.RS("NetTons").Value = ConvertToDbleOrNull(txtNetTons.Text)
    
    'validate weights for report to print correctly
    If IsNull(frmPrintBlankBOL.RS("FrontGrossWt").Value) = False Then
        If IsNull(frmPrintBlankBOL.RS("RearGrossWt").Value) = True Then
            frmPrintBlankBOL.RS("RearGrossWt").Value = 0
        End If
    End If
    
    If IsNull(frmPrintBlankBOL.RS("RearGrossWt").Value) = False Then
        If IsNull(frmPrintBlankBOL.RS("FrontGrossWt").Value) = True Then
            frmPrintBlankBOL.RS("FrontGrossWt").Value = 0
        End If
    End If
    
    If IsNull(frmPrintBlankBOL.RS("FrontTareWt").Value) = False Then
        If IsNull(frmPrintBlankBOL.RS("RearTareWt").Value) = True Then
            frmPrintBlankBOL.RS("RearTareWt").Value = 0
        End If
    End If
    
    If IsNull(frmPrintBlankBOL.RS("RearTareWt").Value) = False Then
        If IsNull(frmPrintBlankBOL.RS("FrontTareWt").Value) = True Then
            frmPrintBlankBOL.RS("FrontTareWt").Value = 0
        End If
    End If
    
    If IsNull(frmPrintBlankBOL.RS("FrontNetWt").Value) = False Then
        If IsNull(frmPrintBlankBOL.RS("RearNetWt").Value) = True Then
            frmPrintBlankBOL.RS("RearNetWt").Value = 0
        End If
    End If
    
    If IsNull(frmPrintBlankBOL.RS("RearNetWt").Value) = False Then
        If IsNull(frmPrintBlankBOL.RS("FrontNetWt").Value) = True Then
            frmPrintBlankBOL.RS("FrontNetWt").Value = 0
        End If
    End If
    
    frmPrintBlankBOL.RS.UpdateBatch
    
    SaveUserInputOrig = True
    
    Exit Function
Error:
    ErrMsg = Me.Name & ".SaveUserInputOrig()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox ErrMsg, vbInformation, "Scale Pass"
    
    Err.Clear
End Function

Private Sub cmdSrchAdd_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Load frmProductSearch
    
    frmProductSearch.ItemID = ConvertToString(frmPrintBlankBOL.RS("AdditiveID").Value)
    frmProductSearch.ItemDesc = ConvertToString(txtAdditiveDesc)
    frmProductSearch.ItemClassID = ConvertToString(frmPrintBlankBOL.RS("AdditiveClass").Value)
    
    frmProductSearch.Show vbModal, Me
    
    frmPrintBlankBOL.RS("AdditiveID").Value = ConvertToString(frmProductSearch.ItemID)
    frmPrintBlankBOL.RS("AdditiveDesc").Value = ConvertToString(frmProductSearch.ItemDesc)
    frmPrintBlankBOL.RS("AdditiveClass").Value = ConvertToString(frmProductSearch.ItemClassID)
    
    txtAdditiveDesc.Text = ConvertToString(frmProductSearch.ItemDesc)
        
    Unload frmProductSearch
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSrchAdd_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time

End Sub

Private Sub Form_Load()
    bCancel = True
    
    txtBOLPrintDate.Text = Date
End Sub

Private Sub Form_Resize()
    
'    Dim picHeight As Long
'    Dim picWidth As Long
'
'    If Me.Height < picHeightDiff - 100 Then
'        picHeight = picHeightDiff - 100
'    Else
'        picHeight = Me.Height - picHeightDiff
'    End If
'
'    If Me.Width < picWidthDiff - 100 Then
'        picWidth = picWidthDiff - 100
'    Else
'        picWidth = Me.Width - picWidthDiff
'    End If
'
'    picContainer.Height = picHeight
'    picContainer.Width = picWidth
'
'    HScrollPic.Width = picWidth
'    HScrollPic.Top = picContainer.Height + picContainer.Top + 100
'    HScrollPic.Left = picContainer.Left
'
'    VScrollPic.Height = picHeight
'    VScrollPic.Left = picContainer.Width + picContainer.Left + 100
'    VScrollPic.Top = picContainer.Top
'
''    If imgBOL.Width <= picContainer.Width - 50 Then
''        HScrollPic.Value = 0
''        imgBOL.Left = 50
''    End If
''
''    If imgBOL.Height <= picContainer.Height - 50 Then
''        VScrollPic.Value = 0
''        imgBOL.Top = 50
''    End If
'
'    Call HScrollPic_Change
'
'    Call VScrollPic_Change
'
End Sub

Private Sub HScrollPic_Change()
    
    Dim TotalDiff As Double
    Dim TotalDistance As Double
    Dim MovementAmt As Long
    Dim NewLeft As Long
    
    'Get the total distance
    TotalDistance = (picChild.Width + 100)
    
    'Get the Movement Amount
    TotalDiff = TotalDistance - picContainer.Width

    If HScrollPic.Value = 0 Then
        picChild.Left = 50
    Else
        MovementAmt = TotalDiff / 10
        
        If MovementAmt < 0 Then
            picChild.Left = 50
        Else
            NewLeft = (MovementAmt * HScrollPic.Value) * -1
            
            picChild.Left = NewLeft
        End If
    End If
    
End Sub


Private Sub txtAdditiveDesc_GotFocus()
    HighLightText
End Sub

Private Sub txtBatchNumber_GotFocus()
    HighLightText
End Sub

Private Sub txtBOL_GotFocus()
   HighLightText
End Sub


Sub HighLightText()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)
End Sub

Private Sub txtBOLPrintDate_GotFocus()
    HighLightText
End Sub

Private Sub txtBOLPrintDate_Validate(Cancel As Boolean)
    If IsDate(txtBOLPrintDate.Text) = False And Trim(txtBOLPrintDate.Text) <> Empty Then
        Cancel = True
        MsgBox "The Print Date Must be a valid date.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtCarrierID_GotFocus()
    HighLightText
End Sub

Private Sub txtContractNumber_GotFocus()
    HighLightText
End Sub

Private Sub txtDeliveryTime_GotFocus()
    HighLightText
End Sub

Private Sub txtDeliveryTime_Validate(Cancel As Boolean)
    If IsDate(txtDeliveryTime.Text) = False And Trim(txtDeliveryTime.Text) <> Empty Then
        Cancel = True
        MsgBox "The Delivery Time Must be a valid date/time.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtDestDirections_GotFocus()
    HighLightText
End Sub

Private Sub txtFashPoint_GotFocus()
    HighLightText
End Sub

Private Sub txtFashPoint_Validate(Cancel As Boolean)
'    If Trim(txtFashPoint.Text) = Empty Then txtFashPoint.Text = 0
    If Trim(txtFashPoint.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtFashPoint.Text) = False Then
        Cancel = True
        MsgBox "The FlashPoint must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtFrontGrossWt_Change()
    Call Calculations
    
'    Dim Front As Double
'    Dim Rear As Double
'    Dim Total As Double
'
'    If IsNumeric(txtFrontGrossWt.Text) = True Then
'        Front = CDbl(txtFrontGrossWt.Text)
'    Else
'        Front = 0
'    End If
'
'    If IsNumeric(txtRearGrossWt.Text) = True Then
'        Rear = CDbl(txtRearGrossWt.Text)
'    Else
'        Rear = 0
'    End If
'
'    Total = Front + Rear
'
'    txtTotalGrossWt.Text = Total
    
End Sub

Private Sub txtFrontGrossWt_GotFocus()
    HighLightText
End Sub

Private Sub txtFrontGrossWt_Validate(Cancel As Boolean)

'    If Trim(txtFrontGrossWt.Text) = Empty Then txtFrontGrossWt.Text = 0
    If Trim(txtFrontGrossWt.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtFrontGrossWt.Text) = False Then
        Cancel = True
        MsgBox "The Front Gross Wt must be a valid number.", vbInformation, "Scale Pass"
    End If
End Sub

Private Sub txtFrontNetWt_Change()
'    Dim Front As Double
'    Dim Rear As Double
'    Dim Total As Double
'
'    If IsNumeric(txtFrontNetWt.Text) = True Then
'        Front = CDbl(txtFrontNetWt.Text)
'    Else
'        Front = 0
'    End If
'
'    If IsNumeric(txtRearNetWt.Text) = True Then
'        Rear = CDbl(txtRearNetWt.Text)
'    Else
'        Rear = 0
'    End If
'
'    Total = Front + Rear
'
'    txtTotalNetWt.Text = Total

End Sub

Private Sub txtFrontNetWt_GotFocus()
    HighLightText
End Sub

Private Sub txtFrontNetWt_Validate(Cancel As Boolean)
'    If Trim(txtFrontNetWt.Text) = Empty Then txtFrontNetWt.Text = 0
    If Trim(txtFrontNetWt.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtFrontNetWt.Text) = False Then
        Cancel = True
        MsgBox "The Front Net Wt must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtFrontTareWt_Change()
    Call Calculations
    
'    Dim Front As Double
'    Dim Rear As Double
'    Dim Total As Double
'    Dim Net As Double
'    Dim Gross As Double
'    Dim Tare As Double
'
'    If IsNumeric(txtFrontTareWt.Text) = True Then
'        Front = CDbl(txtFrontTareWt.Text)
'    Else
'        Front = 0
'    End If
'
'    If IsNumeric(txtRearTareWt.Text) = True Then
'        Rear = CDbl(txtRearTareWt.Text)
'    Else
'        Rear = 0
'    End If
'
'    'Gross
'    If IsNumeric(txtFrontGrossWt.Text) = True Then
'        Gross = CDbl(txtFrontGrossWt.Text)
'    Else
'        Gross = 0
'    End If
'
'    Total = Front + Rear
'
'    Net = sldf
'
'    txtTotalTareWt.Text = Total

End Sub

Private Sub txtFrontTareWt_GotFocus()
    HighLightText
End Sub

Private Sub txtFrontTareWt_Validate(Cancel As Boolean)
'    If Trim(txtFrontTareWt.Text) = Empty Then txtFrontTareWt.Text = 0
    If Trim(txtFrontTareWt.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtFrontTareWt.Text) = False Then
        Cancel = True
        MsgBox "The Front Tare Wt must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtGrossGallons_GotFocus()
    HighLightText
End Sub

Private Sub txtGrossGallons_Validate(Cancel As Boolean)
'    If Trim(txtGrossGallons.Text) = Empty Then txtGrossGallons.Text = 0
    If Trim(txtGrossGallons.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtGrossGallons.Text) = False Then
        Cancel = True
        MsgBox "The Gross Gallons must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtLbsPerGal_GotFocus()
    HighLightText
End Sub

Private Sub txtLbsPerGal_Validate(Cancel As Boolean)
'    If Trim(txtLbsPerGal.Text) = Empty Then txtLbsPerGal.Text = 0
    If Trim(txtLbsPerGal.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtLbsPerGal.Text) = False Then
        Cancel = True
        MsgBox "The Lbs Per Gal must be a valid number.", vbInformation, "Scale Pass"
    End If
    
    'Need to make sure there is a decimal place and at least 1 char that is not 0

End Sub

Private Sub txtLoadTemp_GotFocus()
    HighLightText
End Sub

Private Sub txtLoadTime_GotFocus()
    HighLightText
End Sub

Private Sub txtLoadTime_Validate(Cancel As Boolean)
    If IsDate(txtLoadTime.Text) = False And Trim(txtLoadTime.Text) <> Empty Then
        Cancel = True
        MsgBox "The Load Time Must be a valid date/time.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtLotNbr_GotFocus()
    HighLightText
End Sub

Private Sub txtMASCompliance_GotFocus()
    HighLightText
End Sub

Private Sub txtMASCustomer_GotFocus()
    HighLightText
End Sub

Private Sub txtMASCustomerName_GotFocus()
    HighLightText
End Sub

Private Sub txtMASFacilityName_GotFocus()
    HighLightText
End Sub

Private Sub txtMASkDeliveryLoc_GotFocus()
    HighLightText
End Sub

Private Sub txtMASPenatration_GotFocus()
    HighLightText
End Sub

Private Sub txtMASPenatration_Validate(Cancel As Boolean)
'    If Trim(txtMASPenatration.Text) = Empty Then txtMASPenatration.Text = 0
    If Trim(txtMASPenatration.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtMASPenatration.Text) = False Then
        Cancel = True
        MsgBox "The Penatration must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtMASpenetrationtemp_GotFocus()
    HighLightText
End Sub

Private Sub txtMASpenetrationtemp_Validate(Cancel As Boolean)
'    If Trim(txtMASpenetrationtemp.Text) = Empty Then txtMASpenetrationtemp.Text = 0
    If Trim(txtMASpenetrationtemp.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtMASpenetrationtemp.Text) = False Then
        Cancel = True
        MsgBox "The Penatration Temp must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtMASProjDesc_GotFocus()
    HighLightText
End Sub

Private Sub txtMASProjNumber_GotFocus()
    HighLightText
End Sub

Private Sub txtNetGallons_GotFocus()
    HighLightText
End Sub

Private Sub txtNetGallons_Validate(Cancel As Boolean)
'    If Trim(txtNetGallons.Text) = Empty Then txtNetGallons.Text = 0
    If Trim(txtNetGallons.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtNetGallons.Text) = False Then
        Cancel = True
        MsgBox "The Net Gallons must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtNetTons_GotFocus()
    HighLightText
End Sub

Private Sub txtNetTons_Validate(Cancel As Boolean)
'    If Trim(txtNetTons.Text) = Empty Then txtNetTons.Text = 0
    If Trim(txtNetTons.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtNetTons.Text) = False Then
        Cancel = True
        MsgBox "The Net Tons must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub


Private Sub txtProdDesc_GotFocus()
    HighLightText
End Sub

Private Sub txtPurchas_Order_GotFocus()
    HighLightText
End Sub

Private Sub txtRearGrossWt_Change()
    Call Calculations
    
'    Dim Front As Double
'    Dim Rear As Double
'    Dim Total As Double
'
'    If IsNumeric(txtFrontGrossWt.Text) = True Then
'        Front = CDbl(txtFrontGrossWt.Text)
'    Else
'        Front = 0
'    End If
'
'    If IsNumeric(txtRearGrossWt.Text) = True Then
'        Rear = CDbl(txtRearGrossWt.Text)
'    Else
'        Rear = 0
'    End If
'
'    Total = Front + Rear
'
'    txtTotalGrossWt.Text = Total

End Sub

Private Sub txtRearGrossWt_GotFocus()
    HighLightText
End Sub

Private Sub txtRearGrossWt_Validate(Cancel As Boolean)
'    If Trim(txtRearGrossWt.Text) = Empty Then txtRearGrossWt.Text = 0
    If Trim(txtRearGrossWt.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtRearGrossWt.Text) = False Then
        Cancel = True
        MsgBox "The Rear Gross Wt must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtRearNetWt_Change()
'    Dim Front As Double
'    Dim Rear As Double
'    Dim Total As Double
'
'    If IsNumeric(txtFrontNetWt.Text) = True Then
'        Front = CDbl(txtFrontNetWt.Text)
'    Else
'        Front = 0
'    End If
'
'    If IsNumeric(txtRearNetWt.Text) = True Then
'        Rear = CDbl(txtRearNetWt.Text)
'    Else
'        Rear = 0
'    End If
'
'    Total = Front + Rear
'
'    txtTotalNetWt.Text = Total

End Sub

Private Sub txtRearNetWt_GotFocus()
    HighLightText
End Sub

Private Sub txtRearNetWt_Validate(Cancel As Boolean)
'    If Trim(txtRearNetWt.Text) = Empty Then txtRearNetWt.Text = 0
    If Trim(txtRearNetWt.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtRearNetWt.Text) = False Then
        Cancel = True
        MsgBox "The Rear Net Wt must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtRearTareWt_Change()
    Call Calculations
    
'    Dim Front As Double
'    Dim Rear As Double
'    Dim Total As Double
'
'    If IsNumeric(txtFrontTareWt.Text) = True Then
'        Front = CDbl(txtFrontTareWt.Text)
'    Else
'        Front = 0
'    End If
'
'    If IsNumeric(txtRearTareWt.Text) = True Then
'        Rear = CDbl(txtRearTareWt.Text)
'    Else
'        Rear = 0
'    End If
'
'    Total = Front + Rear
'
'    txtTotalTareWt.Text = Total
'
End Sub

Private Sub txtRearTareWt_GotFocus()
    HighLightText
End Sub

Private Sub txtRearTareWt_Validate(Cancel As Boolean)
'    If Trim(txtRearTareWt.Text) = Empty Then txtRearTareWt.Text = 0
    If Trim(txtRearTareWt.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtRearTareWt.Text) = False Then
        Cancel = True
        MsgBox "The Rear Tear Wt must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtRemarks_GotFocus()
    HighLightText
End Sub

Private Sub txtResidue_GotFocus()
    HighLightText
End Sub

Private Sub txtScaleInTime_GotFocus()
    HighLightText
End Sub

Private Sub txtScaleInTime_Validate(Cancel As Boolean)
    If IsDate(txtScaleInTime.Text) = False And Trim(txtScaleInTime.Text) <> Empty Then
        Cancel = True
        MsgBox "The Load Time In Must be a valid date/time.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtScaleOutTime_GotFocus()
    HighLightText
End Sub

Private Sub txtScaleOutTime_Validate(Cancel As Boolean)
    If IsDate(txtScaleOutTime.Text) = False And Trim(txtScaleOutTime.Text) <> Empty Then
        Cancel = True
        MsgBox "The Load Time Out Must be a valid date/time.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtSecurityTapeNbr_GotFocus()
    HighLightText
End Sub

Private Sub txtSpecificGravity_GotFocus()
    HighLightText
End Sub

Private Sub txtSpecificGravity_Validate(Cancel As Boolean)
'    If Trim(txtSpecificGravity.Text) = Empty Then txtSpecificGravity.Text = 0
    If Trim(txtSpecificGravity.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtSpecificGravity.Text) = False Then
        Cancel = True
        MsgBox "The Spec. Gravity must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtTankNumber_GotFocus()
    HighLightText
End Sub

Private Sub txtTotalGrossWt_GotFocus()
    HighLightText
End Sub

Private Sub txtTotalNetWt_GotFocus()
    HighLightText
End Sub

Private Sub txtTotalTareWt_GotFocus()
    HighLightText
End Sub

Private Sub txtTrailer01_GotFocus()
    HighLightText
End Sub

Private Sub txtTrailer02_GotFocus()
    HighLightText
End Sub

Private Sub txtTruckNo_GotFocus()
    HighLightText
End Sub

Private Sub txtViscosity_GotFocus()
    HighLightText
End Sub

Private Sub txtViscosity_Validate(Cancel As Boolean)
'    If Trim(txtViscosity.Text) = Empty Then txtViscosity.Text = 0
    If Trim(txtViscosity.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtViscosity.Text) = False Then
        Cancel = True
        MsgBox "The Viscosity must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtViscosityTemp_GotFocus()
    HighLightText
End Sub

Private Sub txtViscosityTemp_Validate(Cancel As Boolean)
'    If Trim(txtViscosityTemp.Text) = Empty Then txtViscosityTemp.Text = 0
    If Trim(txtViscosityTemp.Text) = Empty Then Exit Sub
    
    If IsNumeric(txtViscosityTemp.Text) = False Then
        Cancel = True
        MsgBox "The Viscosity Temp must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtWeightTktNbr_GotFocus()
    HighLightText
End Sub

Private Sub VScrollPic_Change()
    Dim TotalDiff As Double
    Dim TotalDistance As Double
    Dim MovementAmt As Long
    Dim NewTop As Long
    
    'Get the total distance
    TotalDistance = (picChild.Height + 100)
    
    'Get the Movement Amount
    TotalDiff = TotalDistance - picContainer.Height

    If VScrollPic.Value = 0 Then
        picChild.Top = 50
    Else
        MovementAmt = TotalDiff / 10
        
        If MovementAmt < 0 Then
            picChild.Top = 50
        Else
            NewTop = (MovementAmt * VScrollPic.Value) * -1
            
            picChild.Top = NewTop
        End If
    End If
End Sub


Sub Calculations()
    Dim FrontGross As Double
    Dim RearGross As Double
    Dim FrontTare As Double
    Dim RearTare As Double
    
    Dim TotalGross As Double
    Dim TotalTare As Double
    Dim NetFront As Double
    Dim NetRear As Double
    Dim TotalNet As Double
    
    Dim NetTons As Double
    
    If IsNumeric(txtFrontTareWt.Text) = True Then
        FrontTare = CDbl(txtFrontTareWt.Text)
    Else
        FrontTare = 0
    End If
    
    If IsNumeric(txtRearTareWt.Text) = True Then
        RearTare = CDbl(txtRearTareWt.Text)
    Else
        RearTare = 0
    End If
    
    If IsNumeric(txtFrontGrossWt.Text) = True Then
        FrontGross = CDbl(txtFrontGrossWt.Text)
    Else
        FrontGross = 0
    End If
    
    If IsNumeric(txtRearGrossWt.Text) = True Then
        RearGross = CDbl(txtRearGrossWt.Text)
    Else
        RearGross = 0
    End If
    
    TotalGross = FrontGross + RearGross
    TotalTare = FrontTare + RearTare
    NetFront = FrontGross - FrontTare
    NetRear = RearGross - RearTare
    TotalNet = NetFront + NetRear
    
    NetTons = TotalNet / 2000

    txtTotalGrossWt.Text = TotalGross
    txtTotalTareWt.Text = TotalNet
    txtFrontNetWt.Text = NetFront
    txtRearNetWt.Text = NetRear
    txtTotalNetWt.Text = TotalNet
    
    txtNetTons.Text = NetTons

End Sub


