VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmSetup02 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Scale Pass Setup"
   ClientHeight    =   3690
   ClientLeft      =   2790
   ClientTop       =   4230
   ClientWidth     =   8985
   ControlBox      =   0   'False
   Icon            =   "frmSetup02.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3690
   ScaleWidth      =   8985
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Height          =   375
      Left            =   6105
      TabIndex        =   30
      Top             =   3210
      Width           =   1335
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   7545
      TabIndex        =   29
      Top             =   3210
      Width           =   1335
   End
   Begin VB.TextBox txtCOACnt 
      Alignment       =   2  'Center
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   1
      EndProperty
      Height          =   285
      Left            =   1680
      TabIndex        =   19
      Top             =   3050
      Width           =   855
   End
   Begin VB.CheckBox chRequireLotNbr 
      Alignment       =   1  'Right Justify
      Caption         =   "Require Lot #"
      Height          =   255
      Left            =   2400
      TabIndex        =   17
      Top             =   2760
      Width           =   1455
   End
   Begin VB.CheckBox chEnforceQASample 
      Alignment       =   1  'Right Justify
      Caption         =   "Enforce QA Sample"
      Height          =   255
      Left            =   120
      TabIndex        =   20
      Top             =   3360
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chEnforceCOA 
      Alignment       =   1  'Right Justify
      Caption         =   "Enforce COA"
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   2760
      Width           =   1815
   End
   Begin VB.ComboBox cmboCypressBOLPrntr 
      Height          =   315
      Left            =   1680
      TabIndex        =   14
      Text            =   "Combo1"
      Top             =   2400
      Width           =   2295
   End
   Begin VB.CheckBox chBOLCypress 
      Alignment       =   1  'Right Justify
      Caption         =   "Print BOL to Cypress"
      Height          =   255
      Left            =   3960
      TabIndex        =   15
      Top             =   2400
      Width           =   1815
   End
   Begin VB.ComboBox cmboSeconds 
      Height          =   315
      ItemData        =   "frmSetup02.frx":0442
      Left            =   7680
      List            =   "frmSetup02.frx":07CD
      Style           =   2  'Dropdown List
      TabIndex        =   23
      Top             =   2040
      Width           =   1095
   End
   Begin VB.CheckBox chAutoReturn 
      Alignment       =   1  'Right Justify
      Caption         =   "Auto Return to Open Load"
      Height          =   375
      Left            =   6360
      TabIndex        =   21
      Top             =   1680
      Width           =   2415
   End
   Begin VB.ComboBox cmboAlternatePrntr 
      Height          =   315
      ItemData        =   "frmSetup02.frx":0D44
      Left            =   1680
      List            =   "frmSetup02.frx":0D46
      TabIndex        =   12
      Text            =   "Combo1"
      Top             =   1980
      Width           =   2295
   End
   Begin VB.ComboBox cmboBOLPrntr 
      Height          =   315
      Left            =   1680
      TabIndex        =   9
      Text            =   "Combo1"
      Top             =   1560
      Width           =   2295
   End
   Begin VB.ComboBox cmboScalePrntr 
      Height          =   315
      Left            =   1680
      TabIndex        =   6
      Text            =   "Combo1"
      Top             =   1155
      Width           =   2295
   End
   Begin VB.CheckBox chManualScaling 
      Alignment       =   1  'Right Justify
      Caption         =   "Manual Scaling"
      Height          =   255
      Left            =   3960
      TabIndex        =   2
      Top             =   270
      Width           =   1815
   End
   Begin VB.ComboBox cmoMode 
      Height          =   315
      Left            =   1680
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   210
      Width           =   2295
   End
   Begin MSComDlg.CommonDialog cdDDEPath 
      Left            =   6090
      Top             =   390
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DialogTitle     =   "Select the DDE Application"
      Filter          =   "DDE (DDE.exe)|DDE.exe|Exacutables (*.exe)|*.exe|All (*.*)|*.*"
   End
   Begin VB.ComboBox cmoPlant 
      Height          =   315
      ItemData        =   "frmSetup02.frx":0D48
      Left            =   1680
      List            =   "frmSetup02.frx":0D4A
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   654
      Width           =   2295
   End
   Begin VB.CheckBox chScale 
      Alignment       =   1  'Right Justify
      Caption         =   "Print Scale Pass"
      Height          =   255
      Left            =   3960
      TabIndex        =   7
      Top             =   1200
      Width           =   1815
   End
   Begin VB.CheckBox chBOL 
      Alignment       =   1  'Right Justify
      Caption         =   "Print the BOL"
      Height          =   255
      Left            =   3960
      TabIndex        =   10
      Top             =   1620
      Width           =   1815
   End
   Begin VB.TextBox txtBOLPrinter 
      Height          =   285
      Left            =   1680
      TabIndex        =   28
      Top             =   1560
      Width           =   2295
   End
   Begin VB.TextBox txtScalePrinter 
      Height          =   285
      Left            =   1680
      TabIndex        =   27
      Top             =   1155
      Width           =   2295
   End
   Begin VB.CommandButton cmdScale 
      Caption         =   "Scale &Pass Database"
      Height          =   375
      Left            =   7050
      TabIndex        =   24
      ToolTipText     =   "Setup the connection to the Scale Pass Database"
      Top             =   210
      Width           =   1815
   End
   Begin VB.CommandButton cmdMAS 
      Caption         =   "M&AS 500 Database"
      Height          =   375
      Left            =   7050
      TabIndex        =   25
      ToolTipText     =   "Setup the connection to the MAS 500 Database"
      Top             =   600
      Width           =   1815
   End
   Begin VB.CommandButton cmdMcLeod 
      Caption         =   "&McLeod Database"
      Height          =   375
      Left            =   7050
      TabIndex        =   26
      ToolTipText     =   "Setup the connection to the McLeod database"
      Top             =   990
      Width           =   1815
   End
   Begin VB.Label lblCOACnt 
      Caption         =   "# Of COA Copies:"
      Height          =   255
      Left            =   120
      TabIndex        =   18
      Top             =   3050
      Width           =   1335
   End
   Begin VB.Label Label7 
      Caption         =   "Cypress Printer"
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   2430
      Width           =   1455
   End
   Begin VB.Label Label5 
      Caption         =   "Seconds"
      Height          =   255
      Left            =   6360
      TabIndex        =   22
      Top             =   2040
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Alternate Printer"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   2010
      Width           =   1335
   End
   Begin VB.Label Label6 
      Caption         =   "Mode"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   270
      Width           =   1335
   End
   Begin VB.Label Label4 
      Caption         =   "BOL Printer"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   1590
      Width           =   1335
   End
   Begin VB.Label Label3 
      Caption         =   "Scale Printer"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   1200
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Pla&nt"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   720
      Width           =   1335
   End
End
Attribute VB_Name = "frmSetup02"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private lcPlant As String                'Name of the plant using scale pass
Private lcPlantPrefix As String          'Number of the plant using scale pass
Private lcSCALEPASSPRINTER As String     'Name of the Scale Pass Printer
Private lcBLPRINTER As String            'Name of the BOL printer
Private lcALTERNATEPRINTER As String     'Name of the Alternate printer
'Private lcDAYSTOKEEP As Integer          '???
Private lcMANUALSCALING As Boolean       'Determines if plant is hooked to a scale
Private lcBOLCypressPRINTER As String     'Name of the BOL Cypress Printer
Private lcNbrOfCOACopies As Integer      'Number of COA copies to print     'RKL DEJ 9/25/13

Private lcEnforceCOA As Boolean
Private lcEnforceQASample As Boolean
Private lcRequireLotNbr As Boolean

'May not need these
Private lcPRINTBL As Boolean         '???
Private lcPRINTSCALEPASS As Boolean  '???
Private lcMODE As String             'Debug/Live
Private lcPrintBOLCypress As Boolean '

Private lcAutoRtnOpenLoads As Boolean
Private lcAutoRtrnSecnds As Integer

'Scale Pass Database
Private lcScaleConnStr As String
Private lcScaleDSN As String
Private lcScaleUID As String
Private lcScalePWD As String
Private lcScaleDB As String
Private lcScaleDriver As String
Private lcScaleServer As String
Private lcScaleAdlPrty As String
Private lcScaleWinAuth As Boolean

'McLeod Database
Private lcMcLeodConnStr As String
Private lcMcLeodDSN As String
Private lcMcLeodUID As String
Private lcMcLeodPWD As String
Private lcMcLeodDB As String
Private lcMcLeodDriver As String
Private lcMcLeodServer As String
Private lcMcLeodAdlPrty As String
Private lcMcLeodWinAuth As Boolean

'MAS 500 Database
Private lcMASConnStr As String
Private lcMASDSN As String
Private lcMASUID As String
Private lcMASPWD As String
Private lcMASDB As String
Private lcMASDriver As String
Private lcMASServer As String
Private lcMASAdlPrty As String
Private lcMASWinAuth As Boolean


Private Sub chAutoReturn_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chAutoReturn.Value = vbChecked Then
        lcAutoRtnOpenLoads = True
    Else
        lcAutoRtnOpenLoads = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chAutoReturn_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Private Sub chBOL_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chBOL.Value = vbChecked Then
        lcPRINTBL = True
    Else
        lcPRINTBL = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chBOL_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Private Sub chBOLCypress_Click()

    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chBOLCypress.Value = vbChecked Then
        lcPrintBOLCypress = True
    Else
        lcPrintBOLCypress = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chBOLCypress_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Private Sub chEnforceCOA_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chEnforceCOA.Value = vbChecked Then
        lcEnforceCOA = True
    Else
        lcEnforceCOA = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chEnforceCOA_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub chEnforceQASample_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chEnforceQASample.Value = vbChecked Then
        lcEnforceQASample = True
    Else
        lcEnforceQASample = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chEnforceQASample_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time


End Sub

Private Sub chManualScaling_Click()
    gbLastActivityTime = Date + Time
    
    If chManualScaling.Value = vbChecked Then
        lcMANUALSCALING = True
    Else
        lcMANUALSCALING = False
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub chRequireLotNbr_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chRequireLotNbr.Value = vbChecked Then
        lcRequireLotNbr = True
    Else
        lcRequireLotNbr = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chRequireLotNbr_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub chScale_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chScale.Value = vbChecked Then
        lcPRINTSCALEPASS = True
    Else
        lcPRINTSCALEPASS = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".chScale_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboAlternatePrntr_Click()
    lcALTERNATEPRINTER = cmboAlternatePrntr.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboAlternatePrntr_Change()
    lcALTERNATEPRINTER = cmboAlternatePrntr.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboBOLPrntr_Click()
    lcBLPRINTER = cmboBOLPrntr.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboBOLPrntr_Change()
    lcBLPRINTER = cmboBOLPrntr.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboCypressBOLPrntr_Change()
    lcBOLCypressPRINTER = cmboCypressBOLPrntr.Text

    gbLastActivityTime = Date + Time

End Sub

Private Sub cmboCypressBOLPrntr_Click()
    lcBOLCypressPRINTER = cmboCypressBOLPrntr.Text

    gbLastActivityTime = Date + Time

End Sub

Private Sub cmboScalePrntr_Click()
    lcSCALEPASSPRINTER = cmboScalePrntr.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboScalePrntr_Change()
    lcSCALEPASSPRINTER = cmboScalePrntr.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboSeconds_Change()
    On Error GoTo Error
    
    lcAutoRtrnSecnds = CInt(cmboSeconds.Text)
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
    
Error:
    MsgBox Me.Name & ".cmboSeconds_Change()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub cmboSeconds_Click()
    On Error GoTo Error
    
    lcAutoRtrnSecnds = CInt(cmboSeconds.Text)
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
    
Error:
    MsgBox Me.Name & ".cmboSeconds_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time


End Sub

Private Sub cmboSeconds_LostFocus()
    On Error GoTo Error
    
    lcAutoRtrnSecnds = CInt(cmboSeconds.Text)
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
    
Error:
    MsgBox Me.Name & ".cmboSeconds_LostFocus()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time


End Sub

Private Sub cmboSeconds_Validate(Cancel As Boolean)
    On Error GoTo Error
    
    lcAutoRtrnSecnds = CInt(cmboSeconds.Text)
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
    
Error:
    MsgBox Me.Name & ".cmboSeconds_Validate()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time


End Sub

Private Sub cmdCancel_Click()
    gbLastActivityTime = Date + Time
    
    Unload Me

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdMAS_Click()
    On Error GoTo Error
    
    Dim FRM As New frmODBCLogon
    
    gbLastActivityTime = Date + Time
    
    FRM.ClearProperties
    FRM.GetDSNsAndDrivers
    
    FRM.ConnString = lcMASConnStr
    FRM.DSN = lcMASDSN
    FRM.UID = lcMASUID
    FRM.Password = lcMASPWD
    FRM.Database = lcMASDB
    FRM.Driver = lcMASDriver
    FRM.Server = lcMASServer
    FRM.AddlProperties = lcMASAdlPrty
    FRM.WinAuth = lcMASWinAuth
    
    FRM.Show vbModal, Me
    
    lcMASConnStr = FRM.ConnString
    lcMASDSN = FRM.DSN
    lcMASUID = FRM.UID
    lcMASPWD = FRM.Password
    lcMASDB = FRM.Database
    lcMASDriver = FRM.Driver
    lcMASServer = FRM.Server
    lcMASAdlPrty = FRM.AddlProperties
    lcMASWinAuth = FRM.WinAuth
    
    Set FRM = Nothing
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdMAS_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdMcLeod_Click()
    On Error GoTo Error
    
    Dim FRM As New frmODBCLogon
    
    gbLastActivityTime = Date + Time
    
    FRM.ClearProperties
    FRM.GetDSNsAndDrivers
    
    FRM.ConnString = lcMcLeodConnStr
    FRM.DSN = lcMcLeodDSN
    FRM.UID = lcMcLeodUID
    FRM.Password = lcMcLeodPWD
    FRM.Database = lcMcLeodDB
    FRM.Driver = lcMcLeodDriver
    FRM.Server = lcMcLeodServer
    FRM.AddlProperties = lcMcLeodAdlPrty
    FRM.WinAuth = lcMcLeodWinAuth
    
    FRM.Show vbModal, Me
    
    lcMcLeodConnStr = FRM.ConnString
    lcMcLeodDSN = FRM.DSN
    lcMcLeodUID = FRM.UID
    lcMcLeodPWD = FRM.Password
    lcMcLeodDB = FRM.Database
    lcMcLeodDriver = FRM.Driver
    lcMcLeodServer = FRM.Server
    lcMcLeodAdlPrty = FRM.AddlProperties
    lcMcLeodWinAuth = FRM.WinAuth
    
    Set FRM = Nothing
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdMcLeod_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdSave_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    'update global properties/settings
    Call UpdateGlobalVar
    
    Call SaveSettings
    
    Unload Me
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSave_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdScale_Click()
    On Error GoTo Error
    
    Dim FRM As New frmODBCLogon
    
    gbLastActivityTime = Date + Time
    
    FRM.ClearProperties
    FRM.GetDSNsAndDrivers
    
    FRM.ConnString = lcScaleConnStr
    FRM.DSN = lcScaleDSN
    FRM.UID = lcScaleUID
    FRM.Password = lcScalePWD
    FRM.Database = lcScaleDB
    FRM.Driver = lcScaleDriver
    FRM.Server = lcScaleServer
    FRM.AddlProperties = lcScaleAdlPrty
    FRM.WinAuth = lcScaleWinAuth
    
    FRM.Show vbModal, Me
    
    lcScaleConnStr = FRM.ConnString
    lcScaleDSN = FRM.DSN
    lcScaleUID = FRM.UID
    lcScalePWD = FRM.Password
    lcScaleDB = FRM.Database
    lcScaleDriver = FRM.Driver
    lcScaleServer = FRM.Server
    lcScaleAdlPrty = FRM.AddlProperties
    lcScaleWinAuth = FRM.WinAuth
    
    Set FRM = Nothing
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdScale_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub cmoMode_Validate(Cancel As Boolean)
    lcMODE = cmoMode.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmoPlant_Validate(Cancel As Boolean)
    lcPlant = cmoPlant.Text
    lcPlantPrefix = cmoPlant.ItemData(cmoPlant.ListIndex)

    gbLastActivityTime = Date + Time
End Sub

Private Sub Form_Load()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Call PopulateCombos
    
    Call SetupLocalVar
    
    Call SetupFrmObjects
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".Form_Load()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub


Sub SetupLocalVar()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    lcPlant = gbPlant
    lcPlantPrefix = gbPlantPrefix
    lcSCALEPASSPRINTER = gbSCALEPASSPRINTER
    lcBLPRINTER = gbBLPRINTER
    lcALTERNATEPRINTER = gbALTERNATEPRINTER
'    lcDAYSTOKEEP = gbDAYSTOKEEP
    lcMANUALSCALING = gbMANUALSCALING
    lcBOLCypressPRINTER = gbBOLCypressPRINTER
    lcNbrOfCOACopies = gbNbrOfCOACopies         'RKL DEJ 9/25/13
    
    
    lcEnforceCOA = gbEnforceCOA
    lcEnforceQASample = gbEnforceQASample
    lcRequireLotNbr = gbRequireLotNbr
    
    lcAutoRtnOpenLoads = gbAutoRtnOpenLoads
    lcAutoRtrnSecnds = gbAutoRtrnSecnds
    
    'May not need these
    lcPRINTBL = gbPRINTBL
    lcPRINTSCALEPASS = gbPRINTSCALEPASS
    lcMODE = gbMODE
    lcPrintBOLCypress = gbPrintBOLCypress
        
    'Scale Pass Database
    lcScaleConnStr = gbScaleConnStr
    lcScaleDSN = gbScaleDSN
    lcScaleUID = gbScaleUID
    lcScalePWD = gbScalePWD
    lcScaleDB = gbScaleDB
    lcScaleDriver = gbScaleDriver
    lcScaleServer = gbScaleServer
    lcScaleAdlPrty = gbScaleAdlPrty
    lcScaleWinAuth = gbScaleWinAuth
    
    'McLeod Database
    lcMcLeodConnStr = gbMcLeodConnStr
    lcMcLeodDSN = gbMcLeodDSN
    lcMcLeodUID = gbMcLeodUID
    lcMcLeodPWD = gbMcLeodPWD
    lcMcLeodDB = gbMcLeodDB
    lcMcLeodDriver = gbMcLeodDriver
    lcMcLeodServer = gbMcLeodServer
    lcMcLeodAdlPrty = gbMcLeodAdlPrty
    lcMcLeodWinAuth = gbMcLeodWinAuth
    
    'MAS 500 Database
    lcMASConnStr = gbMASConnStr
    lcMASDSN = gbMASDSN
    lcMASUID = gbMASUID
    lcMASPWD = gbMASPWD
    lcMASDB = gbMASDB
    lcMASDriver = gbMASDriver
    lcMASServer = gbMASServer
    lcMASAdlPrty = gbMASAdlPrty
    lcMASWinAuth = gbMASWinAuth
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".SetupLocalVar()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub


Sub UpdateGlobalVar()
    On Error GoTo Error
    gbLastActivityTime = Date + Time
    
    gbPlant = lcPlant
    gbPlantPrefix = lcPlantPrefix
    gbSCALEPASSPRINTER = lcSCALEPASSPRINTER
    gbBLPRINTER = lcBLPRINTER
    gbALTERNATEPRINTER = lcALTERNATEPRINTER
'    gbDAYSTOKEEP = lcDAYSTOKEEP
    gbMANUALSCALING = lcMANUALSCALING
    gbBOLCypressPRINTER = lcBOLCypressPRINTER
    gbNbrOfCOACopies = lcNbrOfCOACopies             'RKL DEJ 9/25/13

    gbEnforceCOA = lcEnforceCOA
    gbEnforceQASample = lcEnforceQASample
    gbRequireLotNbr = lcRequireLotNbr


    'May not need these
    gbPRINTBL = lcPRINTBL
    gbPRINTSCALEPASS = lcPRINTSCALEPASS
    gbMODE = lcMODE
    gbPrintBOLCypress = lcPrintBOLCypress
    
    gbAutoRtnOpenLoads = lcAutoRtnOpenLoads
    gbAutoRtrnSecnds = lcAutoRtrnSecnds
    
    'Scale Pass Database
    gbScaleConnStr = lcScaleConnStr
    gbScaleDSN = lcScaleDSN
    gbScaleUID = lcScaleUID
    gbScalePWD = lcScalePWD
    gbScaleDB = lcScaleDB
    gbScaleDriver = lcScaleDriver
    gbScaleServer = lcScaleServer
    gbScaleAdlPrty = lcScaleAdlPrty
    gbScaleWinAuth = lcScaleWinAuth
    
    'McLeod Database
    gbMcLeodConnStr = lcMcLeodConnStr
    gbMcLeodDSN = lcMcLeodDSN
    gbMcLeodUID = lcMcLeodUID
    gbMcLeodPWD = lcMcLeodPWD
    gbMcLeodDB = lcMcLeodDB
    gbMcLeodDriver = lcMcLeodDriver
    gbMcLeodServer = lcMcLeodServer
    gbMcLeodAdlPrty = lcMcLeodAdlPrty
    gbMcLeodWinAuth = lcMcLeodWinAuth
    
    'MAS 500 Database
    gbMASConnStr = lcMASConnStr
    gbMASDSN = lcMASDSN
    gbMASUID = lcMASUID
    gbMASPWD = lcMASPWD
    gbMASDB = lcMASDB
    gbMASDriver = lcMASDriver
    gbMASServer = lcMASServer
    gbMASAdlPrty = lcMASAdlPrty
    gbMASWinAuth = lcMASWinAuth
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".UpdateGlobalVar()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub

Sub PopulateCombos()
    On Error GoTo Error
    
    Dim Prntr As Printer
    
    gbLastActivityTime = Date + Time
    
    cmoMode.Clear
    cmoMode.AddItem "Live"
    cmoMode.AddItem "Debug"
    
    cmoPlant.Clear
    cmoPlant.AddItem "Blackfoot"
    cmoPlant.ItemData(0) = 5
    cmoPlant.AddItem "Hauser"
    cmoPlant.ItemData(1) = 4
    cmoPlant.AddItem "Nampa"
    cmoPlant.ItemData(2) = 3
    cmoPlant.AddItem "Rawlins"
    cmoPlant.ItemData(3) = 7
    cmoPlant.AddItem "Woodscross"
    cmoPlant.ItemData(4) = 6
    
    cmboScalePrntr.Clear
    cmboBOLPrntr.Clear
    cmboAlternatePrntr.Clear
    cmboCypressBOLPrntr.Clear
    For Each Prntr In Printers
        cmboScalePrntr.AddItem Prntr.DeviceName
        cmboBOLPrntr.AddItem Prntr.DeviceName
        cmboAlternatePrntr.AddItem Prntr.DeviceName
        cmboCypressBOLPrntr.AddItem Prntr.DeviceName
    Next
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".PopulateCombos()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub


Sub SetupFrmObjects()
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    cmoMode.Text = lcMODE
    cmoPlant.Text = lcPlant
    
    cmboScalePrntr.Text = lcSCALEPASSPRINTER
    cmboBOLPrntr.Text = lcBLPRINTER
    cmboAlternatePrntr.Text = lcALTERNATEPRINTER
    cmboCypressBOLPrntr.Text = lcBOLCypressPRINTER
    
    txtCOACnt.Text = CStr(lcNbrOfCOACopies)     'RKL DEJ 9/25/13
    
    chManualScaling.Value = lcMANUALSCALING * -1
    
    If lcAutoRtnOpenLoads = True Then
        chAutoReturn.Value = vbChecked
    Else
        chAutoReturn.Value = vbUnchecked
    End If
    
    cmboSeconds.Text = lcAutoRtrnSecnds
    
    If lcEnforceCOA = True Then
        chEnforceCOA.Value = vbChecked
    Else
        chEnforceCOA.Value = vbUnchecked
    End If
    
    If lcEnforceQASample = True Then
        chEnforceQASample.Value = vbChecked
    Else
        chEnforceQASample.Value = vbUnchecked
    End If

    
    If lcRequireLotNbr = True Then
        chRequireLotNbr.Value = vbChecked
    Else
        chRequireLotNbr.Value = vbUnchecked
    End If
    
    If lcPRINTSCALEPASS = True Then
        chScale.Value = vbChecked
    Else
        chScale.Value = vbUnchecked
    End If
    
    If lcPRINTBL = True Then
        chBOL.Value = vbChecked
    Else
        chBOL.Value = vbUnchecked
    End If
    
    If lcMANUALSCALING = True Then
        chManualScaling.Value = vbChecked
    Else
        chManualScaling.Value = vbUnchecked
    End If
    
    If lcPrintBOLCypress = True Then
        chBOLCypress.Value = vbChecked
    Else
        chBOLCypress.Value = vbUnchecked
    End If
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub
    

Private Sub txtBOLPrinter_Change()
    lcBLPRINTER = txtBOLPrinter.Text
    
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtCOACnt_Change()
    If IsNumeric(txtCOACnt.Text) = True Then
        lcNbrOfCOACopies = CInt(txtCOACnt.Text)
    End If
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub txtCOACnt_Validate(Cancel As Boolean)
    If IsNumeric(txtCOACnt.Text) = False Then
        Cancel = True
        MsgBox "Must be a valid number", vbInformation, "Not a Number"
    End If
    gbLastActivityTime = Date + Time
End Sub

Private Sub txtScalePrinter_Change()
    lcSCALEPASSPRINTER = txtScalePrinter.Text

    gbLastActivityTime = Date + Time
End Sub
